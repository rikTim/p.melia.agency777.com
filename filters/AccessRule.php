<?php

namespace app\filters;

use Yii;
use yii\filters\VerbFilter;

class AccessRule extends VerbFilter
{

    /** @inheritdoc */
    protected function matchRole($user)
    {
        if (empty($this->roles)) {
            return true;
        }

        foreach ($this->roles as $role) {
            if ($role === '?') {
                if (Yii::$app->user->isGuest) {
                    return true;
                }
            } elseif ($role === '@') {
                if (!Yii::$app->user->isGuest) {
                    return true;
                }
            } elseif ($role === 'admin') {
                if (!Yii::$app->user->isGuest && Yii::$app->user->identity->statusid = 1  ) {
                    return true;
                }
            }
        }

        return false;
    }
}