<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\components\PopupContactsWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language  ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="format-detection" content="telephone=no"/>

    <link rel="shortcut icon" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/favicon.ico" />

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/grid.min.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/style.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/booking.min.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/pickmeup.min.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/pick-custome.min.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/chosen.min.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/chosen-custome.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/jquery.filer.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/jquery.filer-dragdropbox-theme.min.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/jquery.arcticmodal-0.3.css">
    <link rel="stylesheet" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/css/simple.css">
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->


    <!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/html5shiv.js"></script>
    <![endif]-->

</head>

<body>
    <?php $this->beginBody() ?>
        <?= $content ?>
        <!--==============================FOOTER==============================-->
            <div style="display: none;">
                <div class="box-modal" id="rulesModel">
                    <div class="box-modal_close arcticmodal-close">X</div>
                    <p class="title">Rulles</p>
                </div>
            </div>

            <div style="display: none;">
                <div class="box-modal" id="copyrightModal">
                    <div class="box-modal_close arcticmodal-close">X</div>
                    <p>
                        Постулат, как следует из вышесказанного, притягивает интеграл от функции, имеющий конечный разрыв. Нормаль к поверхности категорически искажает стремящийся ротор векторного поля. Дифференциальное исчисление изменяет интеграл Дирихле.
                    </p>
                    <p>
                        Первообразная функция создает комплексный интеграл Гамильтона, что несомненно приведет нас к истине. Расходящийся ряд охватывает параллельный интеграл Гамильтона, явно демонстрируя всю чушь вышесказанного. Функция выпуклая книзу по-прежнему востребована.
                    </p>
                </div>
            </div>


<!--            <div style="display: none;">-->
<!--                <div class="box-modal" id="registerSuccess">-->
<!--                    <p>Профайл отправлен на модерацию.</p>-->
<!--                </div>-->
<!--            </div>-->


            <div style="display: none;">
                <div class="box-modal" id="contactModal">
                    <div class="box-modal_close arcticmodal-close">X</div>
                    <p class="title"> Contact us </p> <br>
                    <?= PopupContactsWidget::widget(); ?>
                </div>
            </div>


<!--            <div class="box-model-profile-wrapper" style="display: none;">-->
<!--                <div class="box-modal-profile" id="registerSuccess">-->
<!--                    <div class="box-modal_close arcticmodal-close">X</div>-->
<!--                    <p>Профайл отправлен на мадерацию.</p>-->
<!--                </div>-->
<!--            </div>-->

            <footer>
                <div class="container">
                    <div class="footer-brand">
                        <div class="footer-brand_name"><a href="#"><?= Yii::$app->params['siteName']; ?></a></div>
                        <p class="footer-brand_slogan">Premium Partner Program</p>
                    </div>
                    <p class="copyright">
                        © <span id="copyright-year"></span>. All Rights Reserved
                        <!-- {%FOOTER_LINK} -->
                    </p>
                </div>
            </footer>
        </div>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/jquery.js"></script>
    <script>
        var path = location.pathname.split('/');
        window.baseUrl = location.protocol + '//' + location.hostname + ':' + location.port + '/' + path[1];
    </script>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/chosen.jquery.js"></script>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/jquery.arcticmodal-0.3.min.js"></script>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/jquery.cookie.js"></script>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/device.min.js"></script>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/jquery-migrate-1.2.1.js"></script>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/jquery.pickmeup.min.js"></script>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/prism.js"></script>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/functions.js"></script>
    <script src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/js/script.js"></script>


    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter33519188 = new Ya.Metrika({
                        id:33519188,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/33519188" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->




    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
