<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\components\AsideWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="high">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/favicon.ico" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php require_once('adminLayouts/styles.php'); ?>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">
    <?php require_once('adminLayouts/header.php'); ?>
    <?= AsideWidget::widget(); ?>
    <?= $content ?>
    <footer class="main-footer">
        <div class="pull-right hidden-xs"></div>
        <strong>Copyright &copy; 2015 <a href="">Melia Agency</a>.</strong> All rights reserved.
    </footer>
    <?php require_once('adminLayouts/settings.php'); ?>
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->
<?php $this->endBody() ?>
<?php require_once('adminLayouts/js.php'); ?>
</body>
</html>
<?php $this->endPage() ?>
