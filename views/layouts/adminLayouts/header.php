<? use yii\helpers\Url; ?>
<header class="main-header">
    <a href="http://p.melia.agency777.com" class="logo">
        <span class="logo-mini"><b>M</b>AG</span>
        <span class="logo-lg"><b>Melia</b>AGENCY</span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a class="label-danger" href="<?= Url::toRoute(['site/out']); ?>">  <i class="fa fa-sign-out" ></i>Logout </a>
                </li>
            </ul>
        </div>
    </nav>
</header>