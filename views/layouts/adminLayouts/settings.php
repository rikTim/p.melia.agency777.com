<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="control-sidebar-settings-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">General Settings</h3>
                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Email notices
                        <input type="checkbox" class="pull-right" checked>
                    </label>
                    <p>
                        Receive notices via email.
                    </p>
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Phone notices
                        <input type="checkbox" class="pull-right" checked>
                    </label>
                    <p>
                        Receive notices via phone.
                    </p>
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Facebook notices
                        <input type="checkbox" class="pull-right" checked>
                    </label>
                    <p>
                        Receive notices via facebook.
                    </p>
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Vk notices
                        <input type="checkbox" class="pull-right" checked>
                    </label>
                    <p>
                        Receive notices via vk.
                    </p>
                </div><!-- /.form-group -->

            </form>
        </div><!-- /.tab-pane -->
    </div>
</aside><!-- /.control-sidebar -->