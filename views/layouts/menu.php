<section class="well__05">
    <div class="container sectionPadding">
        <nav>
            <ul>
                <li><a href="<?= Url::toRoute(['site/main']); ?>">Main</a></li>
                <li><a href="<?= Url::toRoute(['profile/index']); ?>">Profile</a></li>
                <li><a href="<?= Url::toRoute(['site/messages']); ?>">Messages</a></li>
                <li><a href="<?= Url::toRoute(['site/contact']); ?>">Contacts</a></li>
                <li><a href="<?= Url::toRoute(['addgirl/index']); ?>">Register Women</a></li>
                <li><a href="<?= Url::toRoute(['site/about']); ?>">About agency</a></li>
                <li class="balance"><a href="<?= Url::toRoute(['site/balance']); ?>"><i class="fa fa-money"></i> 100</a></li>
                <li><a href="<?= Url::toRoute(['site/out']); ?>">Logout ( <?= Yii::$app->user->identity->username; ?> )</a></li>
                <li class="lang-select"><span class="selected-lang"></span>
                    <ul>
                        <li>En</li><li>Ru</li><li>Es</li><li>Cz</li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</section>