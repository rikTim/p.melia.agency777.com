<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\ChatTopDates */

?>
<div class="content-wrapper">

    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">

                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">



                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'girl_id')->textInput(['disabled'=>'disabled']) ?>
                <?= $form->field($model, 'girl_id')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'men_id')->textInput(['disabled'=>'disabled']) ?>
                <?= $form->field($model, 'men_id')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'men_name')->textInput(['disabled'=>'disabled'],['maxlength' => true]) ?>
                <?= $form->field($model, 'men_name')->hiddenInput()->label(false) ?>
                <label class="control-label" for="chattopdates-text">Message Men</label>

                <div class="message-men"><?=$message->text ?></div><br>


                <?= $form->field($model, 'text')->textarea(['rows' => 8]) ?>

                <?= $form->field($model, 'is_men_letter')->hiddenInput()->label(false) ?>

                <?= $form->field($model, 'is_readed')->hiddenInput()->label(false) ?>

                <?= $form->field($model, 'datetime')->hiddenInput()->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton( 'Approved', ['class' =>  'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>

</div>