<?php

use yii\bootstrap\Html;
?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-female"></i>

                <h3 class="box-title">Messages</h3>

                <div class="box-tools">
                </div>
            </div>
            <div class="form-active">
                <table class="table table-striped table-bordered">
                    <colgroup>
                        <col width="80">
                        <col width="80">
                        <col>

                        <col width="80">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>
                            ID Girl
                        </th>
                        <th>
                            ID Men
                        </th>
                        <th>
                            Text
                        </th>


                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($messages)): ?>
                        <?php foreach ($messages as $message): ?>
                            <tr>
                                <td><?=$message->girl_id?></td>
                                <td><?=$message->men_id?></td>
                                <td><?=$message->text?></td>

                                <td>
                                    <?= Html::a('Update', 'update?id='.$message->id, ['class' => 'btn btn-primary']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>