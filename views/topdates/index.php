<?php

use yii\bootstrap\Html;
?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-female"></i>

                <h3 class="box-title">Messages</h3>

                <div class="box-tools">
                </div>
            </div>
            <div class="form-active">
                <table class="table table-striped table-bordered">
                    <colgroup>
                        <col width="200">

                        <col>

                        <col width="80">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>
                            ID Girl
                        </th>

                        <th>
                            Text
                        </th>


                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($girls)): ?>
                        <?php foreach ($girls as $girl): ?>
                            <tr>
                                <td><?=$girl->girl_id?></td>
                                <td><?=$girl->text?></td>

                                <td>
                                    <?= Html::a('Update', 'girlmessage?id='.$girl->girl_id, ['class' => 'btn btn-primary']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>