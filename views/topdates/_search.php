<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TopdatesSeach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chat-top-dates-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'girl_id') ?>

    <?= $form->field($model, 'men_id') ?>

    <?= $form->field($model, 'men_name') ?>

    <?= $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'is_men_letter') ?>

    <?php // echo $form->field($model, 'is_readed') ?>

    <?php // echo $form->field($model, 'is_aproved') ?>

    <?php // echo $form->field($model, 'datetime') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
