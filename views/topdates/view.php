<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ChatTopDates */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Chat Top Dates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-top-dates-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'girl_id',
            'men_id',
            'men_name',
            'text:ntext',
            'is_men_letter',
            'is_readed',
            'is_aproved',
            'datetime:datetime',
        ],
    ]) ?>

</div>
