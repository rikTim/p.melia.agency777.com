<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Money */
$this->title = 'Money';
?>
<div class="content-wrapper">
    <section class="content-header">
    </section>

    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-money"></i>

                <h3 class="box-title">Money</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">

                <h1><?= Html::encode($this->title) ?></h1>

                <?php $form = ActiveForm::begin(); ?>

                <!--    --><? //= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'money')->textInput() ?>

                <div class="form-group">

                    <?= Html::submitButton('Update', ['class' => 'btn btn-success', 'name' => 'register-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>


            </div>
        </div>

    </section>

</div>