<?php
use yii\bootstrap\Html;
?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-female"></i>

                <h3 class="box-title">Answer Me</h3>

                <div class="box-tools">
                </div>
            </div>
            <div class="form-active">
                <table class="table table-striped table-bordered">
                    <colgroup>
                        <col width="150">

                        <col width="150">

                        <col >

                        <col width="80">
                    </colgroup>
                    <thead>
                    <tr>

                        <th>
                            Girl Name
                        </th>
                        <th>
                            Men Name
                        </th>

                        <th>
                            Text
                        </th>


                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php if (!empty($reply)): ?>
                        <?php foreach ($reply as $rep): ?>
                            <tr>

                                <td><?= $rep->girl_name ?></td>
                                <td><?= $rep->men_name ?></td>
                                <td><?= substr($rep->text, 0, 275); ?></td>

                                <td>
                                    <?= Html::a('Answer', 'answerme?id=' . $rep->message_id, ['class' => 'btn btn-primary']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>