<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $this->title = 'Balance'; ?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-info"></i>

                <h3 class="box-title">Balance</h3>
            </div>
            <?php if ($partner): ?>
                <div class="form-active">
                    <table class="table table-striped table-bordered detail-view">
                        <?php $form = ActiveForm::begin(['id' => 'balanceForm',]); ?>
                        <tbody>
                        <tr>
                            <th>ID</th>
                            <td class="partnerId"><?= $partner->id; ?></td>
                        </tr>
                        <tr>
                            <th>Partner</th>
                            <td><?= $partner->name . ' ' . $partner->lastname; ?></td>
                        </tr>
                        <tr>
                            <th>Balance</th>
                            <td>$<?= (!empty($partner->balance)) ? $partner->balance : 0; ?></td>
                        </tr>
                        <tr>
                            <th>Get money type</th>
                            <td><?= $partner->getMoney; ?></td>
                        </tr>
                        <tr>
                            <th>Get money</th>
                            <td>
                                <input type="text" class="form-control" name="Balances[transaction]" value=""
                                       maxlength="35">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="Balances[userid]" value="<?= $partner->userid; ?>">
                    <?= Html::submitButton('Cancel', ['class' => 'btn btn-warning']) ?>
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
                    <?php if ($partner->getMoney == 'По месячная оплата' && $isRecount == 0) {
                        echo Html::button('Recalculate the amount for the month', ['class' => 'btn btn-success recountPartnerAmount']);
                    } ?>
                    <?php ActiveForm::end(); ?>
                </div>
            <?php else: ?>
                <h4>No such partner</h4>
            <?php endif; ?>
        </div>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-info"></i>

                <h3 class="box-title">Transaction</h3>
            </div>
            <div class="form-active">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>

                        <th><a href="">Transaction</a></th>
                        <th><a href="">Data</a></th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($alltransaction)): ?>
                        <?php foreach ($alltransaction as $transaction): ?>
                            <tr>
                                <td><?= $transaction->transaction ?>$</td>
                                <td><?=  Yii::$app->formatter->asDatetime($transaction->datetime, "php:d.m.Y H:i"); ?> </td>

                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
    </section>
</div>
