<?php $this->title = 'Messages';
use yii\helpers\Html; ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Partners messages</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Messages</li>
        </ol>
    </section>




    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box box-warning direct-chat direct-chat-warning">
                        <div class="box-header with-border">

                            <?php if (empty($messagesToAdmin)): ?>
                            <h3 class="box-title">Direct Chat</h3>

                            <div class="box-tools pull-right">
                               <?php else: ?>
                                <h3 class="box-title"><?= $partner->name . ' ' . $partner->lastname?></h3>

                                <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts"
                                        data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                            </div>
                        </div>
                        <div class="box-body">

                            <div class="direct-chat-messages">

                                <?php foreach ($messagesToAdmin as $key => $dialog): ?>
                                    <?php if ($dialog->fromuserid == 3): ?>
                                        <div class="direct-chat-msg right">

                                            <div class="direct-chat-info clearfix">
                                                <span class="direct-chat-name pull-right">Admin</span>
                                                <span
                                                    class="direct-chat-timestamp pull-left"><?= Yii::$app->formatter->asDatetime($dialog->datetime, "php:d.m.Y H:i"); ?></span>
                                            </div>
                                            <img class="direct-chat-img"
                                                 src="http://meliaagency.com/web/images/5e3affe0.png"
                                                 alt="message user image"><!-- /.direct-chat-img -->
                                            <div class="direct-chat-text">
                                                <?= $dialog->text ?>
                                            </div>
                                        </div>

                                    <?php else: ?>
                                        <div class="direct-chat-msg">
                                            <div class="direct-chat-info clearfix">
                                                <span
                                                    class="direct-chat-name pull-left"><?= $partner->name . " " . $partner->lastname ?></span>
                                                <span
                                                    class="direct-chat-timestamp pull-right"><?= Yii::$app->formatter->asDatetime($dialog->datetime, "php:d.m.Y H:i"); ?></span>
                                            </div>
                                            <?php $file_headers = @get_headers('http://meliapartner.com/web/' . $avatar); ?>
                                            <?php if (!empty($avatar) && $file_headers[0] == 'HTTP/1.1 200 OK'): ?>
                                                <img class="direct-chat-img"
                                                     src="http://meliapartner.com/web/<?= $avatar ?>"
                                                     alt="message user image"><!-- /.direct-chat-img -->
                                            <?php else: ?>
                                                <img class="direct-chat-img"
                                                     src="http://meliaagency.com/web/images/no-image.jpeg">
                                            <?php endif; ?>

                                            <div class="direct-chat-text">
                                                <?= $dialog->text ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                            </div>


                            <div class="direct-chat-contacts">
                                <ul class="contacts-list">
                                    <?php for ($i = 0; $i < count($partners); $i++): ?>
                                        <li>
                                            <form method="post">
                                                <a class="view-dialog" href="pmessages?id=<?= $partners[$i]->userid ?>">
                                                    <input type="hidden" value="<?= $partners[$i]->id ?>">
                                                    <?php $file_headers = @get_headers('http://meliapartner.com/web/' . $partners[$i]->avatar); ?>
                                                    <?php if (!empty($partners[$i]->avatar) && $file_headers[0] == 'HTTP/1.1 200 OK'): ?>
                                                        <img class="contacts-list-img"
                                                             src="http://meliapartner.com/web/<?= $partners[$i]->avatar ?>">
                                                    <?php else: ?>
                                                        <img class="contacts-list-img"
                                                             src="http://meliaagency.com/web/images/no-image.jpeg">
                                                    <?php endif; ?>


                                                    <div class="contacts-list-info">
                                            <span class="contacts-list-name">
                                              <?= $partners[$i]->name . " " . $partners[$i]->lastname ?>
                                                <small
                                                    class="contacts-list-date pull-right"> <?= Yii::$app->formatter->asDatetime($texts[$i]->datetime, "php:d.m.Y H:i"); ?></small>

                                            </span>
                                                        <span
                                                            class="contacts-list-msg"><?= substr($texts[$i]->text, 0, 275) ?></span>
                                                    </div>
                                                </a>
                                            </form>
                                        </li>
                                    <?php endfor; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="box-footer">
                            <form onsubmit="return false;">
                                <div class="input-group">
                                    <input type="text" name="message" placeholder="Type Message ..." class="form-control text-area">
                            </form>

                          <span class="input-group-btn">
                              <button type="button" data-typeid="2" data-userid="<?= $partner->userid; ?>" class="btn btn-warning btn-flat send-message">
                                  Send
                              </button>
                          </span>
                            <?php endif; ?>
                        </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

</div>
