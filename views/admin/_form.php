<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Staticpages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-header ui-sortable-handle">
        <i class="fa fa-paperclip"></i>

        <h3 class="box-title">Static Pages</h3>

        <div class="box-tools pull-right">
        </div>
    </div>
    <div class="form-active">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'title_es')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'title_cz')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'text_ru')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'text_en')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'text_es')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'text_cz')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'is_active')->checkbox() ?>

        <div class="form-group">
            <?= Html::a('Cancel', ['staticpages', 'id' => $model->id], ['class' => 'btn btn-warning ']) ?>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-lg' : 'btn btn-primary ']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
