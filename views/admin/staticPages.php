<?php $this->title = 'Static pages'; ?>
<? use yii\helpers\Html;
use yii\grid\GridView; ?>
<div class="content-wrapper">
    <section class="content-header">
    </section>

    <section class="content">

        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-paperclip"></i>

                <h3 class="box-title">Static Pages</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //            'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],


                        [
                            'attribute' => 'title_ru',
                            'label' => 'Title',
                            'headerOptions' => ['width' => '200'],
                        ],
                        'text_ru:ntext',
                        [
                            'attribute' => 'is_active',
                            'format' => 'boolean',
                            'label' => 'Active',
                            'options' => ['width' => '70'],

                        ],


                        ['class' => 'yii\grid\ActionColumn',
                            'template' => '{view}{update}',
                            'options' => ['width' => '50'],
                        ],
                    ],
                ]); ?>

            </div>
        </div>

    </section>

</div>