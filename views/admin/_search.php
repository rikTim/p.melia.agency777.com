<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StaticpagesSeach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-wrapper">
    <section class="content-header">
    </section>

    <section class="content">

        <?php $form = ActiveForm::begin([
            'action' => ['staticPages'],
            'method' => 'get',
        ]); ?>

        <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'is_active') ?>

        <?= $form->field($model, 'text') ?>

        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>


    </section>

</div>