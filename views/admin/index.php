<?php $this->title = 'MeliaAGENSY'; ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Main</h1>
    </section>

    <section class="content charts">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-female"></i>

                        <h3 class="box-title">Girls statistic</h3>
                        <div class="box-tools">
                            <p>
                        <span class="girlsStatistic">
                            <span class="label label-primary">All <span class="allCount">0</span></span>
                            <span class="label bg-yellow">New <span class="newCount">0</span></span>
                            <span class="label bg-green">Approved <span class="approveCount">0</span></span>
                            <span class="label bg-red">Rejected <span class="rejectedCount">0</span></span>
                        </span>
                            </p>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="girlsStatistic" style="height:230px"></canvas>
                        </div>
                        <div class="input-block">
                            <label for="filterFrom"><?= Yii::t('app', 'From') ?></label>
                            <input type="text" value="<?= $from; ?>" class="filterFrom selectData">
                            <label for="filterTo"><?= Yii::t('app', 'To') ?></label>
                            <input type="text" value="<?= $to; ?>" class="filterTo selectData">
                            <input type="hidden" class="url" value="getgirlstatistics">
                            <span class="btn btn-primary girlsFilterBtn filterBtn"><?= Yii::t('app', 'Filter') ?></span>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-male"></i>

                        <h3 class="box-title">Partners statistic</h3>
                        <div class="box-tools">
                            <p>
                        <span class="partnersStatistic">
                            <span class="label label-primary">All <span class="allCount">0</span></span>
                            <span class="label bg-yellow">New <span class="newCount">0</span></span>
                            <span class="label bg-green">Approved <span class="approveCount">0</span></span>
                            <span class="label bg-red">Rejected <span class="rejectedCount">0</span></span>
                        </span>
                            </p>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="partnersStatistic" style="height:230px"></canvas>
                        </div>
                        <div class="input-block">
                            <label for="filterFrom"><?= Yii::t('app', 'From') ?></label>
                            <input type="text" value="<?= $from; ?>" class="filterFrom selectData">
                            <label for="filterTo"><?= Yii::t('app', 'To') ?></label>
                            <input type="text" value="<?= $to; ?>" class="filterTo selectData">
                            <input type="hidden" class="url" value="getpartnerstatistics">
                            <span
                                class="btn btn-primary partnerFilterBtn filterBtn"><?= Yii::t('app', 'Filter') ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
