<?php
$this->title = 'Balances';
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use app\models\Money;
use app\models\Partners;

?>

<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-money"></i>

                <h3 class="box-title">Balances</h3>
            </div>
            <div class="box-body">
                <div class="form-group">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [


                            ['attribute' => 'id',
                                'label' => 'ID',
                                'filter' => false,],

                            ['attribute' => 'userid',
                                'label' => 'Name',
                                'format'=>'raw',
                                'value' => function($data){
                                    return Html::a($data->partner->name,['partners/view?id='.$data->getIdPartner($data->userid)] );},
                                'filter' => ArrayHelper::map(Partners::find()->select(['userid', 'name'])->all(), 'userid', 'name'),
                                'options' => ['width' => '500']
                            ],

                            ['attribute' => 'userid',
                                'label' => 'Lastname',
                                'format'=>'raw',
                                'value' => function($data){
                                    return Html::a($data->partner->lastname,['partners/view?id='.$data->getIdPartner($data->userid)] );},
                                'filter' => ArrayHelper::map(Partners::find()->select(['userid', 'lastname'])->all(), 'userid', 'lastname'),
                                'options' => ['width' => '500']
                            ],


                            ['attribute' => 'getmoneytype',
                                'label' => 'Money Type',
                                'filter' => ArrayHelper::map(Money::find()->select(['id', 'title'])->all(), 'id', 'title'),
                                'value' => 'moneytype.title',
                                'options' => ['width' => '400'],
                            ],


                            [
                                'attribute' => 'id',
                                'label' => '',
                                'format' => 'raw',
                                'filter' => false,
                                'value' => function ($data) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['admin/balance?id=' . $data->id]);
                                },
                                'options' => ['width' => '70'],


                            ],
                        ],
                    ]); ?>

                    <a href="<?= Url::toRoute(['paymentbill']) ?>" class="btn btn-success">Выписка по выплатам</a>

                </div>
            </div>
        </div>
    </section>
</div>
