<?php

?>

<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-cogs"></i>

                <h3 class="box-title">Statistic</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="chartStatistics">
                        <canvas id="registerStatistics" style="height:230px"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
