<?php $this->title = 'Main settings'; ?>
<? use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm; ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1></h1>
    </section>

    <section class="content">

        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-cogs"></i>

                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <form role="form">
                <div class="box-body">
                    <div class="form-group">


                        <table class="table table-striped table-bordered">
                            <thead>
                            <thead>
                            <tr>
                                <th>Site</th>

                                <th>Image</th>
                                <th width="75%">Text</th>
                                <th> &nbsp</th>
                            </thead>
                            <tbody>
                            <?php if (!empty($melia)): ?>
                                <tr>

                                    <td>Melia</td>

                                    <td><img src="http://meliaagency.com/web/<?= $melia ?>" style="width: 200px"
                                             alt="">
                                    </td>


                                    <?php if (!empty($textmelia)): ?>
                                        <td><?= $textmelia ?></td>
                                    <?php endif; ?>
                                    <td>
                                        <?= Html::a('Update', 'headmelia', ['class' => 'btn btn-primary']) ?>


                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if (!empty($pmelia)): ?>
                                <tr>

                                    <td>P.Melia</td>

                                    <td><img src="http://meliaagency.com/web/<?= $pmelia ?>" style="width: 200px">
                                    </td>


                                    <?php if (!empty($textpmelia)): ?>
                                        <td><?= $textpmelia ?></td>
                                    <?php endif; ?>
                                    <td>
                                        <?= Html::a('Update', 'headpmelia', ['class' => 'btn btn-primary']) ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>

        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-money"></i>

                <h3 class="box-title">Money</h3>
            </div>
            <form role="form">
                <div class="box-body">
                    <div class="form-group">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                'id',
                                'title',
                                'money',

                                ['class' => 'yii\grid\ActionColumn',
                                    'template' => '{update}',
                                    'options' => ['width' => '50'],
                                    'buttons' => [
                                        'update' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                                'title' => Yii::t('app', 'Update'),
                                            ]);
                                        }
                                    ],
                                    'urlCreator' => function ($action, $model, $key, $index) {
                                        if ($action === 'update') {
                                            $url = 'updatemoney?id=' . $model->id;
                                            return $url;
                                        }
                                    }
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>