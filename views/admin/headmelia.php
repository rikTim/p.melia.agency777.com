<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Staticpages */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Main Image';
?>
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form">
                <div class="box-body">
                    <div class="form-group">
                        <div hidden>
                            <?php
                            $form = ActiveForm::begin([
                                'id' => 'avatar',
                                'options' => ['enctype' => 'multipart/form-data'],
                                'action' => 'headmelia'
                            ]);
                            ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'avatar',
                            'options' => ['enctype' => 'multipart/form-data'],
                            'action' => 'headmelia'
                        ]);
                        ?>
                        <?= $form->field($text, 'text')->textInput(['maxlength' => true, 'value' => $text->text]) ?>

                        <?= $form->field($text, 'is_active')->checkbox() ?>

                        <p>Файл должен быть разрешения *.jpg, *.png, *.jpeg и размером 2048x1600</p>

                        <?= $form->field($model, 'imageFile')->fileInput() ?>


                        <div class="form-group">
                            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>


                        </div>
                        <?php ActiveForm::end(); ?>


                    </div>
                </div>
    </section>

</div>
