<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Staticpages */
$this->title = $model->title_en;

?>
<div class="content-wrapper">
    <section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-paperclip"></i>

                <h3 class="box-title">Static Pages</h3>

                <div class="box-tools pull-right">
                    <?= Html::a('Cancel', ['staticpages', 'id' => $model->id], ['class' => 'btn btn-warning ']) ?>

                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary ']) ?>
                </div>
            </div>
            <div class="form-active">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'title_ru',
                        'title_en',
                        'title_es',
                        'title_cz',
                        'text_ru:ntext',
                        'text_en:ntext',
                        'text_es:ntext',
                        'text_cz:ntext',
                        ['attribute' => 'is_active',
                            'format' => 'boolean',
                            'label' => 'Active',],
                    ],
                ]) ?>
            </div>
        </div>
    </section>

</div>