<?php
$this->title = 'No answered messages';
use yii\helpers\Url;
?>

<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-envelope"></i>
                <h3 class="box-title">No answered messages</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th><a href="">Girl</a></th>
                            <th><a href="">Man</a></th>
                            <th><a href="">Message</a></th>
                            <th><a href="">Date</a></th>
                            <th><a href="">Answer</a></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($messages)): ?>
                            <?php foreach ($messages as $message): ?>
                                <tr>
                                    <td style="width: 7%"><?= $message->tdGirl->name . ' ' . $message->tdGirl->lastname; ?> <small><?= $message->girl_id; ?></small></td>
                                    <td style="width: 7%"><?= $message->men_name; ?> <small><?= $message->men_id; ?></small></td>
                                    <td><?= $message->text; ?></td>
                                    <td style="width: 7%"><?= Yii::$app->formatter->asDatetime($message->datetime, "php:d.m.Y H:i"); ?></td>
                                    <td><a class="btn btn-primary" href="<?= Url::toRoute(['/topdates/update', 'id' => $message->id]) ?>">Answer</a></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
