<?php
    $this->title = 'Payment bill';
?>

<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-money"></i>

                <h3 class="box-title">Balances</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Partner</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Phone</th>
                                <th>Payment method</th>
                                <th>Other</th>
                                <th>Total $</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($paymentbill)): ?>
                            <?php foreach($paymentbill as $payment): ?>
                                <tr>
                                    <td><?= $payment['partner']; ?></td>
                                    <td><?= $payment['country']; ?></td>
                                    <td><?= $payment['city']; ?></td>
                                    <td><?= $payment['phone']; ?></td>
                                    <td><?= $payment['payment method']; ?></td>
                                    <td><?= $payment['other']; ?></td>
                                    <td><?= $payment['total']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="7">В этом месяце еще нет выписок</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>

                    <form action="getexcelbill">
                        <button type="submit" class="btn btn-success">Экспортировать в excel</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
