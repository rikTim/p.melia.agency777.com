<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactsSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-envelope"></i>

                <h3 class="box-title">Contacts</h3>
            </div>
            <div class="box-body">
                <div class="form-group">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'name',
                            'email:email',
                            [ 'attribute'=>'reason',
                                'value'=>function($data){
                                    return \app\models\Contacts::getReason($data->reason);
                                }
                            ],

                            'text:ntext',
                             'datetime:datetime',

//                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </section>
</div>
