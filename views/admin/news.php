<?php $this->title = 'News'; ?>
<? use yii\helpers\Html;
use yii\grid\GridView; ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('Create News', ['giftpages/create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,

            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'title',
                'text:ntext',
                'datetime:datetime',
                'is_active',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        </h1>
    </section>

    <section class="content">

    </section>

</div>