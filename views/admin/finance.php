<?php

use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Partners;
use yii\helpers\Html;

?>

<?php $this->title = 'Finance'; ?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-info"></i>


                <h3 class="box-title">Transaction </h3>
            </div>
            <div class="form-active">
                <?=  GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [

                        [
                            'attribute' =>'id',
                            'options' => ['width' => '250'],
                        ],



                          ['attribute' => 'userid',
                            'label' => 'Partner Name',
                            'format'=>'raw',
                            'filter' => ArrayHelper::map(Partners::find()->select(['userid', 'name'])->all(), 'userid', 'name'),
                            'value' => function($data){
                                return Html::a($data->partners->name,['partners/view?id='.$data->getIdPartner($data->userid)] );},
                            'options'=>[
                                'width' => '300',
                            ],

                        ],


                        ['attribute' => 'userid',
                            'label' => 'Partner Lastname',
                            'format'=>'raw',
                            'filter' => ArrayHelper::map(Partners::find()->select(['userid', 'lastname'])->all(), 'userid', 'lastname'),

                            'value' => function($data){
                                return Html::a($data->partners->lastname,['partners/view?id='.$data->getIdPartner($data->userid)] );},
                            'options'=>[
                                'width' => '300',
                            ],

                        ],





                        ['attribute' => 'transaction',
                            'filter' => false,
                        ],

                        [
                            'attribute'=>'datetime',
                            'format'=>'datetime',
                            'filter' => DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'datetime',
                                'template' => '{addon}{input}',
                                'clientOptions' => [
                                    'autoclose' => true,
                                    'format' => 'd/mm/yyyy'
                                ]
                            ]),
                            'options' => ['width' => '300'],

                        ],
                    ],
                ]); ?>

            </div>
        </div>


    </section>
</div>