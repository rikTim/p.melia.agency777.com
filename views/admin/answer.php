<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\ChatTopDates */

?>
<div class="content-wrapper">

    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">

                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <?php $form = ActiveForm::begin(); ?>

                <label class="control-label" for="chattopdates-text">Girl Name</label>
                <div class="message-men"><?=$girlName ?></div><br>

                <label class="control-label" for="chattopdates-text">Message Men</label>

                <div class="message-men"><?=$chat->text ?></div><br>

                <?= $form->field($newMessage, 'text')->textarea(['rows' => 8]) ?>

                <?= Html::submitButton( 'Send', ['class' =>  'btn btn-success']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>

</div>