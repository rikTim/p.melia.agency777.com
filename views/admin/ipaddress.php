<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Partners;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogsSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-cogs"></i>

                <h3 class="box-title">Ip Address</h3>
            </div>
            <div class="box-body">
                <div class="form-group">


                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'id',

                            ['attribute' => 'userid',
                                'label' => 'User ID',
                            ],

                            ['attribute' => 'id',
                                'label' => 'Partner Name',
                                'format'=>'raw',
                                'filter' => ArrayHelper::map(Partners::find()->select(['userid', 'name'])->all(), 'userid', 'name'),
                                'value' => function($data){
                                    return Html::a($data->partner->name,['partners/view?id='.$data->getIdPartner($data->userid)] );},
                                'options'=>[
                                    'width' => '200',
                                ],

                            ],


                            ['attribute' => 'id',
                                'label' => 'Partner Lastname',
                                'format'=>'raw',
                                'filter' => ArrayHelper::map(Partners::find()->select(['userid', 'lastname'])->all(), 'userid', 'lastname'),
                                'value' => function($data){
                                    return Html::a($data->partner->lastname,['partners/view?id='.$data->getIdPartner($data->userid)] );},
                                'options'=>[
                                    'width' => '200',
                                ],

                            ],


                            ['attribute' => 'acceptedtime',
                                'label' => 'Time',
                                'format' => 'datetime',
                                'filter' => DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'acceptedtime',
                                    'template' => '{addon}{input}',
                                    'clientOptions' => [
                                        'autoclose' => true,
                                        'format' => 'd/mm/yyyy'
                                    ]
                                ]),
                                'options' => ['width' => '300'],
                            ],
                            ['attribute' => 'acceptedip',
                                'label' => 'IP',
                                'filter' => false
                            ],
                            ['attribute' => 'acceptedip',
                                'label' => 'Country',
                                'value' => function ($data) {
                                    return \app\models\Logs::getCountry($data->acceptedip);
                                },
                                'filter' => false
                            ],

                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </section>
</div>
