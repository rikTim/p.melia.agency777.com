<?php

use yii\helpers\Html;

?>
<div class="content-wrapper">
    <section class="content-header">

        <h1><?= Html::encode($this->title) ?></h1>
    </section>

    <section class="content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>


    </section>

</div>