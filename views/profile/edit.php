<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\components\MenuWidget;

$this->title = 'Personal profile';
?>
<?php Yii::$app->language = $_COOKIE['lang'];?>
<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="<?= Url::toRoute(['index']); ?>"><?= Yii::$app->params['siteName']; ?></a></h1>
                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>
    <?= MenuWidget::widget(); ?>
    <section class="well center well__06 bg01 shadow profile">
        <div class="container">
            <div class="row">
                <div class="grid_12">
                    <div class="img coverImg edit">
                        <center>
                            <?php $filePath = Yii::$app->getUrlManager()->getBaseUrl() . '/' . $file; ?>
                            <?php if($model->statusid->id == 6) $class = ''; else $class = 'noApproved'; ?>
                            <?php $filePath = 'http://meliapartner.com/web/' . $file; ?>
                            <?php $file_headers = @get_headers('http://meliapartner.com/web/' . $file);?>
                            <?php if(!empty($file) && $file_headers[0] == 'HTTP/1.1 200 OK'):?>
                                <img class="<?= $class; ?>"  src="<?= $filePath; ?>">
                            <?php else: ?>
                                <img class="<?= $class; ?>" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/no-image-man.png" alt="">
                            <?php endif; ?>

                            <?php if(!empty($model->id)): ?>
                                <h3>
                                    <a href="<?= Url::toRoute(['profile/index']); ?>">
                                        <?= $model->name . ' ' . $model->lastname ?>
                                    </a>
                                </h3>
                                <small><?= Yii::t('app', 'Status')?>: <?= $model->statusid->title ?></small>
                                <?php
                                    $form = ActiveForm::begin([
                                        'id' => 'avatarForm',
                                        'options' => ['class' => 'notifications-form', 'enctype' => 'multipart/form-data'],
                                        'action' => 'uploadavatar'
                                    ]);
                                ?>
                                <?= $form->field($fileModel, 'imageFile')->fileInput(array('class' => 'girlAvatar', 'style' => 'display: none'))->label(false); ?>
                                <?= Html::submitButton('Edit',['class' => 'updateAvatar', 'style' => 'display: none']) ?>
                                <?php ActiveForm::end(); ?>

                                <p><i class="fa fa-map-marker"></i><?= $model->city . ', ' . $model->country ?></p>
                                <p>
                                    <span><i class="fa fa-envelope"></i> <?php if(!empty($model->email)) echo $model->email; ?></span>
                                    <span><i class="fa fa-phone"></i><?php if(!empty($model->phone)) echo $model->phone; ?> </span>
                                </p>
                            <?php endif; ?>
                        </center>
                        <center style="color:#f00;">
                            <?= Yii::$app->session->getFlash('error'); ?>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="well well__03 profile profile-form">
        <div class="container">
            <?php
            $form = ActiveForm::begin([
                'id' => 'profileForm',
                'options' => ['class' => 'partner-edit-form', 'enctype' => 'multipart/form-data']
            ]);
            ?>

            <div class="step step_1">
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="controlHolder">
                                <div class="tmInput">
                                    <?= $form->field($model, 'name')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Name')])->label(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?= $form->field($model, 'lastname')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Lastname')])->label(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?= $form->field($model, 'phone')->textInput(['id' => 'phoneMask', 'class' => 'form-item', 'placeholder' => Yii::t('app','Phone')])->label(Yii::t('app','Phone')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?= $form->field($model, 'email')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Email')])->label(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <label class="control-label" for="girls-country"><?= Yii::t('app','Country')?></label>
                                <select name="Partners[country]" class="country-list-chosen">
                                    <option value=""><?= $model->country; ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <label class="control-label" for="girls-city"><?= Yii::t('app','City')?></label>
                                <select name="Partners[city]" class="city-list-chosen">
                                    <option value=""><?= $model->city; ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?php echo $form->field($model, 'pay_metod')->dropDownList($payMoney)->label(Yii::t('app','Payment method')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?= $form->field($model, 'pay_data')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Payment information')])->label(Yii::t('app','Payment information')); ?>
                            </div>
                        </div>
                    </div>
                </div>
<!--                <div class="row">-->
<!--                    <div class="grid_12">-->
<!--                        <div class="iconed-box">-->
<!--                            <div class="tmInput">-->
<!--                                --><?//= $form->field($model, 'passportnumber')->textInput(['class' => 'form-item', 'placeholder' =>Yii::t('app','ID / Passport number') ])->label( Yii::t('app','Passport number')); ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>

            <div class="control-btns">
                <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id])->label(false);?>
                <input type="hidden" value="<?= $model->country; ?>" class="partnersCountry" name="Partners[country]">
                <input type="hidden" value="<?= $model->city; ?>" class="partnersCity" name="Partners[city]">
                <?= Html::submitButton(Yii::t('app','Save profile'), ['class' => 'btn save-partner-profile', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>

</main>
