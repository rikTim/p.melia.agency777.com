<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\components\MenuWidget;

$this->title = 'Personal profile';
?>
<?php Yii::$app->language = $_COOKIE['lang'];?>
<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="<?= Url::toRoute(['index']); ?>"><?= Yii::$app->params['siteName']; ?></a></h1>
                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>
    <?= MenuWidget::widget(); ?>
    <section class="well center well__06 bg01 shadow profile">
        <div class="container">
            <div class="row">
                <div class="grid_12">
                    <div class="img coverImg">
                        <center>
                            <?php $filePath = 'http://meliapartner.com/web/' . $file; ?>
                            <?php $file_headers = @get_headers('http://meliapartner.com/web/' . $file);?>
                            <?php if(!empty($file) && $file_headers[0] == 'HTTP/1.1 200 OK'):?>
                                <img  src="<?= $filePath; ?>">
                            <?php else: ?>
                                <img  src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/no-image-man.png" alt="">
                            <?php endif; ?>


                            <?php if(!empty($model->id)): ?>
                                <h3><a href="<?= Url::toRoute(['profile/index']); ?>"><?= $model->name . ' ' . $model->lastname ?></a></h3>

                                    <small><?= Yii::t('app','Status')?>: <?= $model->statusid->title ?></small>

                                <p><i class="fa fa-map-marker"></i><?= $model->city . ', ' . $model->country ?></p>
                                <p>
                                    <span><i class="fa fa-envelope"></i> <?php if(!empty($model->email)) echo $model->email; ?></span>
                                    <span><i class="fa fa-phone"></i><?php if(!empty($model->phone)) echo $model->phone; ?> </span>
                                </p>
                            <?php endif; ?>
                            <p>
                                <span><a href="<?= Url::toRoute(['edit']); ?>"><i class="fa fa-cogs"></i><?= Yii::t('app','Edit profile')?></a></span>
                            </p>
                        </center>
                        <center style="color:#f00;">
                            <?= Yii::$app->session->getFlash('error'); ?>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="well well__03 profile profile-form m-height">
        <div class="container">
            <?php $form = ActiveForm::begin([
                'id' => 'notificationForm',
                'options' => ['class' => 'notifications-form', 'enctype' => 'multipart/form-data']]);
            ?>
            <div class="step step_1">
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="controlHolder">
                                <div class="tmInput">
                                    <b><?= Yii::t('app','Name')?>: </b><?= $model->name; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Lastname')?>: </b><?= $model->lastname; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Phone')?>: </b><?= $model->phone; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Email')?>: </b><?= $model->email; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Country')?>: </b><?= $model->country; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','City')?>: </b><?= $model->city; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Payment method')?>: </b><?= $model->pay_metod; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Payment information')?>: </b><?= $model->pay_data; ?>
                            </div>
                        </div>
                    </div>
                </div>
<!--                <div class="row">-->
<!--                    <div class="grid_6">-->
<!--                        <div class="iconed-box">-->
<!--                            <div class="tmInput">-->
<!--                                <b>--><?//= Yii::t('app','Passport number')?><!--: </b>--><?//= $model->passportnumber; ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="grid_6">-->
<!--                        <div class="iconed-box">-->
<!--                            <div class="tmInput">-->
<!--                                <b>--><?//= Yii::t('app','Get money type')?><!--: </b>--><?//= $model->getmoneytype; ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
    </section>
</main>
