<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Girls */

$this->title = 'Create Girls';
$this->params['breadcrumbs'][] = ['label' => 'Girls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-wrapper">
    <section class="content-header">
    </section>

    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-user"></i>
                <h3 class="box-title">Personal Info</h3>
                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'education')->textInput() ?>

                <?= $form->field($model, 'occupation')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'birthdate')->textInput() ?>

                <?= $form->field($model, 'religionid')->dropDownList($religions)->label('Religion') ?>

                <?= $form->field($model, 'passportnumber')->textInput() ?>

                <?= $form->field($fileModel,'imageFile')->fileInput(['class' => 'form-item'])?>

                <?= $form->field($fileModel, 'identVideo')->fileInput(['class' => 'form-item']); ?>

                <?= $form->field($model, 'materialstatusid')->dropDownList($statuses)->label('Materialstatus') ?>

                <?= $form->field($model, 'height')->textInput() ?>

                <?= $form->field($model, 'weight')->textInput() ?>

                <?= $form->field($model, 'eyescolor')->dropDownList($eyescolor) ?>

                <?= $form->field($model, 'haircolor')->dropDownList($haircolor) ?>

                <?= $form->field($model, 'alcohol')->checkbox() ?>

                <?= $form->field($model, 'children')->checkbox() ?>

                <?=$form->field($model, 'smoking')->checkbox() ?>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-map"></i>
                <h3 class="box-title">Contacts</h3>
                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <?= $form->field($model, 'country')->textInput()->label('Country') ?>

                <?= $form->field($model, 'city')->textInput()->label('City') ?>

                <?= $form->field($model, 'fblink')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'vklink')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'livingaddress')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'postaddress')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-info"></i>
                <h3 class="box-title">Info</h3>
                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">

                <?= $form->field($model, 'characterid')->dropDownList($characters)->label('Character') ?>

                <?= $form->field($model, 'interests')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'mansdescr')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'firstletter')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-star-o"></i>
                <h3 class="box-title">Other</h3>
                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">

                <?= $form->field($model, 'topdatesid')->textInput() ?>

                <?= $form->field($model, 'partnerid')->textInput()->label('Partner') ?>

                <?= $form->field($model, 'statusid')->dropDownList($users)->label('Status') ?>

                <?= $form->field($model, 'is_active')->checkbox() ?>

                <?= $form->field($model, 'is_aproved')->checkbox() ?>

                <?= $form->field($model, 'is_accepted')->checkbox() ?>


            </div>
        </div>

            <?= Html::a('Cancel', ['index','id' => $model->id],['class' => 'btn btn-warning ']) ?>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn  btn-success' : 'btn btn-primary']) ?>


        <?php ActiveForm::end(); ?>
    </section>

</div>
