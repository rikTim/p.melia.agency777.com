<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Girls */

$this->title = 'Update Girls: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Girls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="content-wrapper">
    <section class="content-header">

        <h1><?= Html::encode($this->title) ?></h1>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-user"></i>

                <h3 class="box-title">Personal Info</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <?php $form = ActiveForm::begin(); ?>

                <div class="columns-one ">

                    <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'birthdate')->textInput() ?>

                    <?= $form->field($model, 'passportnumber')->textInput()->label('Passport Number') ?>

<!---->
<!--                    --><?php //if (!empty($statuses)): ?>
<!--                        --><?//= $form->field($model, 'materialstatusid')->dropDownList($statuses)->label('Material Status') ?>
<!--                    --><?php //endif; ?>
                    <?= $form->field($model, 'materialstatusid')->textInput()->label('Material Status') ?>


                    <?= $form->field($model, 'height')->textInput() ?>

                    <?= $form->field($model, 'weight')->textInput() ?>

<!--                    --><?php //if (!empty($eyescolor)): ?>
<!--                        --><?//= $form->field($model, 'eyescolor')->dropDownList($eyescolor)->label('Eyes Color') ?>
<!--                    --><?php //endif; ?>
                    <?= $form->field($model, 'eyescolor')->textInput()->label('Eyes Color') ?>

<!--                    --><?php //if (!empty($haircolor)): ?>
<!--                        --><?//= $form->field($model, 'haircolor')->dropDownList($haircolor)->label('Hair Color') ?>
<!--                    --><?php //endif; ?>
                    <?= $form->field($model, 'haircolor')->textInput()->label('Hair Color') ?>





                                    </div>


                                    <div class="columns-two" style="margin-bottom: -20px">

                    <!--                    --><?php //if (!empty($education)): ?>
<!--                        --><?//= $form->field($model, 'education')->dropDownList($education) ?>
<!--                    --><?php //endif; ?>
                    <?= $form->field($model, 'education')->textInput() ?>

<!--                    --><?php //if (!empty($occupation)): ?>
<!--                        --><?//= $form->field($model, 'occupationid')->dropDownList($occupation)->label('Occupation') ?>
<!--                    --><?php //endif; ?>
                    <?= $form->field($model, 'occupationid')->textInput()->label('Occupation') ?>

<!--                    --><?php //if (!empty($religions)): ?>
<!--                        --><?//= $form->field($model, 'religionid')->dropDownList($religions)->label('Religion') ?>
<!--                    --><?php //endif; ?>
                    <?= $form->field($model, 'religionid')->textInput()->label('Religion') ?>

                    <p><label class="control-label">Passport Image</label></p>
                    <?php $file_headers = @get_headers('http://meliaagency.com/web/' . $file); ?>
                    <?php if (!empty($file) && $file_headers[0] == 'HTTP/1.1 200 OK'): ?>
                        <div class="image-user">

                            <img src="<?= 'http://meliaagency.com/web/' . $file ?>">
                        </div>
                    <?php else: ?>
                        <div class="image-user">
                            <img src="http://meliaagency.com/web/images/no-image.jpeg">
                        </div>
                    <?php endif; ?>
                    <div class="clr"></div>
                    <div class="video">
                        <p><label class="control-label">Identification Video</label></p>
                        <video controls>

                            <source src='http://meliaagency.com/web/<?= $videolink; ?>'>
                        </video>
                        <div class="clr"></div>
                        <a href="http://meliaagency.com/web/<?= $videolink; ?>" class="btn btn-warning" download="http://meliaagency.com/web/<?= $videolink; ?>" >Download video</a>
                    </div>

                </div>


            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-map"></i>

                <h3 class="box-title">Contacts</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">

                <div class="columns-one">

                    <?= $form->field($model, 'country')->textInput()->label('Country') ?>

                    <?= $form->field($model, 'fblink')->textInput(['maxlength' => true])->label('Fb Link') ?>

                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'livingaddress')->textInput(['maxlength' => true])->label('Living Adress') ?>

                </div>

                <div class="columns-two" style="padding-bottom: 40px">

                    <?= $form->field($model, 'city')->textInput()->label('City') ?>

                    <?= $form->field($model, 'vklink')->textInput(['maxlength' => true])->label('Vk Link') ?>

                    <?= $form->field($model, 'postaddress')->textInput(['maxlength' => true])->label('Post Adress') ?>

                </div>


            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-info"></i>

                <h3 class="box-title">Info</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <div class="column">

                    <?php if (!empty($characters)): ?>
                        <?= $form->field($model, 'characterid')->dropDownList($characters)->label('Character') ?>
                    <?php endif; ?>


                    <?= $form->field($model, 'interests')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'mansdescr')->textarea(['rows' => 6])->label('Mans Descr') ?>

                    <?= $form->field($model, 'firstletter')->textarea(['rows' => 6])->label('First Letter') ?>

                </div>
            </div>
        </div>


        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-camera"></i>

                <h3 class="box-title">Photos</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <?php $file_headers = @get_headers('http://meliaagency.com/web/' . $avatar); ?>
                <?php if (!empty($avatar) && $file_headers[0] == 'HTTP/1.1 200 OK'): ?>
                    <p><label class="control-label">Main Image</label></p>
                    <img data-avatarid="<?= $avatarKey; ?>" class="girl-avatar"
                         src="<?= 'http://meliaagency.com/web/' . $avatar ?>" alt="">
                <?php else: ?>
                    <img src="http://meliaagency.com/web/images/no-image.jpeg">
                <?php endif; ?>
                <div class="clr"></div>

                <div class="all-image">
                    <?php if (!empty($paths)): ?>
                        <p><label class="control-label">All Image</label></p>
                        <?php foreach ($paths as $key => $path): ?>
                            <div class="image-user">
                                <div class="icons">
                                    <i data-id="<?= $key ?>" class="delete-photo fa fa-trash-o"></i>
                                    <i data-id="<?= $key ?>" class="set-avatar fa fa-star"></i>

<!--                                    <i data-id="--><?//= $key ?><!--" class="rotate-photo fa fa-undo"></i>-->

                                </div>
                                    <span class="photo">
                                        <a class="profile-gallery-item"
                                           href="<?= 'http://meliaagency.com/web/' . $path; ?>"
                                           data-fancybox-group="gallery" title="">
                                            <img src="<?= 'http://meliaagency.com/web/' . $path; ?>" alt="">
                                        </a>
                                    </span>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>


            </div>
        </div>


        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-star-o"></i>

                <h3 class="box-title">Other</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <div class="column">

                    <?= $form->field($model, 'topdatesid')->textInput()->label('TopDates ID') ?>

<!--                    --><?//= $form->field($model, 'partnerid')->textInput(['disabled'=>'disabled'])->label('Partner ID') ?>

                    <?= $form->field($model, 'partnerid')->hiddenInput()->label(false) ?>


                    <?php if (!empty($users)): ?>
                        <?= $form->field($model, 'statusid')->dropDownList($users)->label('Status') ?>
                    <?php endif; ?>

                    <?= $form->field($model, 'is_active')->checkbox() ?>

                    <?= $form->field($model, 'userid')->hiddenInput()->label(false) ?>
<!---->
<!--                    --><?//= $form->field($model, 'is_aproved')->checkbox() ?>
<!---->
<!--                    --><?//= $form->field($model, 'is_accepted')->checkbox() ?>


                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-question"></i>

                <h3 class="box-title">Update</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <div class="column">

                    <?php if (!empty($messages)): ?>
                        <?= $form->field($messages, 'text')->textArea(['rows' => 6]); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>


        <div class="form-group">
            <?= Html::a('Cancel', ['index', 'id' => $model->id], ['class' => 'btn btn-warning ']) ?>
            <?= Html::submitButton( 'Update', ['class' =>   'btn btn-primary ']) ?>
            <?= Html::button( 'Push to Top-dates', ['class' =>   'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </section>

</div>
