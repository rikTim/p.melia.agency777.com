<?php


use yii\grid\GridView;
use app\models\Statuses;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Partners;


/* @var $this yii\web\View */
/* @var $searchModel app\models\GirlsSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Girls';
?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-female"></i>
                <h3 class="box-title">Girls</h3>
                <div class="box-tools">
                    <p>
                        <span>
                            <span class="label  label-primary">All <?=$allGirl?></span>
                            <span class="label  bg-yellow">New <?=$newGirl?></span>
                            <span class="label  bg-green">Approved <?=$approvedGirl?></span>
                            <span class="label  bg-red">Rejected <?=$rejectedGirl?></span>
                        </span>
                    </p>
                </div>
            </div>
            <div class="form-active">


                <?= GridView::widget([

                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [

                        ['attribute' => 'id',
                            'label' => 'ID',
                            'filter' => false,],
                        [
                            'attribute'=>'name',
                            'format'=>'html',
                            'value'=> function($data)
                            {
                                return  '<a class="thumbnail" href="#">'.$data->name.'<span><img src="http://meliaagency.com/web/'.$data->getAvatar($data->userid).'" /></span></a>';
                            }
                        ],
                        'name',
                        'lastname',
                        ['attribute' => 'topdatesid',
                            'label' => 'TopDates ID',
                        ],

                        ['attribute' => 'partnerid',
                            'label' => 'Partner Name',
                            'format'=>'raw',
                            'filter' => ArrayHelper::map(Partners::find()->select(['id', 'name'])->all(), 'id', 'name'),
                            'value' => function($data){
                                return Html::a($data->partner->name,['partners/view?id='.$data->partnerid] );},
                            'options'=>[
                                'width' => '200',
                            ],

                        ],


                        ['attribute' => 'partnerid',
                            'label' => 'Partner Lastname',
                            'format'=>'raw',
                            'filter' => ArrayHelper::map(Partners::find()->select(['id', 'lastname'])->all(), 'id', 'lastname'),
                            'value' => function($data){
                                return Html::a($data->partner->lastname,['partners/view?id='.$data->partnerid] );},
                            'options'=>[
                                'width' => '200',
                            ],

                        ],





                        [ 'attribute'=>'id',
                            'label'=> 'Languages',
                            'value'=>function($data){
                                return $data->getLanguagesGirl($data->id);
                            },
                            'filter'=>false,
                        ],


                        ['attribute' => 'statusid',
                            'label' => 'Status',
                            'filter' => ArrayHelper::map(Statuses::find()->select(['id', 'title'])->all(), 'id', 'title'),
//                            'value' =>'statusidTitle',
                            'value' =>'status.title',
                        ],
                        ['class' => 'yii\grid\ActionColumn',
                            'template' => '{update}{delete}',
                            'options' => ['width' => '50'],
                        ],


                    ],
                ]); ?>
            </div>
        </div>
    </section>
</div>
