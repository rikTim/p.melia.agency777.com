<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Girls */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="girls-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'topdatesid')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birthdate')->textInput() ?>

    <?= $form->field($model, 'passportnumber')->textInput() ?>

    <?= $form->field($model, 'passportimage')->textInput() ?>

    <?= $form->field($model, 'height')->textInput() ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'eyescolor')->dropDownList(/*$eyescolor*/) ?>

    <?= $form->field($model, 'alcohol')->checkbox() ?>

    <?= $form->field($model, 'materialstatusid')->dropDownList(/*$statuses*/) ?>

    <?= $form->field($model, 'smoking')->checkbox() ?>

    <?= $form->field($model, 'children')->checkbox() ?>

    <?= $form->field($model, 'identificationvideo')->textInput() ?>

    <?= $form->field($model, 'occupation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'religionid')->dropDownList(/*$religions*/) ?>

    <?= $form->field($model, 'country')->textInput() ?>

    <?= $form->field($model, 'city')->textInput() ?>

    <?= $form->field($model, 'livingaddress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postaddress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'characterid')->dropDownList(/*$characters*/) ?>

    <?= $form->field($model, 'interests')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'mansdescr')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'firstletter')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fblink')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vklink')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'partnerid')->textInput() ?>

    <?= $form->field($model, 'statusid')->textInput() ?>

    <?= $form->field($model, 'is_active')->textInput() ?>

    <?= $form->field($model, 'is_aproved')->textInput() ?>

    <?= $form->field($model, 'is_accepted')->textInput() ?>

    <div class="form-group">
        <?= Html::a('Cancel', ['index', 'id' => $model->id], ['class' => 'btn btn-warning ']) ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
