<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Girls */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Girls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    th {
        width: 40%;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">

        <h1><?= Html::encode($this->title) ?></h1>
    </section>


    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-user"></i>

                <h3 class="box-title">Personal Info</h3>

                <div class="box-tools pull-right">

                </div>
                <div class="box-tools pull-right">
                </div>
            </div>


            <div class="form-active">

                <table class="table table-striped table-bordered detail-view">

                    <tbody>

                    <tr>
                        <th>User Name (login)</th>
                        <td><?= $username ?></td>
                    </tr>

                    <tr>
                        <th>Name</th>
                        <td><?= $model->name ?></td>
                    </tr>

                    <tr>
                        <th>Lastname</th>
                        <td><?= $model->lastname ?></td>
                    </tr>

                    <tr>
                        <th>Education</th>
                        <td><?= $model->education ?></td>
                    </tr>
                    <?php if (!empty($occupation)): ?>
                        <tr>
                            <th>Occupation</th>
                            <td><?= $occupation['title'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <th>Birthdate</th>
                        <td><?= $model->birthdate ?></td>
                    </tr>

                    <tr>
                        <th>Religion</th>
                        <td><?= $model->religion->title ?></td>
                    </tr>

                    <tr>
                        <th>Passport Number</th>
                        <td><?= $model->passportnumber ?></td>
                    </tr>

                    <tr>
                        <th>Passport Image</th>
                        <td>   <?php $file_headers = @get_headers('http://meliaagency.com/web/' . $file); ?>
                            <?php if (!empty($file) && $file_headers[0] == 'HTTP/1.1 200 OK'): ?>
                                <div class="image-user">
                                    <?php $pass = 'http://meliaagency.com/web/' . $file; ?>
                                    <a href="<?= $pass; ?>"><img src="<?= $pass ?>"></a>
                                </div>
                            <?php else: ?>
                                <div class="image-user">
                                    <img src="http://meliaagency.com/web/images/no-image.jpeg">
                                </div>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php if (!empty($videolink)): ?>
                        <tr>
                            <th>Identification Video</th>
                            <td>
                                <video controls>
                                    <source src='http://meliaagency.com/web/<?= $videolink; ?>'>
                                </video>
                            </td>
                        </tr>
                    <?php endif; ?>

                    <tr>
                        <th>Material Status</th>
                        <td><?= $model->materialstatus->title ?></td>
                    </tr>

                    <tr>
                        <th>Languages</th>
                        <td><?= $languages ?></td>
                    </tr>

                    <tr>
                        <th>Height</th>
                        <td><?= $model->height ?></td>
                    </tr>

                    <tr>
                        <th>Weight</th>
                        <td><?= $model->weight ?></td>
                    </tr>

                    <tr>
                        <th>Eyes color</th>
                        <td><?= $model->eyescolor ?></td>

                    </tr>

                    <tr>
                        <th>Hair color</th>
                        <td><?= $model->haircolor ?></td>
                    </tr>


                    </tbody>
                </table>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-map"></i>

                <h3 class="box-title">Contacts</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">


                <table class="table table-striped table-bordered detail-view">
                    <tbody>

                    <tr>
                        <th>Country</th>
                        <td>
                            <?= $model->country ?>
                        </td>
                    </tr>


                    <tr>
                        <th>City</th>
                        <td>
                            <?= $model->city ?>
                        </td>
                    </tr>

                    <tr>
                        <th>Fb Link</th>
                        <td>
                            <a><?= $model->fblink ?></a>
                        </td>
                    </tr>

                    <tr>
                        <th>Vk Link</th>
                        <td>
                            <a><?= $model->vklink ?></a>
                        </td>
                    </tr>

                    <tr>
                        <th>Phone</th>
                        <td>
                            <?= $model->phone ?>
                        </td>
                    </tr>

                    <tr>
                        <th>Living Address</th>
                        <td>
                            <?= $model->livingaddress ?>
                        </td>
                    </tr>



                    <tr>
                        <th>Post Address</th>
                        <td>
                            <?= $model->postaddress ?>
                        </td>
                    </tr>

                    </tbody>
                </table>


            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-info"></i>

                <h3 class="box-title">Info</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <table class="table table-striped table-bordered detail-view">
                    <tbody>

                    <tr>
                        <th>Character</th>
                        <td><?= $model->character->title ?></td>
                    </tr>

                    <tr>
                        <th>Interests</th>
                        <td><?= $model->interests ?></td>
                    </tr>

                    <tr>
                        <th>Hobbies</th>
                        <td><?= $model->hobbies ?></td>
                    </tr>

                    <tr>
                        <th>Mans Descr</th>
                        <td><?= $model->mansdescr ?></td>
                    </tr>
                    <tr>
                        <th>First Letter</th>
                        <td><?= $model->firstletter ?></td>
                    </tr>

                    </tbody>
                </table>

            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-star-o"></i>

                <h3 class="box-title">Other</h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <table class="table table-striped table-bordered detail-view">
                    <tbody>

                    <tr>
                        <th>Main Image</th>
                        <td> <?php $file_headers = @get_headers('http://meliaagency.com/web/' . $avatar); ?>
                            <?php if (!empty($avatar) && $file_headers[0] == 'HTTP/1.1 200 OK'): ?>
                                <p><label class="control-label">Main Image</label></p>
                                <img data-avatarid="<?= $avatarKey; ?>" class="girl-avatar"
                                     src="<?= 'http://meliaagency.com/web/' . $avatar ?>" alt="">
                            <?php else: ?>
                                <img src="http://meliaagency.com/web/images/no-image.jpeg">
                            <?php endif; ?>
                        </td>
                    </tr>

                    <?php if (!empty($photos)): ?>
                        <tr>
                            <th>All Image</th>
                            <td>

                                <?php foreach ($photos as $photo): ?>
                                    <span class="photo">
                                        <a class="profile-gallery-item"
                                           href="<?= 'http://meliaagency.com/web/' . $photo->filepath; ?>"
                                           data-fancybox-group="gallery" title="">
                                            <img src="<?= 'http://meliaagency.com/web/' . $photo->filepathmini; ?>"
                                                 alt="">
                                        </a>
                                    </span>
                                <?php endforeach; ?>

                            </td>
                        </tr>
                    <?php endif; ?>

                    <tr>
                        <th>User ID</th>
                        <td><?= $model->userid ?></td>
                    </tr>

                    <tr>
                        <th>TopDates ID</th>
                        <td><?= $model->topdatesid ?></td>
                    </tr>

                    <tr>
                        <th>Partner ID</th>
                        <td><?= $model->partnerid ?></td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td><?= $model->status->title ?></td>
                    </tr>


                    </tbody>
                </table>
            </div>
        </div>
        <input type="hidden" class="girlIdHidden" value="<?= $model->id; ?>">
        <?= Html::a('Cancel', ['index', 'id' => $model->id], ['class' => 'btn btn-warning ']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php if (empty($model->topdatesid)): ?>
            <?= Html::button('Push to Top-dates', ['class' => 'btn btn-success', 'id' => 'pushToTopDates']) ?>
        <?php endif; ?>
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
        <span class="success-msg successMsg">Девушка успешно отправлена на top-dates</span>

    </section>

</div>