<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Statuses;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartnersSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box box-info">
            <div class="box-header">
                <i class="fa fa-male"></i>

                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                <div class="pull-right box-tools">
                    <!--                    --><? //= Html::a('Create Partners', ['create'], ['class' => 'btn btn-success btn-lg']) ?>
                    <p>
                        <span>
                            <span class="label  label-primary">All <?=$allPartner?></span>
                            <span class="label  bg-yellow">New <?=$newPartner?></span>
                            <span class="label  bg-green">Approved <?=$approvedPartner?></span>
                            <span class="label  bg-red">Rejected <?=$rejectedPartner?></span>
                        </span>
                    </p>
                </div>
            </div>
            <div class="form-active">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [


                        ['attribute' => 'id',
                            'label' => 'ID',

                            'filter' => false],
                        'name',
                        'lastname',
                        ['attribute' => 'statusid',
                            'label' => 'Status',
                            'value' => 'status.title',
                            'filter' => ArrayHelper::map(Statuses::find()->select(['id', 'title'])->all(), 'id', 'title'),


                        ],

                        ['attribute' => 'id',
                            'label' => 'Girl Status',

                            'value' => function ($data) {
                                return $data->getGirlStatus($data->id);
                            },

                            'filter' => false],
                        [
                            'attribute' =>'userid',
                            'label'=> 'IP',
                            'value'=>function($data){
                                return \app\models\Logs::getIP($data->userid);
                            }
                        ],
                        [
                            'attribute' =>'userid',
                            'label'=> 'Country',
                            'value'=>function($data){
                                return \app\models\Logs::getIpCountry($data->userid);
                            }
                        ],
                        ['class' => 'yii\grid\ActionColumn',
                            'template' => '{update}{delete}',
                            'options' => ['width' => '50'],
                        ],


                    ],
                ]); ?>
            </div>
        </div>
    </section>
</div>