<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Partners */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-info"></i>

                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                <div class="box-tools pull-right">
                    <?= Html::a('Cancel', ['index', 'id' => $model->id], ['class' => 'btn btn-warning ']) ?>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary ']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger ',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="form-active">

                <table class="table table-striped table-bordered detail-view">
                    <tbody>

                    <tr>
                        <th>ID</th>
                        <td><?= $model->id ?></td>
                    </tr>

                    <tr>
                        <th>User Name (login)</th>
                        <td><?= $username ?></td>
                    </tr>

                    <tr>
                        <th>Name</th>
                        <td><?= $model->name ?></td>
                    </tr>

                    <tr>
                        <th>Lastname</th>
                        <td><?= $model->lastname ?></td>
                    </tr>

                    <tr>
                        <th>Country</th>
                        <td><?= $model->country ?></td>
                    </tr>

                    <tr>
                        <th>Сity</th>
                        <td><?= $model->city ?></td>
                    </tr>

                    <tr>
                        <th>Email</th>
                        <td><?= $model->email ?></td>
                    </tr>

                    <tr>
                        <th>Phone</th>
                        <td><?= $model->phone ?></td>
                    </tr>

                    <tr>
                        <th>Avatar</th>
                        <td>
                            <?php $file_headers = @get_headers('http://meliapartner.com/web/' . $avatar); ?>
<!--                            <p><label class="control-label">Main Image</label></p>-->
                            <?php if (!empty($avatar) && $file_headers[0] == 'HTTP/1.1 200 OK'): ?>
                                <img class="girl-avatar"
                                     src="<?= 'http://meliapartner.com/web/' . $avatar ?>" alt="">
                            <?php else: ?>
                                <img src="http://meliaagency.com/web/images/no-image.jpeg">
                            <?php endif; ?>

                        </td>
                    </tr>

<!--                    <tr>-->
<!--                        <th>Passport Number</th>-->
<!--                        <td>--><?//= $model->passportnumber ?><!--</td>-->
<!--                    </tr>-->
                    <?php if(!empty($money)): ?>
                    <tr>
                        <th>Money Type</th>
                        <td><?= $money ?></td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <th>Payment method</th>
                        <td><?= $model->pay_metod ?></td>
                    </tr>
                    <tr>
                        <th>Payment information</th>
                        <td><?= $model->pay_data ?></td>
                    </tr>
<!--                    <tr>-->
<!--                        <th>Withdrawal</th>-->
<!--                        <td>--><?//= $model->withdrawal ?><!--</td>-->
<!--                    </tr>-->

                    <tr>
                        <th>Status</th>
                        <?php if (!empty($status)): ?>
                            <td><?= $status['title'] ?></td>
                        <?php endif; ?>
                    </tr>

                    </tbody>
                </table>

            </div>
        </div>
    </section>

</div>