<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Partners */

$this->title = 'Update Partners: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="content-wrapper">

    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-user"></i>

                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'userid')->hiddenInput()->label(false); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'phone')->textInput() ?>


                <?php $file_headers = @get_headers('http://meliapartner.com/web/' . $avatar); ?>
                <p><label class="control-label">Main Image</label></p>
                <?php if (!empty($avatar) && $file_headers[0] == 'HTTP/1.1 200 OK'): ?>
                    <img class="girl-avatar"
                         src="<?= 'http://meliapartner.com/web/' . $avatar ?>" alt="">
                <?php else: ?>
                    <img src="http://meliaagency.com/web/images/no-image.jpeg">
                <?php endif; ?>



                <?= $form->field($model, 'passportnumber')->textInput()->label('Passport Number') ?>

                <?= $form->field($model, 'pay_metod')->dropDownList($payMoney)->label('Payment method') ?>

                <?= $form->field($model, 'pay_data')->textInput()->label('Payment information') ?>

                <!--                --><? //= $form->field($model, 'withdrawal')->textInput(['maxlength' => true]) ?>
                <?php if (!empty($users)): ?>
                    <?= $form->field($model, 'statusid')->dropDownList($users)->label('Status'); ?>
                <?php endif; ?>
                <?= Html::a('Cancel', ['index', 'id' => $model->id], ['class' => 'btn btn-warning ']) ?>

                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success ' : 'btn btn-primary ']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>

</div>
