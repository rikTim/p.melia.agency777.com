<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Partners */

$this->title = 'Create Partners';
$this->params['breadcrumbs'][] = ['label' => 'Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-wrapper">

    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-user"></i>

                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="form-active">
                <div class="column">

                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'phone')->textInput() ?>

                    <?= $form->field($model, 'avatar')->textInput() ?>

                    <?= $form->field($model, 'passportnumber')->textInput()->label('Passport Number') ?>

                    <?= $form->field($model, 'withdrawal')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'getmoneytype')->textInput()->label('Get Money Type') ?>

                    <?= $form->field($model, 'statusid')->dropDownList($users); ?>

                    <?= Html::a('Cancel', ['index', 'id' => $model->id], ['class' => 'btn btn-warning ']) ?>

                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success ' : 'btn btn-primary ']) ?>


                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>

</div>