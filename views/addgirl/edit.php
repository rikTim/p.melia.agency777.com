<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\components\MenuWidget;

$this->title = 'Girls';
?>
<?php Yii::$app->language = $_COOKIE['lang'];?>
<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="<?= Url::toRoute(['index']); ?>"><?= Yii::$app->params['siteName']; ?></a></h1>
                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>
    <?= MenuWidget::widget(); ?>
    <section class="well center well__06 bg01 shadow profile">
        <div class="container">
            <div class="row">
                <div class="grid_12">
                    <div class="img coverImg">
                        <center>
                            <?php $filePath = 'http://meliaagency.com/web/' . $model->avatarPath; ?>
                            <?php if(!empty($model->avatarPath) || file_exists($filePath)): ?>
                                <img  src="<?= $filePath; ?>">
                            <?php else: ?>
                                <img  src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/no-image-girl.png" alt="">
                            <?php endif; ?>

                            <?php if(!empty($model->id)): ?>
                                <h3><a href="<?= Url::toRoute(['profile/index']); ?>"><?= $model->name . ' ' . $model->lastname ?></a></h3>
                                <small><?= Yii::t('app','Status')?>: <?= $model->status->title; ?></small>
                                <p><i class="fa fa-map-marker"></i><?= $model->city . ', ' . $model->country ?></p>
                                <p>
                                    <span><i class="fa fa-envelope"></i> <?php if(!empty($model->email)) echo $model->email; ?></span>
                                    <span><i class="fa fa-phone"></i><?php if(!empty($model->phone)) echo $model->phone; ?> </span>
                                </p>
                            <?php endif; ?>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="well well__03 profile profile-form">
        <div class="container">
            <div>
                <span data-step="step_1" class="btn-noshadow tab-btn step-btn-index"><?= Yii::t('app','Personal info')?></span>
                <span data-step="step_2" class="btn-noshadow tab-btn step-btn-index unactive"><?= Yii::t('app','Contacts')?></span>
                <span data-step="step_3" class="btn-noshadow tab-btn step-btn-index unactive"><?= Yii::t('app','Info')?></span>
                <span data-step="step_4" class="btn-noshadow tab-btn step-btn-index unactive"><?= Yii::t('app','Photos')?></span>
<!--                <span data-step="step_5" class="btn-noshadow tab-btn step-btn-index unactive">--><?//= Yii::t('app','Video')?><!--</span>-->
            </div>

            <?php
            $form = ActiveForm::begin([
                'id' => 'girlProfileForm',
                'options' => ['class' => 'notifications-form', 'enctype' => 'multipart/form-data']
            ]);
            ?>
            <div class="step step_1">
                <div class="row">
                    <div class="grid_6">
                        <div class="controlHolder">
                            <div class="tmInput">
                                <?= $form->field($model, 'nameid')->dropDownList($names)->label(Yii::t('app','Name')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="tmInput">
                            <?php echo $form->field($model, 'education')->dropDownList($education)->label(); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="tmInput">
                            <?= $form->field($model, 'lastname')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Lastname')])->label(); ?>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="tmInput">
                            <?php echo $form->field($model, 'occupationid')->dropDownList($occupations)->label(Yii::t('app','Occupation')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                            <div class="tmInput">
                                <?= $form->field($model, 'birthdate')->textInput(['placeholder' => Yii::t('app','Birth date'), 'class' => 'form-item birthdate'])->label(Yii::t('app','Birth date')); ?>
                            </div>
                    </div>
                    <div class="grid_6">
                        <div class="tmInput">
                            <?= $form->field($model, 'religionid')->dropDownList($religions)->label(Yii::t('app','Religion')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="tmInput">
                            <?= $form->field($model, 'passportnumber')->textInput(['class' => 'form-item', 'placeholder' => 'ID/passport number'])->label(Yii::t('app','Passport number')); ?>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="tmInput">
                            <?= $form->field($model, 'materialstatusid')->dropDownList($statuses)->label(Yii::t('app','Marital status')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="tmInput">
                            <?php echo $form->field($model, 'languageid')->dropDownList($languages)->label(Yii::t('app','Language')); ?>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="tmInput">
                            <?php echo $form->field($model, 'languageid2')->dropDownList($languages)->label('Second language'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="tmInput">
                            <?= $form->field($model, 'height')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Height (cm)')])->label(); ?>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="tmInput">
                            <?= $form->field($model, 'weight')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Weight (kg)')])->label(); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="tmInput">
                            <?= $form->field($model, 'eyescolor')->dropDownList($eyescolor)->label(Yii::t('app','Eyes color')); ?>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="tmInput">
                            <?php echo $form->field($model, 'haircolor')->dropDownList($haircolor)->label(Yii::t('app','Hair color')); ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="step step_2">
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <label class="control-label" for="girls-country"><?=Yii::t('app','Country')?></label>
                                <select name="Girls[country]" class="country-list-chosen">
                                    <option value="<?= $model->country?>"><?= $model->country?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput required">
                                <?= $form->field($model, 'cityid')->dropDownList($cities)->label(Yii::t('app','City')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?= $form->field($model, 'fblink')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Fb link')])->label(Yii::t('app','Facebook link')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?= $form->field($model, 'vklink')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Vk link') ])->label(Yii::t('app','Vkontakte link')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?= $form->field($model, 'phone')->textInput(['id' => 'phoneMask', 'class' => 'form-item', 'placeholder' => Yii::t('app','Phone')])->label(Yii::t('app','Phone')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?= $form->field($model, 'email')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Email')])->label(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?= $form->field($model, 'livingaddress')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Living address')])->label(Yii::t('app','Living address')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <?= $form->field($model, 'postaddress')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Post address')])->label(Yii::t('app','Post address')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="step step_3">
                <div class="row">
                    <div class="grid_12">
                        <div class="iconed-box">
                            <?php echo $form->field($model, 'characterid')->dropDownList($characters)->label(Yii::t('app','Character')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_12">
                        <div class="iconed-box">
                            <?= $form->field($model,'interests')->textArea(['class' => 'form-item', 'placeholder'=>Yii::t('app','Interests')])->label(Yii::t('app','Interests'));?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_12">
                        <div class="iconed-box">
                            <?= $form->field($model,'hobbies')->textArea(['class' => 'form-item', 'placeholder'=> Yii::t('app','Hobbies')])->label(Yii::t('app','Hobbies'));?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_12">
                        <div class="iconed-box">
                            <?= $form->field($model,'mansdescr')->textArea(['class' => 'form-item', 'placeholder'=>Yii::t('app',"Man's description")])->label(Yii::t('app',"Man's description"));?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_12">
                        <div class="iconed-box">
                            <?= $form->field($model,'firstletter')->textArea(['class' => 'form-item', 'placeholder'=> Yii::t('app','First letter')])->label(Yii::t('app','First letter'));?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="step step_4">
                <?php if(!empty($photos)): ?>
                    <div class="row">
                        <div class="grid_12">
                            <div class="girl-album edit">
<!--                                <h5>Uploaded photos</h5>-->
                                <?php foreach($photos as $photo): ?>
                                    <span class="photo">
                                        <a class="profile-gallery-item" href="<?= 'http://melia.agency777.com/web/' . $photo->filepath; ?>" data-fancybox-group="gallery" title="">
                                            <img src="<?= 'http://meliaagency.com/web/' . $photo->filepath; ?>" alt="">
                                        </a>
                                        <span class="del-photo" data-photo="<?= $photo->id; ?>">
                                            <i class="fa fa-trash-o"></i>
                                        </span>
                                    </span>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="grid_12">
                        <div style="background: #f7f8fa;padding: 50px;">
                            <input type="file" multiple="multiple" name="files[]" id="input2">
                        </div>
                    </div>
                </div>
            </div>



            <div class="clr"></div>
            <div class="control-btns">
                <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id])->label(false);?>

                <input type="hidden" class="girlsCountry" value="<?= $model->country; ?>" name="Girls[country]">
                <input type="hidden" class="girlsCityId" value="<?= $model->cityid; ?>" name="Girls[cityid]">
                <input type="hidden" class="girlsCity" value="<?= $model->city; ?>" name="Girls[city]">
                <input type="hidden" class="girlsName" value="<?= $model->name; ?>" name="Girls[name]">

                <span class="btn-noshadow prev-step"><?=Yii::t('app','Prev step')?></span>
                <span class="btn-noshadow next-step"><?=Yii::t('app','Next step')?></span>
                <?= Html::submitButton(Yii::t('app','Save profile'), ['class' => 'btn-noshadow', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>



        </div>
    </section>
</main>
