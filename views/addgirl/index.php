<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\components\MenuWidget;

$this->title = 'Girls';
?>
<?php Yii::$app->language = $_COOKIE['lang'];?>
<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="<?= Url::toRoute(['index']); ?>"><?= Yii::$app->params['siteName']; ?></a></h1>
                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>
    <?= MenuWidget::widget(); ?>

    <section class="well center well__06 bg01 shadow filter-block">
        <h2><?=Yii::t('app','Girls')?></h2>
    </section>

    <section class="well center m-height partner-girls">
        <div class="container">
            <?php if(!empty($partner)): ?>
                <?php if($partner->statusid == 6): ?>
                    <a href="<?= Url::toRoute(['addgirl/create']); ?>">
                        <button type="button" class="btn-noshadow"><?= Yii::t('app','Add girl')?></button>
                    </a>
                    <?php if(!empty($girls)): ?>
                        <div class="row">
                            <div class="girls">
                                <?php foreach($girls as $girl): ?>
                                    <a href="<?= Url::toRoute(['view', 'id' => $girl->id]); ?>">
                                        <div class="girl">

                                            <?php $filePath = 'http://meliaagency.com/web/' . $girl->avatarPath; ?>
                                            <?php $file_headers = @get_headers($filePath);?>
                                            <?php if(!empty($girl->avatarPath) && $file_headers[0] == 'HTTP/1.1 200 OK'):?>
                                                <img  src="<?= $filePath; ?>">
                                            <?php else: ?>
                                                <img  src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/no-image-girl.png" alt="">
                                            <?php endif; ?>
                                            <br>
                                            <span class="name"><?= $girl->name .' '.$girl->lastname; ?></span><br>
                                            <span class="status"><?= Yii::t('app','Status')?>: <?= $girl->status->title; ?></span><br>
                                            <span class="contacts">
                                                <span class="address">
                                                    <i class="fa fa-map-marker"></i><?= $girl->city .', '. $girl->country; ?>
                                                </span><br>
                                                <span class="email"><i class="fa fa-envelope"></i><?= $girl->email; ?></span><br>
                                                <span class="phone"><i class="fa fa-phone"></i><?= $girl->phone; ?></span>
                                            </span>
                                        </div>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <h5><?= Yii::t('app','You have not registered girls yet')?></h5>
                    <?php endif; ?>
                <?php else: ?>
                    <h5> <?= Yii::t('app',"You can't add girls yet. Your profile doesn't approved") ?></h5>
                <?php endif; ?>
            <?php else: ?>
                <h5><?=Yii::t('app',"Fill your profile to add girls")?></h5>
            <?php endif; ?>
        </div>
    </section>
</main>
