<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\components\MenuWidget;

$this->title = 'Personal Profile';
?>
<?php Yii::$app->language = $_COOKIE['lang'];?>
<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="<?= Url::toRoute(['index']); ?>"><?= Yii::$app->params['siteName']; ?></a></h1>
                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>
    <?= MenuWidget::widget(); ?>
    <section class="well center well__06 bg01 shadow profile">
        <div class="container">
            <div class="row">
                <div class="grid_12">
                    <div class="img coverImg">
                        <center>
                            <?php $filePath = 'http://meliaagency.com/web/' . $model->avatarPath; ?>

                            <?php $file_headers = @get_headers($filePath);?>
                            <?php if(!empty($model->avatarPath) && $file_headers[0] == 'HTTP/1.1 200 OK'):?>
                                <img  src="<?= $filePath; ?>">
                            <?php else: ?>
                                <img  src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/no-image-girl.png" alt="">
                            <?php endif; ?>

                            <?php if(!empty($model->id)): ?>
                                <h3><a href="<?= Url::toRoute(['profile/index']); ?>"><?= $model->name . ' ' . $model->lastname ?></a></h3>
                                <small><?= Yii::t('app','Status')?>: <?= $model->status->title; ?></small>
                                <p><i class="fa fa-map-marker"></i><?= $model->city . ', ' . $model->country ?></p>
                                <p>
                                    <span><i class="fa fa-envelope"></i> <?php if(!empty($model->email)) echo $model->email; ?></span>
                                    <span><i class="fa fa-phone"></i><?php if(!empty($model->phone)) echo $model->phone; ?> </span>
                                </p>
                            <?php endif; ?>
                            <?php if($model->statusid->id == 5 || $model->statusid->id == 7): ?>
                                <p>
                                    <span>
                                        <a href="<?= Url::toRoute(['edit', 'id' => $model->id]); ?>">
                                            <i class="fa fa-cogs"></i><?= Yii::t('app','Edit profile')?>
                                        </a>
                                    </span>
                                </p>
                            <?php endif; ?>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="well well__03 profile profile-form">
        <div class="container">
            <div>
                <span data-step="step_1" class="btn-noshadow tab-btn step-btn-index"><?= Yii::t('app','Personal info')?></span>
                <span data-step="step_2" class="btn-noshadow tab-btn step-btn-index unactive"><?= Yii::t('app','Contacts')?></span>
                <span data-step="step_3" class="btn-noshadow tab-btn step-btn-index unactive"><?= Yii::t('app','Info')?></span>
                <span data-step="step_4" class="btn-noshadow tab-btn step-btn-index unactive"><?= Yii::t('app','Photos')?></span>
            </div>

            <?php $form = ActiveForm::begin([
                'id' => 'notificationForm',
                'options' => ['class' => 'notifications-form', 'enctype' => 'multipart/form-data']]);
            ?>
            <div class="step step_1">
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="controlHolder">
                                <div class="tmInput">
                                    <b><?= Yii::t('app','Name')?>: </b><?= $model->name; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Education')?>: </b><?= $model->education; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Lastname')?>: </b><?= $model->lastname; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Occupation')?>: </b><?= $model->occupationid; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Birth date')?>: </b><?= $model->birthdate; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Religion')?>: </b><?= $model->religion->title; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Passport number')?>: </b><?= $model->passportnumber; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Language')?>: </b><?= $model->languageid; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Marital status')?>: </b><?= $model->materialstatus->title; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b>Second language: </b><?= $model->languageid2; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Height')?>: </b><?= $model->height; ?> cm
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Weight')?>: </b><?= $model->weight; ?> kg
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Eyes color')?>: </b><?= $model->eyescolor; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Hair color')?>: </b><?= $model->haircolor; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="step step_2">
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Country')?>: </b><?= $model->country; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','City')?>: </b><?= $model->city; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Facebook link')?>: </b><?= $model->fblink; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Vkontakte link')?>: </b><?= $model->vklink; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Phone')?>: </b><?= $model->phone; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Email')?>: </b><?= $model->email; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Living address')?>: </b><?= $model->livingaddress; ?>
                            </div>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="tmInput">
                                <b><?= Yii::t('app','Post address')?>: </b><?= $model->postaddress; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="step step_3">
                <div class="row">
                    <div class="grid_12">
                        <div class="iconed-box">
                            <b><?= Yii::t('app','Character')?>: </b><br /><?= $model->characterid; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_12">
                        <div class="iconed-box">
                            <b><?= Yii::t('app','Interests')?>: </b><br /><?= $model->interests; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_12">
                        <div class="iconed-box">
                            <b><?= Yii::t('app','Hobbies')?>: </b><br /><?= $model->hobbies; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_12">
                        <div class="iconed-box">
                            <b><?= Yii::t('app',"Man's description")?>: </b><br /><?= $model->mansdescr; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_12">
                        <div class="iconed-box">
                            <b><?= Yii::t('app','First letter')?>: </b><br /><?= $model->firstletter; ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="step step_4">
                <div class="row">
                    <div class="girl-album">
                        <?php if(!empty($photos)): ?>
                            <?php foreach($photos as $photo): ?>
                                <span class="photo">
                                        <a class="profile-gallery-item" href="<?= 'http://meliaagency.com/web/' . $photo->filepath; ?>" data-fancybox-group="gallery" title="">
                                            <img src="<?= 'http://meliaagency.com/web/' . $photo->filepath; ?>" alt="">
                                        </a>
                                    </span>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <h5 style="text-align: center"><?=Yii::t('app',"No photos");?></h5>
                        <?php endif; ?>
                    </div>
                </div>
            </div>


            <?php if($model->status->id == 5 || $model->status->id == 7): ?>
                <div class="control-btns">
                    <input type="hidden" class="girlId" value="<?= $model->id; ?>">
                    <span class="btn-noshadow fast-edit"><?=Yii::t('app','Edit profile');?></span>
                </div>
            <?php endif; ?>
        </div>
    </section>

</main>
