<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\components\MenuWidget;

$this->title = 'Girls';
?>
<?php Yii::$app->language = $_COOKIE['lang'];?>
<div class="page">
    <header>
        <section class="">
            <div class="top-panel">
                <div class="container">
                    <div class="brand">
                        <h1 class="brand_name"><a href="<?= Url::toRoute(['index']); ?>"><?= Yii::$app->params['siteName']; ?></a></h1>
                        <p class="brand_slogan">dating site</p>
                    </div>
                </div>
            </div>
        </section>
    </header>

    <!--==============================CONTENT==============================-->

    <main>
        <?php
        $form = ActiveForm::begin([
            'id' => 'girlProfileForm',
            'options' => ['class' => 'notifications-form', 'enctype' => 'multipart/form-data']
        ]);
        ?>
        <?= MenuWidget::widget(); ?>
        <section class="well center well__06 bg01 shadow profile">
            <div class="container">
                <div class="row">
                    <div class="grid_12">
                        <h2><?= Yii::t('app','Add girl')?> </h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="well well__03 profile profile-form">
            <div class="container">
                <div>
                    <span data-step="step_1" class="btn-noshadow tab-btn"><?= Yii::t('app','Personal info')?></span>
                    <span data-step="step_2" class="btn-noshadow tab-btn unactive"><?= Yii::t('app','Contacts')?></span>
                    <span data-step="step_3" class="btn-noshadow tab-btn unactive"><?= Yii::t('app','Info')?></span>
                    <span data-step="step_4" class="btn-noshadow tab-btn unactive"><?= Yii::t('app','Photos')?></span>
                    <span data-step="step_5" class="btn-noshadow tab-btn unactive"><?= Yii::t('app','Video')?></span>
                </div>

                <!-- step-btn-index -->


                <div class="step step_1">
                    <div class="row">
                        <div class="grid_6">
                            <div class="controlHolder">
                                <div class="tmInput required">
                                    <?= $form->field($model, 'nameid')->dropDownList($names)->label(Yii::t('app','Name')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'education')->dropDownList($education)->label(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'lastname')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Lastname')])->label(); ?>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'occupationid')->dropDownList($occupations)->label(Yii::t('app','Occupation')); ?>
                            </div>
                        </div>
                    </div>
<!--                    <div class="row">-->
<!--                        <div class="grid_6">-->
<!--                            <div class="controlHolder">-->
<!--                                <div class="tmInput required">-->
<!--                                    --><?//= $form->field($userModel, 'username')->textInput(['class' => 'form-item', 'placeholder' => 'User name (for log in)'])->label(Yii::t('app','Username')); ?>
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="grid_6">-->
<!--                            <div class="tmInput required">-->
<!--                                --><?//= $form->field($userModel, 'password')->textInput(['class' => 'form-item', 'placeholder' => 'Password (for log in)'])->label(Yii::t('app','Password')); ?>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'birthdate')->textInput(['placeholder' => Yii::t('app','Birth date'), 'class' => 'form-item birthdate'])->label(Yii::t('app','Birth date')); ?>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'religionid')->dropDownList($religions)->label(Yii::t('app','Religion')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'passportnumber')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','ID / Passport number')])->label(Yii::t('app','Passport number')); ?>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?php echo $form->field($model, 'materialstatusid')->dropDownList($statuses)->label('Marital status'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput imgBlock required">
                                <span class="btn-noshadow uploadPersonalImage"><?=Yii::t('app','Passport image')?></span>
                                <?= $form->field($fileModel, 'imageFile')->fileInput(['data-type' => 'Img', 'style' => 'display: none', 'class' => 'uploadPassport form-item'])->label(false); ?>
                                <div class="fileName fileNameImg">
                                    <span class="name"></span>
                                    <i data-type="Img" class="fa fa-trash-o del-btn deleteFile"></i>
                                </div>
                            </div>
                            <span class="required">*</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?php echo $form->field($model, 'languageid')->dropDownList($languages)->label(Yii::t('app','Language')); ?>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?php echo $form->field($model, 'languageid2')->dropDownList($languages)->label('Second language'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'height')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Height (cm)')])->label(); ?>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'weight')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Weight (kg)')])->label(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?php echo $form->field($model, 'eyescolor')->dropDownList($eyescolor)->label(Yii::t('app','Eyes color')); ?>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?php echo $form->field($model, 'haircolor')->dropDownList($haircolor)->label(Yii::t('app','Hair color')); ?>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="step step_2">
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput required">
                                <label class="control-label" for="girls-country"><?=Yii::t('app','Country')?></label>
                                <select name="Girls[country]" class="country-list-chosen">
                                </select>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'cityid')->dropDownList($cities)->label(Yii::t('app','City')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput">
                                <?= $form->field($model, 'fblink')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Fb link')])->label(Yii::t('app','Facebook link')); ?>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput">
                                <?= $form->field($model, 'vklink')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Vk link')])->label(Yii::t('app','Vkontakte link')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'phone')->textInput(['id' => 'phoneMask', 'class' => 'form-item', 'placeholder' => Yii::t('app','Phone') ])->label(Yii::t('app','Phone')); ?>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'email')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Email')])->label(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'livingaddress')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Living address')])->label(Yii::t('app','Living address')); ?>
                            </div>
                        </div>
                        <div class="grid_6">
                            <div class="tmInput required">
                                <?= $form->field($model, 'postaddress')->textInput(['class' => 'form-item', 'placeholder' => Yii::t('app','Post address')])->label(Yii::t('app','Post address')); ?>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="step step_3">
                    <div class="row">
                        <div class="grid_12">
                            <div class="tmInput required">
                                <?php echo $form->field($model, 'characterid')->dropDownList($characters)->label(Yii::t('app','Character')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_12">
                            <div class="tmInput required">
                                <?= $form->field($model,'interests')->textArea(['class' => 'form-item', 'placeholder'=> Yii::t('app','Interests')])->label(Yii::t('app','Interests'));?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_12">
                            <div class="tmInput required">
                                <?= $form->field($model,'hobbies')->textArea(['class' => 'form-item', 'placeholder'=>Yii::t('app','Hobbies')])->label(Yii::t('app','Hobbies'));?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_12">
                            <div class="tmInput required">
                                <?= $form->field($model,'mansdescr')->textArea(['class' => 'form-item', 'placeholder'=>Yii::t('app',"Man's description")])->label(Yii::t('app',"Man's description"));?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="grid_12">
                            <div class="tmInput required">
                                <?= $form->field($model,'firstletter')->textArea(['class' => 'form-item', 'placeholder'=> Yii::t('app','First letter')])->label(Yii::t('app','First letter'));?>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="step step_4">
                    <div class="row">
                        <div class="grid_12">
                            <div style="background: #f7f8fa;padding: 50px;">
                                <input type="file" multiple="multiple" name="files[]" id="input2">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="step step_5">
                    <div class="row">
                        <div class="grid_12">
                            <div style="background: #f7f8fa;padding: 50px;">
                                <input type="file" name="files" id="input3">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clr"></div>
                <div class="control-btns">
                    <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id])->label(false);?>

                    <input type="hidden" class="girlsCountry" name="Girls[country]">
                    <input type="hidden" class="girlsCityId" name="Girls[cityid]">
                    <input type="hidden" class="girlsCity" name="Girls[city]">
                    <input type="hidden" class="girlsName" name="Girls[name]">

                    <input type="hidden" value="3" name="Users[status]">
                    <span class="btn-noshadow prev-step"><?=Yii::t('app','Prev step')?></span>
                    <span class="btn-noshadow next-step"><?=Yii::t('app','Next step')?></span>
                    <?= Html::button(Yii::t('app','Save profile'), ['class' => 'btn-noshadow save-profile saveProfile', 'name' => 'login-button']) ?>
                    <span class="rulesBtn"></span>



                    <!--  Suuccess msg popup  -->
                    <div class="arcticmodal-overlay successPopup" style="display: none; opacity: 0.6; background-color: rgb(0, 0, 0);"></div>
                    <div class="arcticmodal-container successPopup" style="display: none;">
                        <table class="arcticmodal-container_i">
                            <tbody>
                            <tr>
                                <td class="arcticmodal-container_i2">
                                    <div class="box-modal" id="copyrightModal">
                                        <h4 style="margin-bottom: 20px;display: inline-block;"><?=Yii::t('app','Thank you for registering . <br> Profile has been sent for moderation. <br> Check your email please')?></h4>
                                        <?= Html::submitButton('Ok', ['class' => 'btn-noshadow save-profile', 'name' => 'login-button']); ?>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </section>
        <?php ActiveForm::end(); ?>
    </main>


    <!--  Rules popup  -->
    <?php if(!$isLogs): ?>
        <div class="arcticmodal-overlay" style="opacity: 0.6; background-color: rgb(0, 0, 0);"></div>
        <div class="arcticmodal-container">
            <table class="arcticmodal-container_i">
                <tbody>
                <tr>
                    <td class="arcticmodal-container_i2">
                        <div class="box-modal" id="copyrightModal">
                            <center><h4 style="display: inline-block;"><?= Yii::t('app', 'Rules') ?></h4></center>
                            <?php if (!empty($aboutPage)): ?>
                                <p><?= $aboutPage->$lntext; ?></p>
                                <center style="margin-top: 30px;">
                                    <span class="label-wrapper"><label><input class="accept-rules" type="checkbox"/><?= Yii::t('app', 'Accept') ?></label></span>
                                    <span class="btn-rules to-edit btn-nomargintop unactive">Ok</span>
                                </center>
                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    <?php endif; ?>