<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Giftpages */

$this->title = 'Update News: ' . ' ' . $model->title_en;

?>
<div class="content-wrapper">
    <section class="content-header">

    </section>


    <section class="content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </section>

</div>