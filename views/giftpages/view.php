<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Giftpages */
$this->title = $model->title_en;

?>
<div class="content-wrapper">
    <section class="content-header">
    </section>

    <section class="content">
        <div class="box box-primary">
            <div class="box-header ui-sortable-handle">
                <i class="fa fa-newspaper-o"></i>

                <h3 class="box-title">News</h3>

                <div class="box-tools pull-right">
                    <?= Html::a('Cancel', ['index', 'id' => $model->id], ['class' => 'btn btn-warning ']) ?>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary ']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger ',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="form-active">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'title_ru',
                        'title_en',
                        'title_es',
                        'title_cz',
                        'text_ru:ntext',
                        'text_en:ntext',
                        'text_es:ntext',
                        'text_cz:ntext',
                        'datetime:datetime',
                        ['attribute' => 'is_active',
                            'format' => 'boolean',
                            'label' => 'Active',],
                    ],
                ]) ?>

            </div>
        </div>
    </section>


</div>
