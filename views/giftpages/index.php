<?php $this->title = 'News'; ?>
<? use yii\helpers\Html;
use yii\grid\GridView; ?>
<div class="content-wrapper">


    <section class="content">
        <section class="content">
            <div class="box box-primary">
                <div class="box-header ui-sortable-handle">
                    <i class="fa fa-newspaper-o"></i>

                    <h3 class="box-title">News</h3>

                    <div class="box-tools pull-right">
                        <?= Html::a('Create News', ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
                <div class="form-active">


                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //            'filterModel' => $searchModel,

                        'columns' => [

                            [
                                'attribute' => 'id',

                                'headerOptions' => ['width' => '50'],
                            ],

                            [
                                'attribute' => 'title_ru',
                                'label' => 'Title',
                                'headerOptions' => ['width' => '200'],
                            ],


                            [
                                'attribute' => 'text_ru',
                                'options' => ['width' => '1000'],
                                'format' => 'ntext',


                            ],


                            [
                                'attribute' => 'is_active',
                                'format' => 'boolean',
                                'label' => 'Active',
                                'options' => ['width' => '70'],

                            ],

                            [
                                'attribute' => 'datetime',
                                'format' => 'datetime',
                                'options' => ['width' => '150'],

                            ],


                            ['class' => 'yii\grid\ActionColumn',
                                'options' => ['width' => '80'],
                            ],
                        ],
                    ]); ?>


                </div>
            </div>
        </section>

</div>