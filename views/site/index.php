<?php $this->title = 'MeliaAGENSY'; ?>
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php Yii::$app->language = $_COOKIE['lang']; ?>

<span class="contact-btn contactBtn"><?= Yii::t('app','Any questions?'); ?></span>

<div class="page">
    <header>
        <?php $file_headers = @get_headers('http://melia.agency777.com/web/images/'.$image); ?>
        <?php if (!empty($image) && $file_headers[0] == 'HTTP/1.1 200 OK'): ?>
        <section style="background-image:url(<?=$image?>) " class="parallax parallax01">
            <?php else: ?>
            <section style="background-image:url(http://meliaagency.com/web/images/parallax01.jpg) " class="parallax parallax01">
                <?php endif; ?>
                <div class="top-panel">
                    <div class="container">
                        <div class="brand">
                            <h1 class="brand_name"><a href="./"><?= Yii::$app->params['siteName']; ?></a></h1>
                            <p class="brand_slogan">Premium Partner Program</p>
                        </div>
                    </div>
            <span class="langs-list">
                <ul>
                    <li data-lang="En">En</li>
                    <li data-lang="Ru">Ru</li>
                    <li data-lang="Cz">Cz</li>
                </ul>
            </span>
                </div>
                <div class="container">
                    <?php if(!empty($header)): ?>
                        <h2><?= $header->$lntext; ?></h2>
                    <?php else: ?>
                        <h2>
                            <em><?= Yii::t('app','Register for Free to Meet')?></em>
                            <?= Yii::t('app','New People and Start Dating Today!')?>
                        </h2>
                    <?php endif; ?>
                    <div class="row">


                        <?php if(!empty($message)): ?>
                            <div class="preffix_4 grid_4 form logInForm">
                                <div class="booking-form">
                                    <p style="color:#4C69B3;"><?= Yii::t('app', $message) ?></p>
                                </div>
                            </div>
                        <?php endif; ?>




                        <div class="preffix_4 grid_4 form logInForm <?php $res = $model->getErrors(); if(!empty($message) || !empty($res)) echo 'd-none'; ?>">
                            <?php $form = ActiveForm::begin([
                                'id' => 'bookingForm',
                                'options' => ['class' => 'booking-form']]);?>
                            <div class="tmInput">
                                <?= $form->field($userModel, 'username')->textInput(['placeholder' => Yii::t('app','Login')])->label(false, ['style'=>'display:none']);?>
                            </div>
                            <div class="tmInput">
                                <?= $form->field($userModel, 'password')->passwordInput(['placeholder' => Yii::t('app','Password')])->label(false, ['style'=>'display:none']); ?>
                            </div>
                            <div class="tmInput">
                                <?= $form->field($userModel, 'rpassword')->passwordInput(['placeholder' => Yii::t('app','Repeat Password')])->label(false, ['style'=>'display:none']); ?>
                            </div>
                            <div class="tmInput">
                                <?= $form->field($userModel, 'email')->textInput(['placeholder' => Yii::t('app','Email')])->label(false, ['style'=>'display:none']);?>
                            </div>
                            <?= $form->field($userModel, 'status')->hiddenInput(['value'=> '2'])->label(false);?>

                            <?= Html::submitButton(Yii::t('app','Register'), ['class' => 'btn', 'name' => 'register-button']) ?>
                            <br>
                            <span data-log="singInForm" class="logBtn"><?= Yii::t('app','Log in')?></span>
                            <?php if (Yii::$app->getSession()->hasFlash('error')) {
                                echo '<div class="alert alert-danger">'.Yii::$app->getSession()->getFlash('error').'</div>';
                            } ?>

                            <br>
                            <div class="social-btns">
                                <i class="fa fa-vk social-icons vkRegister"></i>
                                <i class="fa fa-facebook social-icons fbRegister"></i>
                                <?= \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>


                        <div class="d-none preffix_4 grid_4 form singInForm <?php $res = $model->getErrors(); if(!empty($res)) echo 'd-block'; ?>">
                            <?php $form = ActiveForm::begin([
                                'id' => 'bookingForm',
                                'options' => ['class' => 'booking-form']]);?>
                            <div class="tmInput">
                                <?= $form->field($model, 'username')->textInput(['placeholder' => Yii::t('app','Login')])->label(false, ['style'=>'display:none']);?>
                            </div>
                            <div class="tmInput">
                                <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app','Password')])->label(false, ['style'=>'display:none']); ?>
                            </div>

                            <div style="float: left;">
                                <?= $form->field($model, 'rememberMe')->checkbox()->label(Yii::t('app','Remember Me')); ?>
                            </div>
                            <?= Html::submitButton(Yii::t('app','Login'), ['class' => 'btn btn-nomargintop', 'name' => 'login-button']) ?>
                            <br>
                            <a href="http://meliapartner.com/web/password" class="logBtn"><?= Yii::t('app','Forgot a password'); ?></a>
                            <br>

                        <span data-log="logInForm" class="logBtn">
                            <?= Yii::t('app','Sing in')?>
                        </span>

                            <br>
                            <div class="social-btns">
                                <i class="fa fa-vk social-icons vkRegister"></i>
                                <i class="fa fa-facebook social-icons fbRegister"></i>
                                <?= \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>

                    </div>
                </div>
            </section>
    </header>


    <!--==============================CONTENT==============================-->
    <main>
        <section class="well center">
            <div class="container">
                <h2><?= Yii::t('app','Premium Partner Program')?></h2>
                <p><?= Yii::t('app','We value and respect all our partners and pay the highest commissions.  We offer two programs for our partners. We pay our partners up to $ 100 for each uploaded profile of a woman who is seriously interested in the services of our dating agency. The second program is $20 monthly for each downloaded profile for all the time while it is active on our website.  In our dating agency men pay for all services, all services are free for women .')?></p>
                <div class="row">
                    <div class="grid_4">
                        <div class="icon icon-md-person325"></div>
                        <h3><?= Yii::t('app','Register Like Partner')?></h3>
                        <p class="small"><?= Yii::t('app','Registration is used for internal use only and is not transmitted to third parties')?></p>
                    </div>
                    <div class="grid_4">
                        <div class="icon icon-md-mountain31"></div>
                        <h3><?= Yii::t('app','Create Woman Profile')?></h3>
                        <p class="small"><?= Yii::t('app','All women must understand that they are registered in the dating agency')?></p>
                    </div>
                    <div class="grid_4">
                        <div style="font: 65px Arial; padding-top: 15px;height: 90px; margin-top:0;" class="icon fa fa-money"></div>
                        <h3><?= Yii::t('app','Get Your Money')?></h3>
                        <p class="small"><?= Yii::t('app','Payments are made at the beginning of each month. Once a month you will get a total amount for all the uploaded profiles.')?></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="well well__02 bg01 shadow">
            <div class="container">
                <div class="row">
                    <div class="grid_5">
                        <div class="img"><img src="images/img01.jpg" alt=""></div>
                    </div>
                    <div class="grid_7 offset01">
                        <h2><?= Yii::t('app','We take care of each of our clients and make them happy')?></h2>
                        <p><?= Yii::t('app','Our dating agency is focused on serious customers who are interested in dating. The purpose of our agency and our partners is the happiness of our customers. We are committed to every customer that could not find a mate for marriage or friendship. Individual approach to every user is our success.')?></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="well well__03">
            <div class="container">
                <h2><?= Yii::t('app','What does the lady whose profile is posted on the website get ?')?></h2>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="icon icon-md-birthday20"></div>
                            <h4><?= Yii::t('app','Flowers & Gifts')?></h4>
                            <p><?= Yii::t('app','All the girls get signs of attention from our men. They buy flowers and gifts for our girls and our delivery service will deliver it to any convenient place.')?></p>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="icon icon-md-add184"></div>
                            <h4><?= Yii::t('app','Free Photosession')?></h4>
                            <p><?= Yii::t('app','All the girls get free professional photoshoot consisting of about 100 original and 10 photoshopped photos.')?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="icon icon-md-favorite22"></div>
                            <h4><?= Yii::t('app','Love and Happiness')?></h4>
                            <p><?= Yii::t('app',"Romance begins with the first day of the profile's activity. Girls receive instant messages from all men and begin correspondence.")?></p>
                        </div>
                    </div>
                    <div class="grid_6">
                        <div class="iconed-box">
                            <div class="icon icon-md-two385"></div>
                            <h4><?= Yii::t('app','Mens Profiles')?></h4>
                            <p><?= Yii::t('app',"Opportunity to make a quick database search of the men's profiles and start the conversation.")?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="parallax parallax02 shadow">
            <div class="container">
                <div class="row">
                    <div class="preffix_1 grid_10">
                        <blockquote><?= Yii::t('app','Our dating agency has helped hundreds of people around the world to find their soulmate and thousands of people have become happier.')?></blockquote>
                        <div class="img img__circle"><img src="images/img02.jpg" alt=""></div>
                        <h5><?= Yii::t('app','Be happy')?></h5>
                        <p class="small"><?= Yii::t('app','With our Dating Agency')?></p>
                    </div>
                </div>
            </div>
        </section>

        <section class="well">
            <div class="container">
                <h2><?= Yii::t('app','Asked Questions')?></h2>
                <div class="row">
                    <div class="grid_6 left">
                        <h4><?= Yii::t('app','Who can become our partner ?')?></h4>
                        <p><?= Yii::t('app','Anyone who can represent and advertise our dating agency in their region can become our partner. Anyone who who will make sure the ladies are aware of the rules of our agency, who can organize photosessions and upload data into our database.')?></p>
                    </div>
                    <div class="grid_6 left">
                        <h4><?= Yii::t('app','How much money will I earn?')?> </h4>
                        <p><?= Yii::t('app',"You will earn up to $ 100 for registering every woman  into our dating agency, the number of posted profiles is unlimited. If you use a second system then every month your income will increase and again the amount of profiles you register is unlimited. Our active partners earn from $ 700 to $ 8000 per month.")?><!--</p>-->
                    </div>
                </div>
                <div class="row offset02">
                    <div class="grid_6 left">
                        <h4><?= Yii::t('app','How do I recieve my money ?')?> </h4>
                        <p><?= Yii::t('app','All our partners receive payment at the beginning of each month. We pay using the following methods: money transfer to a bank card,  Western Union or Paypal system.')?></p>
                    </div>
                    <div class="grid_6 left">
                        <h4><?= Yii::t('app','What system of cooperation should I choose?')?></h4>
                        <p><?= Yii::t('app',"Each system is individual and works well for different partners in different ways. The first one will immediately bring great income in the form of $ 100 for each profile. Second system allows you to make your business in the future as you get paid  $ 20 for the whole time until the woman's profile is active in our database.")?></p>
                    </div>
                </div>
            </div>
        </section>

        <section class="well well__04 bg01">
            <div class="container">
                <h2><?= Yii::t('app','Start making money right now')?> </h2>
                <p><?= Yii::t('app',"After registration you will receive a letter in the mail with a link to activate your profile.
                    After confirmation you can start working. In all matters please use the link to the administrator's contacts -  we will be happy to answer all your questions.")?></p>
                <a href="#" class="btn"><?= Yii::t('app','Get Started Now!')?></a>
            </div>
        </section>
    </main>
