<?php
$this->title = 'Meliapartner';
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>


<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="./"><?= Yii::$app->params['siteName']; ?></a></h1>
                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>


    <section class="well center well__06 bg01 shadow m-height sectionPadding">
        <div class="container hidden-door">
            <h2>Вход</h2>


            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute('/site/agency')
            ]);?>
            <!-- <form method="get" action="<?= Url::toRoute('/site/agency'); ?>"> -->
            <input type="password" name="pass">
            <?= Html::submitButton('Ok', ['class' => 'btn']); ?>
            <!-- </form> -->
            <?php ActiveForm::end(); ?>



        </div>
    </section>


</main>
