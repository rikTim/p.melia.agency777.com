<?php
use yii\helpers\Url;
use app\components\MenuWidget;

$this->title = 'Information about the agency';
?>
<?php Yii::$app->language = $_COOKIE['lang']; ?>

<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="./"><?= Yii::$app->params['siteName']; ?></a></h1>

                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>
    <?= MenuWidget::widget(); ?>

    <section class="well center well__06 bg01 shadow filter-block">
        <h2><?= Yii::t('app', 'Information about the agency') ?></h2>
    </section>


    <section class="well center m-height">
        <div class="container news">
            <div class="row">
                <p><?= $aboutPage; ?>
                </p>
            </div>
        </div>
    </section>
</main>
