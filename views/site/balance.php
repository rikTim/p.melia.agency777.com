<?php
use app\components\MenuWidget;

$this->title = 'Balance';
?>
<?php Yii::$app->language = $_COOKIE['lang']; ?>

<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="./"><?= Yii::$app->params['siteName']; ?></a></h1>
                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>
    <?= MenuWidget::widget(); ?>


    <section class="well center well__06 bg01 shadow filter-block">
        <h2><?= Yii::t('app', 'Balance') ?></h2>
    </section>


    <section class="well center m-height balances">
        <div class="container">
            <center style="color:#4C69B3;">
                <?= Yii::$app->session->getFlash('msg'); ?>
            </center>
            <div class="balance-table-wrapper">
                <table>
                    <tr>
                        <th><?= Yii::t('app', 'Action') ?></th>
                        <th><?= Yii::t('app', 'Date') ?></th>
                        <th><?= Yii::t('app', 'Money') ?></th>
                    </tr>
                    <?php if (!empty($balanceList)): ?>
                        <?php foreach ($balanceList as $balance): ?>
                            <tr>
                                <td><?= ($balance->transaction < 0) ? Yii::t('app', 'Get money') : Yii::t('app', 'Add new girl'); ?></td>
                                <td><?= Yii::$app->formatter->asDatetime($balance->datetime, "php:d.m.Y"); ?></td>
                                <td class="<?= ($balance->transaction < 0) ? 'sub' : 'add'; ?>">
                                    <?php if ($balance->transaction < 0): ?>
                                        <?= '-$'.str_replace('-', '', $balance->transaction); ?>
                                    <?php else: ?>
                                        <?= '$'.$balance->transaction; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>


                        <?php $total = (!empty($total)) ? $total : 0; ?>
                        <tr class="total" data-money="<?= $total; ?>">
                            <td></td>
                            <td><?= Yii::t('app', 'Total') ?></td>
                            <td>$<?= $total; ?></td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <td></td>
                            <td><h4><?= Yii::t('app', 'No active trancactions') ?></h4></td>
                            <td></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>

            <?php if (!empty($balanceList)): ?>
                <div class="balance-btn-wrapper">
                    <button type="button" class="btn-noshadow getMoney"><?=yii::t('app','Get money');?></button>
                </div>


                <div class="get-money-block getMoneyBlock">
                    <input type="text" class="getMoney">
                    <button type="button" class="btn-noshadow getMoneyBtn"><?=yii::t('app','Get');?></button>
                    <p class="msg error"><?=Yii::t('app','The requested amount is more than you have in the account');?></p>
                </div>
            <? endif; ?>

        </div>
    </section>
</main>
