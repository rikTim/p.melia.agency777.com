<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\components\MenuWidget;

$this->title = 'Meliapartner';
?>
<?php Yii::$app->language = $_COOKIE['lang']; ?>
<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="./"><?= Yii::$app->params['siteName']; ?></a></h1>

                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>
<!--==============================CONTENT==============================-->
<main>
    <?= MenuWidget::widget(); ?>
    <?php if (empty($thanksText)): ?>
        <section class="well center well__06 bg01 shadow m-height filter-block">
            <div class="container contact">
                <h2><?= Yii::t('app', 'Statistics') ?> </h2>

                <div class="row">
                    <div>
                        <canvas id="statistics" height="450" width="600"></canvas>
                        <h4 class="no-register-girls"><?= Yii::t('app', 'No registered girls') ?></h4>
                    </div>
                </div>
            </div>
        </section>

        <section class="well__05 filter-block input-data-block">
            <div class="row">
            </div>
            <div class="input-block">
                <label for="filterFrom"><?= Yii::t('app', 'From') ?></label>
                <input type="text" value="<?= $from; ?>" class="filterFrom" id="filterFrom">
                <label for="filterTo"><?= Yii::t('app', 'To') ?></label>
                <input type="text" value="<?= $to; ?>" class="filterTo" id="filterTo">
            </div>
            <span class="btn filterBtn"><?= Yii::t('app', 'Filter') ?></span>
        </section>
    <?php else: ?>
    <?php if (!empty($user) && $user->is_greeting == 0): ?>
        <section class="well center well__06 bg01 shadow filter-block">
            <div class="container contact">
                <h2><?= Yii::t('app', 'Thank you for registering'); ?></h2>
                <?php if (!empty($thanksText)): ?>
                    <div class="row">
                        <?= $thanksText; ?>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endif; ?>
    <?php if (!empty($aboutPage)): ?>
    <?php if (!empty($user) && $user->is_greeting == 0): ?>
    <section class="well center">
        <?php else: ?>
        <section class="well center well__06 bg01 shadow m-height filter-block">
            <?php endif; ?>

            <div class="container news">
                <h2><?= Yii::t('app', 'Training video') ?></h2>

                <div class="row">

                    <?php if ($_COOKIE['lang'] == 'Ru'): ?>

                        <video controls>
                            <source src='http://meliapartner.com/web/images/training/rus.mp4'>

                        </video>
                    <?php else: ?>
                        <video controls>

                            <source src='http://meliapartner.com/web/images/training/eng.mp4'>
                        </video>
                    <?php endif; ?>
                </div>
            </div>


            <div class="container">
                <h2><?= Yii::t('app', 'Choose the way of making money'); ?></h2>

                <p><?= $aboutPage->$lntext; ?></p>

                <div class="row">

                    <div class="grid_6">
                        <div data-getmoneytype="2" class="getMoneyType">
                            <div style="font: 65px Arial; padding-top: 15px;height: 90px; margin-top:0;"
                                 class="get-money icon fa fa-money"></div>
                        </div>
                        <h3><?= Yii::t('app', 'In fact'); ?></h3>

                        <p class="small"><?= Yii::t('app', 'Earning up to $ 100 for each registered girl.'); ?></p>
                    </div>

                    <div class="grid_6">
                        <div data-getmoneytype="2" class="getMoneyType get-money icon">
                            <div style="font: 65px Arial; padding-top: 15px;height: 90px; margin-top:0;"
                                 class="get-money icon fa fa-calendar"></div>
                        </div>
                        <h3><?= Yii::t('app', 'Per month'); ?></h3>

                        <p class="small"><?= yii::t('app', 'Earning $ 20 a month for each attracted client in a project for as long as she does not remove your profile from the database of our agency'); ?></p>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>
        <?php endif; ?>


</main>

