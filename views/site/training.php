<?php
use yii\helpers\Url;
use app\components\MenuWidget;

$this->title = 'Information about the agency';
?>
<?php Yii::$app->language = $_COOKIE['lang']; ?>

<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="./"><?= Yii::$app->params['siteName']; ?></a></h1>

                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>
    <?= MenuWidget::widget(); ?>

    <section class="well center well__06 bg01 shadow filter-block">
        <h2><?= Yii::t('app', 'Training') ?></h2>
    </section>


    <section class="well center m-height">

        <div class="container news">

            <div class="container news">
                <h3><?= Yii::t('app', 'Training video') ?></h3>

                <div class="row">

                    <?php if ($_COOKIE['lang']== 'Ru'): ?>

                    <video  controls>
                        <source  src='http://meliapartner.com/web/images/training/rus.mp4'>

                    </video>
                    <?php else: ?>
                    <video  controls>

                        <source src='http://meliapartner.com/web/images/training/eng.mp4'>
                    </video>
                                                    <?php endif;?>
                </div>
            </div>




            <h3><?= Yii::t('app', 'Photo examples') ?></h3>

            <div class="row">
                <div class="girl-album">
                    <span class="photo">
                <a class="profile-gallery-item"
                   href="<?= 'http://meliapartner.com/web/images/training/image-1.jpg' ?>"
                   data-fancybox-group="gallery" title="">
                    <img class="training" src="<?= 'http://meliapartner.com/web/images/training/image-1.jpg' ?>"
                         alt="">
                </a>
                    </span>
                     <span class="photo">
                <a class="profile-gallery-item"
                   href="<?= 'http://meliapartner.com/web/images/training/image-2.jpg' ?>"
                   data-fancybox-group="gallery" title="">
                    <img class="training" src="<?= 'http://meliapartner.com/web/images/training/image-2.jpg' ?>"
                         alt="">
                </a>
                </span>
                     <span class="photo">
                <a class="profile-gallery-item"
                   href="<?= 'http://meliapartner.com/web/images/training/image-3.jpg' ?>"
                   data-fancybox-group="gallery" title="">
                    <img class="training" class="training" "
                    src="<?= 'http://meliapartner.com/web/images/training/image-3.jpg' ?>"
                    alt="">
                </a>
                         </span>
                         <span class="photo">
                <a class="profile-gallery-item"
                   href="<?= 'http://meliapartner.com/web/images/training/image-4.jpg' ?>"
                   data-fancybox-group="gallery" title="">
                    <img class="training" src="<?= 'http://meliapartner.com/web/images/training/image-4.jpg' ?>"
                         alt="">
                </a>
                             </span>
                </div>
            </div>
            <div class="row">
                <div class="girl-album">
                             <span class="photo">
                <a class="profile-gallery-item"
                   href="<?= 'http://meliapartner.com/web/images/training/image-5.jpg' ?>"
                   data-fancybox-group="gallery" title="">
                    <img class="training" src="<?= 'http://meliapartner.com/web/images/training/image-5.jpg' ?>"
                         alt="">
                </a>
                                 </span>
                                  <span class="photo">
                <a class="profile-gallery-item"
                   href="<?= 'http://meliapartner.com/web/images/training/image-6.jpg' ?>"
                   data-fancybox-group="gallery" title="">
                    <img class="training" src="<?= 'http://meliapartner.com/web/images/training/image-6.jpg' ?>"
                         alt="">
                </a>
                </span>
                                             <span class="photo">
                <a class="profile-gallery-item"
                   href="<?= 'http://meliapartner.com/web/images/training/image-7.jpg' ?>"
                   data-fancybox-group="gallery" title="">
                    <img class="training" src="<?= 'http://meliapartner.com/web/images/training/image-7.jpg' ?>"
                         alt="">
                </a>
                </span>
                             <span class="photo">
                <a class="profile-gallery-item"
                   href="<?= 'http://meliapartner.com/web/images/training/image-8.jpg' ?>"
                   data-fancybox-group="gallery" title="">
                    <img class="training" src="<?= 'http://meliapartner.com/web/images/training/image-8.jpg' ?>"
                         alt="">
                </a>
                </span>
                </div>
            </div>
        </div>

    </section>
</main>