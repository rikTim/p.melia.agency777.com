<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\components\MenuWidget;

$this->title = 'Contact';
?>
<?php Yii::$app->language = $_COOKIE['lang']; ?>
<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="./"><?= Yii::$app->params['siteName']; ?></a></h1>

                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>
<!--==============================CONTENT==============================-->
<main>
    <?= MenuWidget::widget(); ?>

    <section class="well center well__06 bg01 shadow filter-block">
        <h2><?= Yii::t('app', 'Contacts') ?></h2>
    </section>

    <section class="well center m-height">
        <div class="container contact">
            <?php if (!empty($contactPage)): ?>
                <div class="row" style="text-align: justify;">
                    <?= $contactPage; ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($partnerId)): ?>
                <div class="row">
                    <h5><?= Yii::t('app', 'Write message to admin') ?></h5>
                    <div class="grid_12">
                        <?php if (!empty($messages)): ?>
                            <div class="chat">
                                <?php foreach ($messages as $message): ?>
                                    <div class="message <?php if ($message->fromuserid == $partnerId) echo 'right-mess'; ?>">
                                        <?php if ($message->fromuserid != $partnerId): ?>
                                            <span class="avatar">
                                                <img src="http://meliapartner.com/web/images/5e3affe0.png" alt="">
                                            </span>
                                        <?php endif; ?>
                                        <div class="mess">
                                            <span class="text">
                                                <?= $message->text; ?>
                                                <br>
                                                <span class="time">
                                                    <?= Yii::$app->formatter->asDatetime($message->datetime, "php:H:i d.m.Y"); ?>
                                                </span>
                                            </span>
                                        </div>
                                        <?php if ($message->fromuserid == $partnerId): ?>
                                            <span class="avatar">
                                            <?php $filePath = Yii::$app->getUrlManager()->getBaseUrl() . '/' . $file; ?>
                                                <?php if (!empty($file) && file_exists($filePath)): ?>
                                                    <img src="<?= $filePath; ?>">
                                                <?php else: ?>
                                                    <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/no-image-man.png" alt="">
                                                <?php endif; ?>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="clr"></div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row">
                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                    <div class="box-body profile-form">
                        <div class="form-group">
                            <?= $form->field($model, 'text')->textArea(['rows' => 6, 'placeholder' => Yii::t('app', 'Message to admin')])->label(false); ?>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="form-group">
                            <center><?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn']) ?></center>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            <?php else: ?>
                <div class="row">
                    <h5> <?= Yii::t('app', 'Fill your profile to write messages to admin')?></h5>
                </div>
            <?php endif; ?>
        </div>
    </section>
</main>