<?php
$this->title = 'Get money type';
?>

<?php Yii::$app->language = $_COOKIE['lang']; ?>
<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="./"><?= Yii::$app->params['siteName']; ?></a></h1>

                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>
    <section class="well__05">
        <div class="container sectionPadding">
            <h2  style="display: inline-block;"><?= Yii::t('app','Payment method');?></h2>
            <nav style="display: inline-block; margin-top: 12px;">
                <ul>
                    <li class="lang-select"><span class="selected-lang"></span>
                        <ul>
                            <!--                        <li>Es</li>-->
                            <li>En</li><li>Ru</li><li>Cz</li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </section>

    <section class="well center well__06 bg01 shadow m-height rules-page">
        <div class="container news">
            <div class="row">
                <?php if (!empty($aboutPage)): ?>
                    <p><?= $aboutPage->$lntext; ?></p>
                <?php endif; ?>

                <center>
                    <span class="label-wrapper"><label for="second"><?= Yii::t('app','In fact');?></label></span>
                    <input data-getmoneytype="2" class="get-type" type="radio" name="getmoneytype" id="second">
                    <span class="label-wrapper"><label for="first"><?= Yii::t('app','Per month');?></label></span>
                    <input data-getmoneytype="1" class="get-type" type="radio" name="getmoneytype" id="first">


                    <span class="btn money-type unactive btn-nomargintop"><?= Yii::t('app', 'Edit profile') ?></span>
                </center>

            </div>
        </div>
    </section>
</main>
