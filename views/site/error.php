<?php
$this->title = '404';
?>


<header>
    <section class="">
        <div class="top-panel">
            <div class="container">
                <div class="brand">
                    <h1 class="brand_name"><a href="./"><?= Yii::$app->params['siteName']; ?></a></h1>
                    <p class="brand_slogan">dating site</p>
                </div>
            </div>
        </div>
    </section>
</header>

<!--==============================CONTENT==============================-->
<main>
    <section class="well center well__06 bg01 shadow m-height sectionPadding">
        <div class="container not-fount">
            <h2><i class="fa fa-exclamation-triangle"></i> 404 Page not found</h2>
            <a href="http://meliapartner.com">Go home</a>
        </div>
    </section>
</main>
