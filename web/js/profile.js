$(function () {

    /*========================================================
     Fast edit profile btn
     =========================================================*/
    $('.control-btns .fast-edit').on('click', function () {
        var dataStep = $('.profile .step-btn-index:not(.unactive)').data('step').split('_');
        var step = dataStep[1];
        var girlId = $('.control-btns .girlId').val();

        location.href = 'edit?id='+girlId+'#'+step;
    });
    var pathname = location.pathname;
    if (pathname.indexOf('addgirl/edit') + 1) {
        var hash = parseInt(location.hash.replace('#', ''));
        if (hash >= 1 && hash <= 4 ) {
            var hashClass = 'step_'+hash;
            functions.hideSteps();
            functions.unactivateBtns();
            $('.'+hashClass).show();
            $('span[data-step='+hashClass+']').removeClass('unactive');
            if (hash == 4) {
                $('.control-btns .next-step').hide();
                $('.control-btns .prev-step').css('display', 'inline-block');
            } else if (hash > 2 && hash < 4) {
                $('.control-btns .prev-step').css('display', 'inline-block');
                $('.control-btns .next-step').css('display', 'inline-block');
            }
        }
    }
    /*================================================================================================================*/


    /*========================================================
     Girls Profile scripts
     =========================================================*/
    // steps btns index profile
    $('.profile .step-btn-index').on('click', function () {
        var $this = $(this);
        var step  = $this.data('step');
        functions.unactivateBtns();
        functions.hideSteps();
        $this.removeClass('unactive');
        $('.'+step).show();
    });


    // profile steps
    var isNew = $('#is_new').val();
    var $nextStep = $('.control-btns .next-step');
    var $prevStep = $('.control-btns .prev-step');
    var $submitBtn = $('.control-btns .save-profile');

    // если редактируем запись, тогда есть возможно переключения по верхним вкладкам
    if (isNew == 0) {
        $('.profile .step-btn').on('click', function () {
            var $this = $(this);
            var step  = $this.data('step');
            functions.unactivateBtns();
            functions.hideSteps();
            $this.removeClass('unactive');
            $('.'+step).show();
            $submitBtn.hide();
        });

        $('.profile .step-btn:not([data-step=step_1])').on('click', function () {
            $prevStep.css('display', 'inline-block');
            $nextStep.css('display', 'inline-block');
        });
        $('.profile .step-btn[data-step=step_5]').on('click', function () {
            $nextStep.hide();
            $submitBtn.css('display', 'inline-block');
        });
        $('.profile .step-btn[data-step=step_1]').on('click', function () {
            $prevStep.hide();
            $nextStep.css('display', 'inline-block')
        });
    }


    // valiodate email
    $('.profile #girls-email').focusout(function () {
        var $this = $(this);
        var email = $this.val();

        if (email.length > 0) {
            $.ajax({
                type: 'POST',
                data: {'email': email},
                url: 'checkemail',
                success: function (data) {
                    if (data.length) {
                        $this.addClass('error');
                        $this.next().show().text(data);
                    }
                }
            });
        }
    });


    // validate user name
    $('.profile #users-username').focusout(function () {
        var $this = $(this);
        var username = $this.val();

        if (username.length > 0) {
            $.ajax({
                type: 'POST',
                data: {'username': username},
                url: 'checkusername',
                success: function (data) {
                    if (data.length) {
                        $this.addClass('error');
                        $this.next().show().text(data);
                    }
                }
            });
        }
    });

    // control buttons
    // next btn
    $nextStep.on('click', function () {
        functions.stepBtns($(this), 'next');
    });
    // prev btn
    $prevStep.on('click', function () {
        functions.stepBtns($(this), 'prev');
        $nextStep.css('display', 'inline-block');
    });



    /*==================================================================================================================
     Upload files
     =================================================================================================================*/
    $('.profile .uploadIdentificationVideo').on('click', function () {
        $('.profile .uploadVideo').trigger('click');
    });

    $('.profile .step_1 .uploadPersonalImage').on('click', function () {
        $('.step_1 .uploadPassport').trigger('click');
    });

    $('.profile input[type=file]').on('change', function () {
        var $this = $(this);
        var val = $this.val();
        var type = $this.data('type');
        var $profilePassportTest = $('.profile .fileName'+type);
        val = val.split('\\')[2];

        $profilePassportTest.show();
        $profilePassportTest.find('.name').text(val);
    });

    $('.profile .deleteFile').on('click', function () {
        var $this = $(this);
        var type = $this.data('type');
        var $profilePassportTest = $('.profile .fileName'+type);

        $('.profile .step_1 input[data-type=' + type + ']').val('');
        $profilePassportTest.hide();
        $profilePassportTest.find('.name').text('');
    });
    /*================================================================================================================*/




    // upload avatar
    $('.step_1 .uploadAvatar').on('click', function () {
        $('.step_1 .uploadAvatarInput').trigger('click');
    });

    // country and city names to hidden inputs (partner register)
    $('#profileForm').submit(function() {
        functions.geoDropDown('.partnersCountry', '.partnersCity');
    });

    // country and city names to hidden inputs (girls register)
    $('.save-profile').click(function() {
        //$('#girlProfileForm').submit(function() {
        //functions.geoDropDown('.girlsCountry', '.girlsCity');
        functions.girlsHome('.girlsCountry', '.girlsCity', '.girlsName');
    });

    // remove error class from inputs
    $('input, textarea').on('click', function () {
        $(this).removeClass('error');
    });

    //change avatar
    $('.profile .coverImg img').on('click', function () {
        $('.profile .girlAvatar').trigger('click');
    });
    $('.profile .girlAvatar').on('change', function () {
        var girlAvatar = $(this).val();
        $('.profile .updateAvatar').trigger('click');
    });
    /*========================================================*/





    /*========================================================
     Delete photo
     =========================================================*/
    $('.girl-album .del-photo').on('click', function () {
        var $this = $(this);
        var photoId = $this.data('photo');

        $.ajax({
            type: 'POST',
            data: {'photoId': photoId},
            url: 'del-album-photo',
            success: function () {
                $this.parent().remove();
            }
        })
    });
    /*========================================================*/






    /*========================================================
     Save btn
     =========================================================*/
    $('.saveProfile').on('click', function(){
        $('.box-model-profile-wrapper').show();
    });
});
