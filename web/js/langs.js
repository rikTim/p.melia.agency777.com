$(function () {

    var lang = functions.getCookie('lang');
    if (typeof lang == 'undefined') {
        lang = 'En';
        document.cookie = "lang=En;path=/;";
    }
    $('.langs-list li[data-lang=' + lang + ']').addClass('active');

    $('.langs-list li').on('click', function () {
        var $this = $(this);
        var lang = $this.data('lang');
        console.log(lang);
        document.cookie = "lang=" + lang + ";path=/;";
        location.reload();
    });

});