$(function () {


    /*========================================================
     Login singin buttons
     =========================================================*/
    $('.logBtn').on('click', function () {
        var $this = $(this);
        var data = $this.data('log');

        $this.parents('.form').hide();
        $('.' + data).show();
    });
    /*========================================================*/



    /*========================================================
     Social auth
     =========================================================*/
    var $socialBtns = $('.social-btns');

    if ($socialBtns.length > 0) {
        $socialBtns.find('.vkRegister').on('click', function () {
            $('.eauth-service-id-vkontakte a').trigger('click');
        });
        $socialBtns.find('.fbRegister').on('click', function () {
            $('.eauth-service-id-facebook a').trigger('click');
        });
    }



    /*========================================================
     Filter scripts
     =========================================================*/
    var $filterBlock = $('.filter-block');

    if ($filterBlock.length > 0) {
        $filterBlock.find('.filterBtn').on('click', function () {
            $.ajax({
                type: 'POST',
                url: 'get-statistics',
                dataType: 'json',
                data: {
                    'filterFrom': $filterBlock.find('#filterFrom').val(),
                    'filterTo': $filterBlock.find('#filterTo').val(),
                    _csrf: $('meta[name="csrf-token"]').attr("content")
                },
                success: function (data) {
                    functions.drowCharts(data, 'statistics');
                }
            });
        });
    }
    /*========================================================*/



    /*========================================================
     Accept rules check button
     =========================================================*/
    $('.accept-rules').on('change', function () {
        var check = $(this).prop('checked');
        if (check == true) {
            $('.to-edit').addClass('to-edit-form').removeClass('unactive');
        } else {
            $('.to-edit').removeClass('to-edit-form').addClass('unactive');
        }
    });


    $(document).on('click', '.to-edit-form', function () {
        var $this = $(this);
        $.ajax({
            type: 'POST',
            url: 'log',
            success: function () {
                $this.closest('.arcticmodal-container').hide();
                $('.arcticmodal-overlay').hide();
            }
        });
    });
    /*========================================================*/




    /*========================================================
     Get money type
     =========================================================*/
    (function($) {

        $('.getMoneyType').on('click', function () {
            var getMoneyType = $(this).data('getmoneytype');


            $.ajax({
                type: 'POST',
                data: {
                    'getMoneyType': getMoneyType
                },
                url: 'type-money',
                success: function () {
                    //location.reload();
                }
            })
        });

    })(jQuery);
    /*========================================================*/



    /*========================================================
     Get money from balance
     =========================================================*/
    (function($) {
        var $balances = $('.balances');

        if ($balances.length > 0) {
            var $getMoneyInput = $balances.find('input.getMoney');
            var $errorMsg = $balances.find('.msg.error');

            $balances.find('.getMoney').on('click', function () {
                $balances.find('.getMoneyBlock').show();
                $getMoneyInput.focus();

            });

            $balances.find('.getMoneyBtn').on('click', function () {
                var totalSum = parseInt($balances.find('.total').data('money'));
                var wantedSum = parseInt($getMoneyInput.val());


                if (wantedSum > totalSum) {
                    $errorMsg.show();
                    return false;
                } else {
                    $.ajax({
                        type: 'POST',
                        data: {
                            'wantedSum': wantedSum
                        },
                        url: 'get-money',
                        success: function () {
                            location.reload();
                        }
                    })
                }
            });

            $getMoneyInput.on('focusin', function () {
                $errorMsg.hide();
            })
        }
    })(jQuery);
    /*========================================================*/














































});

