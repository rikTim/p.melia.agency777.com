function include(scriptUrl) {
    document.write('<script src="' + scriptUrl + '"></script>');
}

function isIE() {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
};


/* cookie.JS
 ========================================================*/
;
(function ($) {
    $(document).ready(function () {
        var $selectedLang = $('.lang-select .selected-lang');
        if ($selectedLang.length != 0) {
            var cookieLang = $.cookie('lang');

            $selectedLang.text(cookieLang);
            if ($selectedLang.text() == '' || cookieLang == null) {
                document.cookie = "lang=En;path=/;";
                $selectedLang.text('En');
            } else {
                $selectedLang.text(cookieLang);
            }

            // language select
            $('.lang-select').on('click', function () {
                var $this = $(this);
                $this.children().toggle();
            });

            $('.lang-select li').on('click', function () {
                var $this = $(this);
                var lang = $this.text();
                $('.lang-select .selected-lang').text(lang);
                functions.deleteCookie('lang');
                document.cookie = "lang=" + lang + ";path=/;";
                location.reload();
            });
        }

    })
})(jQuery);


/* Copyright Year
 ========================================================*/
;
(function ($) {
    $(document).ready(function () {
        $("#copyright-year").text((new Date).getFullYear());
    });
})(jQuery);


/* Orientation tablet fix
 ========================================================*/
$(function () {
    // IPa  d/IPhone
    var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
        ua = navigator.userAgent,

        gestureStart = function () {
            viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";
        },

        scaleFix = function () {
            if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
                viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
                document.addEventListener("gesturestart", gestureStart, false);
            }
        };

    scaleFix();
    // Menu Android
    if (window.orientation != undefined) {
        var regM = /ipod|ipad|iphone/gi,
            result = ua.match(regM);
        if (!result) {
            $('.sf-menus li').each(function () {
                if ($(">ul", this)[0]) {
                    $(">a", this).toggle(
                        function () {
                            return false;
                        },
                        function () {
                            window.location.href = $(this).attr("href");
                        }
                    );
                }
            })
        }
    }
});
var ua = navigator.userAgent.toLocaleLowerCase(),
    regV = /ipod|ipad|iphone/gi,
    result = ua.match(regV),
    userScale = "";
if (!result) {
    userScale = ",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0' + userScale + '">');


/* SMOOTH SCROLLING
 ========================================================*/
;
(function ($) {
    var o = $('html');
    if (o.hasClass('desktop')) {
        include(window.baseUrl + '/js/jquery.mousewheel.min.js');
        include(window.baseUrl + '/js/jquery.simplr.smoothscroll.min.js');

        $(document).ready(function () {
            $.srSmoothscroll({
                step: 150,
                speed: 800
            });
        });
    }
})(jQuery);


/* Parallax
 ========================================================*/
;
(function ($) {
    var o = $('.parallax');
    if (o.length > 0 && $('html').hasClass('desktop')) {
        include(window.baseUrl + '/js/jquery.rd-parallax.js');

        $(function () {
            var obj;
            if ((obj = $('.parallax')).length > 0 && $('html').hasClass('desktop')) {
                for (var i = 0; i < obj.length; i++) {
                    if (isIE() && isIE() < 9) {
                        $(obj[i]).parallax($(obj[i]).css('backgroundPositionX'), obj[i].getAttribute('data-parallax-speed'), obj[i].getAttribute('data-parallax-offset'));
                    } else {
                        $(obj[i]).parallax($(obj[i]).css('background-position').split(" ")[0], obj[i].getAttribute('data-parallax-speed'), obj[i].getAttribute('data-parallax-offset'));
                    }
                }
            }

            function isIE() {
                var myNav = navigator.userAgent.toLowerCase();
                return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
            }
        });

    }
})(jQuery);


/* Popups
 ========================================================*/
;
(function ($) {
    $(function () {
        $('footer .copyright').on('click', function () {
            $('#copyrightModal').arcticmodal();
        });
        $('.contactBtn').on('click', function () {
            $('#contactModal').arcticmodal();
        });
        $('.rulesBtn').on('click', function () {
            $('#rulesModel').arcticmodal();
        });
        $('.saveProfile').on('click', function () {
            $('.successPopup').show();
        });
    });

})(jQuery);


/* Mask js
 ========================================================*/
;
(function ($) {
    var o = $('#phoneMask');
    if (o.length > 0) {
        include(window.baseUrl + '/js/jquery.mask.js');
        $(function () {
            $('#phoneMask').mask('(000) 000-00000');
            $('#girls-height').mask('000');
            $('#girls-weight').mask('000');
            $('.birthdate').mask('00.00.0000');
        });
    }
    // маска для поля вывода средств в балансах
    o = $('.balances');
    if (o.length > 0) {
        include(window.baseUrl + '/js/jquery.mask.js');
        $(function () {
            o.find('input.getMoney').mask('0000000');
        });
    }
})(jQuery);


/* My scripts
 ========================================================*/
include(window.baseUrl + '/js/my.js');
include(window.baseUrl + '/js/langs.js');
include(window.baseUrl + '/js/profile.js');
include(window.baseUrl + '/js/messanger.js');


/* Booking Form
 ========================================================*/
;
(function ($) {
    var o = $('#bookingForm');
    if (o.length > 0) {
        include(window.baseUrl + '/js/booking/booking.js');
        include(window.baseUrl + '/js/booking/jquery-ui-1.10.3.custom.min.js');
        include(window.baseUrl + '/js/booking/jquery.fancyform.js');
        include(window.baseUrl + '/js/booking/jquery.placeholder.js');
        include(window.baseUrl + '/js/booking/regula.js');
        $(document).ready(function () {
            o.bookingForm({
                ownerEmail: '#'
            });
        });
    }
})(jQuery);


/* Pickmeup calendar
 ========================================================*/
include(window.baseUrl + '/js/jquery.pickmeup.min.js');
;
(function ($) {
    $('.filter-block .filterFrom').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        format: 'd.m.Y'
    });
    $('.filter-block .filterTo').pickmeup({
        position: 'bottom',
        hide_on_select: true,
        format: 'd.m.Y'
    });
})(jQuery);


/* Chosen
 ========================================================*/
;
(function ($) {
    var o = $('.profile-form');
    if (o.length > 0) {
        // @todo ПОПРАВИТЬ!!
        o.find('#girls-nameid').chosen({width: "100%"});
        o.find('#girls-education').chosen({width: "100%"});
        o.find('#girls-occupationid').chosen({width: "100%"});
        o.find('#girls-religionid').chosen({width: "100%"});
        o.find('#girls-materialstatusid').chosen({width: "100%"});
        o.find('#girls-languageid').chosen({width: "100%"});
        o.find('#girls-languageid2').chosen({width: "100%"});
        o.find('#girls-eyescolor').chosen({width: "100%"});
        o.find('#girls-haircolor').chosen({width: "100%"});
        o.find('#girls-characterid').chosen({width: "100%"});
        o.find('#girls-cityid').chosen({width: "100%"});
    }


    var lang = functions.getCookie('lang');
    if (typeof lang == 'undefined') {
        lang = 'en';
    } else {
        lang = lang.toLowerCase();
    }
    include(window.baseUrl + '/js/chosen.jquery.js');
    include(window.baseUrl + '/js/prism.js');

    // http://habrahabr.ru/post/204840/
    var countryReq = 'http://api.vk.com/method/database.getCountries?v=5.5&need_all=1&count=1000&lang=' + lang;
    var $countrySelect = $('.country-list-chosen');
    if ($countrySelect.length > 0) {
        functions.geoName(countryReq, $countrySelect, false);
    }

    // occupation select list
    $('.profile-form #girls-occupation').chosen({
        width: "100%"
    });
})(jQuery);


/* Charts
 ========================================================*/
include(window.baseUrl + '/js/Chart.min.js');
;
(function () {
    var pathname = location.pathname;
    if (pathname.indexOf('main') + 1) {
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            type: 'POST',
            url: 'get-statistics',
            dataType: 'json',
            data: {
                _csrf: csrfToken
            },
            success: function (data) {
                functions.drowCharts(data, 'statistics');
            }
        });
    }
})(jQuery);


/* Fancy box
 ========================================================*/
;
(function ($) {
    include(window.baseUrl + '/js/jquery.mousewheel-3.0.6.pack.js');
    include(window.baseUrl + '/js/jquery.fancybox.js');
    $(function () {
        $('.profile-gallery-item').fancybox();
    });
})(jQuery);


/* D&D files
 ========================================================*/
;
(function ($) {
    include(window.baseUrl + '/js/jquery.filer.min.js');

    $(function () {
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        var girlUserId = $('.girlUserId').val();
        var girlId = $('#girls-id').val();
        $('#input2').filer({
            limit: 200,
            maxSize: null,
            extensions: ['jpg', 'jpeg', 'png', 'gif'],
            changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
            showThumbs: true,
            appendTo: null,
            theme: "dragdropbox",
            templates: {
                box: '<ul class="jFiler-item-list"></ul>',
                item: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                            <input type="hidden" name="photoes[][{{fi-name}}]">\
                        </li>',
                itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <span class="jFiler-item-others">{{fi-icon}} {{fi-size2}}</span>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                            <input type="hidden" name="photoes[][{{fi-name}}]">\
                        </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                removeConfirmation: false,
                _selectors: {
                    list: '.jFiler-item-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action',
                }
            },
            uploadFile: {
                url: "uploadalbums",
                dataType: 'json',
                type: 'POST',
                data: {_csrf: csrfToken, 'girlUserId': girlUserId, 'girlId': girlId},
                enctype: 'multipart/form-data',
                beforeSend: function () {
                },
                success: function (data, el) {
                    var parent = el.find(".jFiler-jProgressBar").parent();
                    el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                        $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
                    });
                },
                error: function (el) {
                    var parent = el.find(".jFiler-jProgressBar").parent();
                    el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                        $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                    });
                },
                statusCode: {},
                onProgress: function () {
                }
            },
            dragDrop: {
                dragEnter: function () {
                },
                dragLeave: function () {
                },
                drop: function () {
                }
            },
            addMore: true,
            clipBoardPaste: true,
            excludeName: null,
            beforeShow: function () {
                return true
            },
            onSelect: function () {
            },
            afterShow: function () {
            },
            onRemove: function () {
            },
            onEmpty: function () {
            },
            captions: {
                button: "Choose Files",
                feedback: "Choose files To Upload",
                feedback2: "files were chosen",
                drop: "Drop file here to Upload",
                removeConfirmation: "Are you sure you want to remove this file?",
                errors: {
                    filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                    filesType: "Only Images are allowed to be uploaded.",
                    filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                    filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
                }
            }
        });
    });




    $(function () {
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        var girlUserId = $('.girlUserId').val();
        var girlId = $('#girls-id').val();
        $('#input3').filer({
            limit: 1,
            maxSize: 10,
            extensions: ['avi', 'wmv', 'mpg', 'mpeg'],
            changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop video here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
            showThumbs: true,
            appendTo: null,
            theme: "dragdropbox",
            templates: {
                box: '<ul class="jFiler-item-list"></ul>',
                item: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                            <input type="hidden" name="photoes[][{{fi-name}}]">\
                        </li>',
                itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <span class="jFiler-item-others">{{fi-icon}} {{fi-size2}}</span>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                            <input type="hidden" name="photoes[][{{fi-name}}]">\
                        </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                removeConfirmation: false,
                _selectors: {
                    list: '.jFiler-item-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action',
                }
            },
            uploadFile: {
                url: "uploadvideo",
                dataType: 'json',
                type: 'POST',
                data: {_csrf: csrfToken, 'girlUserId': girlUserId, 'girlId': girlId},
                enctype: 'multipart/form-data',
                beforeSend: function () {
                },
                success: function (data, el) {
                    var parent = el.find(".jFiler-jProgressBar").parent();
                    el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                        $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
                    });
                },
                error: function (el) {
                    var parent = el.find(".jFiler-jProgressBar").parent();
                    el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                        $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                    });
                },
                statusCode: {},
                onProgress: function () {
                }
            },
            dragDrop: {
                dragEnter: function () {
                },
                dragLeave: function () {
                },
                drop: function () {
                }
            },
            addMore: true,
            clipBoardPaste: true,
            excludeName: null,
            beforeShow: function () {
                return true
            },
            onSelect: function () {
            },
            afterShow: function () {
            },
            onRemove: function () {
            },
            onEmpty: function () {
            },
            captions: {
                button: "Choose Video",
                feedback: "Choose Video To Upload",
                feedback2: "files were chosen",
                drop: "Drop video here to Upload",
                removeConfirmation: "Are you sure you want to remove this file?",
                errors: {
                    filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                    filesType: "Only Video are allowed to be uploaded.",
                    filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                    filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
                }
            }
        });
    });






})(jQuery);