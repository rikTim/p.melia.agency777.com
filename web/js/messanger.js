/*========================================================
 Messenger scripts
 =========================================================*/

(function($) {

    $(function () {

        var $messenger = $('.messenger');

        if ($messenger.length != 0) {
            $messenger.find('.contact').on('click', function () {
                var $this = $(this);
                var manId = $this.data('fromid');

                $messenger.find('.contact').each(function () {
                    $(this).removeClass('active');
                });
                $this.addClass('active');

                $.ajax({
                    type: 'POST',
                    url: 'get-messages',
                    data: {
                        'manId': manId
                    },
                    success: function (d) {
                        var $chat = $messenger.find('.chat');
                        $chat.empty();

                        var data = $.parseJSON(d);
                        for (var i = 0, len = data.length; i < len; i++) {
                            var rightMess = (data[i]['fromid'] == data[i]['girlId']) ? 'right-mess' : '';
                            var leftAvatar = (data[i]['fromid'] != data[i]['girlId']) ? '<span class="avatar"><img src="' + window.baseUrl + '/images/no-image-man.png" alt=""></span>' : '';
                            var rightAvatar = (data[i]['fromid'] == data[i]['girlId']) ? '<span class="avatar"><img src="' + window.baseUrl + '/images/no-image-girl.png" alt=""></span>' : '';

                            var str = '<div class="message  ' + rightMess + '  ">' + leftAvatar;
                            str += '<div class="mess"><span class="text">' + data[i]['text'] + '<br>';
                            str += '<span class="time">' + data[i]['datetime'] + '</span></span></div>' + rightAvatar;
                            str += '</div><div class="clr"></div>';

                            $chat.append(str);
                        }

                    }
                });
            });

            $messenger.find('.chatSendBtn').on('click', function () {
                var text = $messenger.find('.message-field textarea').val();
                if (text.length == '') {
                    return false;
                }
                var toid = $messenger.find('.contacts .active').data('toid');
                var fromid = $messenger('.contacts .active').data('fromid');

                $.ajax({
                    type: 'POST',
                    url: 'add-message',
                    data: {
                        'text': text,
                        'toid': toid,
                        'fromid': fromid
                    }
                });

            });
        }

        /*========================================================*/

    });

})(jQuery);