<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "staticpages".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 * @property string $text
 */
class Staticpages extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staticpages';
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru','title_en','title_es','title_cz','is_active','text_ru','text_en','text_es','text_cz'],'required'],
            [['is_active'],'integer'],
            [['text_ru','text_en','text_es','text_cz'],'string'],
            [['title_ru','title_es','title_cz'],'string','max'=>120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'=>Yii::t('app', 'ID'),
            'title_ru'=>Yii::t('app', 'Title_ru'),
            'title_en'=>Yii::t('app', 'Title_en'),
            'title_es'=>Yii::t('app', 'Title_es'),
            'title_cz'=>Yii::t('app', 'Title_cz'),
            'is_active'=>Yii::t('app', 'Is Active'),
            'text_ru'=>Yii::t('app', 'Text_ru'),
            'text_en'=>Yii::t('app', 'Text_en'),
            'text_es'=>Yii::t('app', 'Text_es'),
            'text_cz'=>Yii::t('app', 'Text_cz'),
        ];
    }
    public function getText($id)
    {
        return $this->findOne(['id'=>$id]);
    }
}
