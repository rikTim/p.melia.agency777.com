<?php

namespace app\models;

use Yii;

class Cities extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'cities';
    }

    public function rules()
    {
        return [];
    }

    public function attributeLabels()
    {
        return [];
    }

    public function getAllCities()
    {
        $cities = $this
            ->find()
            ->select(['topdatesid' => 'topdatesid', 'title' => 'title'])
            ->where(['is_active' => 1])
            ->asArray()
            ->all();

        $citiesList = [];
        foreach($cities as $city) {
            $citiesList[$city['topdatesid']] = $city['title'];
        }
        return $citiesList;
    }
}
