<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "balances".
 *
 * @property integer $id
 * @property integer $userid
 * @property integer $usertypeid
 * @property integer $transaction
 * @property integer $datetime
 *
 * @property Usertypes $usertype
 */
class Balances extends \yii\db\ActiveRecord
{
    public $partner;
    public $partnerid;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balances';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'usertypeid', 'transaction', 'datetime'], 'required'],
            [['userid', 'usertypeid', 'transaction', 'datetime'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userid' => Yii::t('app', 'Userid'),
            'usertypeid' => Yii::t('app', 'Usertypeid'),
            'transaction' => Yii::t('app', 'Transaction'),
            'datetime' => Yii::t('app', 'Datetime'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsertype()
    {
        return $this->hasOne(Usertypes::className(), ['id' => 'usertypeid']);
    }


    public function getPartners()
    {
        return $this->hasOne(Partners::className(), ['userid' => 'userid']);

    }

    public function getPartnerBalance($userId)
    {
        $balance = $this->find()
            ->select(['SUM(transaction) AS total'])
            ->where("(userid = ".$userId.")")
            ->groupBy("userid")
            ->asArray()
            ->one();

        return $balance['total'];
    }

    public function getPartnerBalanceList($userId)
    {
        return $this->find()
            ->where("userid = ".$userId)
            ->orderBy('datetime DESC')
            ->all();
    }

    public function addTransaction($userId, $transaction)
    {
        $this->userid = $userId;
        $this->usertypeid = 2;
        $this->transaction = -$transaction;
        $this->datetime = time();
        $this->save();
    }

    public function addMoneyPerGirl($userid)
    {
        $moneyModel = new Money();
        $amount = $moneyModel->getMoneyByType(2); // 2 - начисление денег по факту одобрения девушки

        $this->setAttributes([
            'userid' => $userid,
            'usertypeid' => 2, // 2 usertypeid партнера
            'transaction' => $amount,
            'datetime' => time()
        ]);

        $this->save();
    }

    public function recountMoneyPerMonthByUserId($userid, $partnerid)
    {
        $moneyModel = new Money();
        $girlsModel = new Girls();
        $amount = $moneyModel->getMoneyByType(1); // 1 - начисление денег по факту одобрения девушки

        $countGirlsPerMonth = $girlsModel->getCountOfGirlsByPartnerId($partnerid);

        $this->setAttributes([
            'userid' => $userid,
            'usertypeid' => 2, // 2 usertypeid партнера
            'transaction' => $amount * $countGirlsPerMonth,
            'datetime' => time()
        ]);
        $this->save();
        return true;
    }

    public function checkMonthrecountByUserId($userid)
    {
        $dateMonthBegin = strtotime(date('F Y'));
        $dateMonthEnd = strtotime(date('F Y', strtotime('+1 month')));

        $check = $this->find()
            ->select(['COUNT(id) AS count'])
            ->where('userid = ' . $userid . ' AND datetime >= '.$dateMonthBegin.' AND datetime <= ' .$dateMonthEnd)
            ->asArray()
            ->all();


        return $check[0]['count'];
    }
    public function getTransactionUser($userid)
    {
        return $this->find()
            ->where(['userid'=>$userid])
            ->orderBy('datetime DESC')
            ->limit(10)
            ->all();
    }
    public function getAlltransaction()
    {
        return $this->find()
            ->orderBy('datetime DESC')
            ->all();
    }
    public function getFullName($userid)
    {
       $partner = Partners::find()->where(['userid'=>$userid])->one();


        $fullname =$partner->name . ' ' . $partner->lastname;
        return $fullname;
    }
    public function getIdPartner($userid)
    {
        $partner = Partners::find()->where(['userid'=>$userid])->one();
        return $partner->id;
    }


}
