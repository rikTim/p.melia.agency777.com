<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chat_top_dates".
 *
 * @property integer $id
 * @property integer $girl_id
 * @property integer $men_id
 * @property string $men_name
 * @property string $text
 * @property integer $is_men_letter
 * @property integer $is_readed
 * @property integer $datetime
 */
class ChatTopDates extends \yii\db\ActiveRecord
{

    public $Translate = '';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chattopdates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_notification', 'girl_id', 'men_id', 'men_name', 'text', 'is_men_letter', 'is_readed', 'datetime'], 'required'],
            [['is_notification', 'girl_id', 'men_id', 'is_men_letter', 'is_readed', 'datetime'], 'integer'],
            [['text', 'image'], 'string'],
            [['men_name'], 'string', 'max' => 120],
            ['text', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'girl_id' => 'Girl ID',
            'men_id' => 'Men ID',
            'men_name' => 'Men Name',
            'text' => 'Text',
            'is_men_letter' => 'Is Men Letter',
            'is_readed' => 'Is Readed',
            'datetime' => 'Datetime',
        ];
    }

    public function getTdGirl()
    {
        return $this->hasOne(Girls::className(), ['topdatesid' => 'girl_id']);
    }




    public function getMessageFromGirl($id)
    {
        return $this->find()
            ->where(['girl_id' => $id, 'is_men_letter' => 0, 'is_aproved' => 0])
            ->all();
    }

    public function getMessageFromId($id)
    {
        return $this->find()
            ->where(['id' => $id])
            ->one();
    }

    public function getMessageMen($girl_id, $men_id)
    {
        return $this->find()
            ->where(['men_id' => $men_id, 'girl_id' => $girl_id, 'is_men_letter' => 1])
            ->orderBy('datetime DESC')
            ->one();
    }

    public function getCountMessage()
    {
        $count = $this->find()
            ->select(['COUNT(*) AS count'])
            ->where(['is_men_letter' => 0, 'is_aproved' => 0])
            ->asArray()
            ->all();

        return $count[0]['count'];
    }

    public function getNotificationNull()
    {
        return $this->find()
            ->where(['is_notification' => 0])
            ->all();
    }

    public function getGirl()
    {
        return $this->find()
            ->where(['is_men_letter' => 0, 'is_aproved' => 0])
            ->orderBy('datetime DESC')
            ->groupBy(['girl_id'])
            ->all();
    }


    public function answerAllMessages($manId, $girlId)
    {
        $this->updateAll(['is_answer' => 1], 'girl_id = ' . $girlId . ' AND men_id = '. $manId);
    }

}