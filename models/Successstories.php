<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "successstories".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $fileid
 * @property integer $is_active
 *
 * @property Files $file
 */
class Successstories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'successstories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'fileid', 'is_active'], 'required'],
            [['text'], 'string'],
            [['fileid', 'is_active'], 'integer'],
            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'fileid' => Yii::t('app', 'Fileid'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(Files::className(), ['id' => 'fileid']);
    }
}
