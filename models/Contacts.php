<?php

namespace app\models;

use Yii;

class Contacts extends \yii\db\ActiveRecord
{
    public $captcha;

    public static function tableName()
    {
        return 'contacts';
    }

    public function rules()
    {
        return [
            [['name', 'email', 'text', 'captcha'], 'required'],
            ['captcha', 'captcha'],
            ['email', 'email'],
            [['reason'], 'safe'],
            [['is_readed'],'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'reason' => 'Reason',
            'text' => 'Text',
            'datetime' => 'Datetime'
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->datetime = time();
        }
        return parent::beforeSave($insert);
    }

    public function message($data)
    {
        $this->attributes = $data;
        if ($this->save()) {
            $text = 'На meliapartner.com был оставлен вопрос от '.$data['name'].' <br />';
            $text .= 'email: '.$data['email'] .'<br />';
            $text .= 'текст сообщения: '.$data['text'];
            $text = iconv("UTF-8", "CP1251", $text);

//          swift mailer
            $res = Yii::$app->mail
                ->compose('auth', ['text' => $text])
                ->setFrom(['admin@meliapartner.com'=>'Meliapartner'])
                ->setTo('agencykievodessa@gmail.com')
                ->setSubject('New message')
                ->send();
            return $res;
        } else {
            return false;
        }
    }

    public static function getReason($id)
    {
        switch ($id) {
            case 1: $reason = "Вопрос"; break;
            case 2: $reason = "Неисправность"; break;
            case 3: $reason ="Другой вопрос"; break;
        }
        return $reason;
    }


    public function getCountMessage()
    {
        $count  = $this->find()
            ->select(['COUNT(*) AS count'])
            ->where(['is_readed'=>0])
            ->asArray()
            ->all();

        return $count[0]['count'];
    }

}
