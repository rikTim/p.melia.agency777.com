<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "statuses".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 *
 * @property Chats[] $chats
 * @property Girls[] $girls
 * @property Users[] $users
 */
class Statuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'required'],
            [['is_active'], 'integer'],
            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chats::className(), ['statusid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirls()
    {
        return $this->hasMany(Girls::className(), ['statusid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['statusid' => 'id']);
    }

    public function getAllUser()
    {
        $users = $this->find()->select(array('id' => 'id', 'title' => 'title'))->where(['is_active' => 1,'id'=>[5,6,7]])->asArray()->all();
        $usersList = [];
        foreach($users as $user) {
            $usersList[$user['id']] = $user['title'];
        }
        return $usersList;
    }

    public function getOneStatus($id)
    {
        return $this->find()->select(['title'=>'title'])->where(['id'=>$id])->asArray()->one();
    }

    public function getStatusById($id)
    {
        return $this->find()
            ->where('id ='.$id)
            ->one();
    }

}
