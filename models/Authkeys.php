<?php

namespace app\models;

use Yii;

class Authkeys extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'authkeys';
    }

    public function rules()
    {
        return [];
    }

    public function attributeLabels()
    {
        return [];
    }

    public function createKey()
    {
        $this->auth = Yii::$app->security->generateRandomString();
        $this->used = 0;
        $this->save();

        return $this->auth;
    }

    public function checkKey($auth)
    {
        $check = $this->find()
            ->where('auth = "'.$auth.'" AND used = 0')
            ->one();

        if (!empty($check)) {
            return true;
        } else {
            return false;
        }
    }


    public function useKey($auth)
    {
        $this->updateAll(['used' => 1], 'auth = "'.$auth.'"');
    }
}
