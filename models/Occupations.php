<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "occupations".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 */
class Occupations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'occupations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['is_active'], 'integer'],
            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    public function getAllOccupations()
    {
        $occupations = $this->find()->select(array('id' => 'id', 'title' => 'title'))->where(['is_active' => 1])->asArray()->all();
        $occupationsList = [];
        foreach($occupations as $occupation) {
            $occupationsList[$occupation['id']] = $occupation['title'];
        }
        return $occupationsList;
    }

    public function getOccupationByGirlId($id)
    {
        $occupation = $this->find()
            ->where('id = '.$id.' AND is_active = 1')
            ->one();
        return $occupation->title;
    }

    public function getOccupationsAll()
    {
        $occupations = $this->find()->select(['id' => 'id', 'title' => 'title'])->where(['is_active' => 1])->asArray()->all();
        $occupationsList = [];
        foreach($occupations as $occupation) {
            $occupationsList[$occupation['id']] = $occupation['title'];
        }
        return $occupationsList;
    }

    public  function getOneOccupations($id)
    {
        return $this->find()->select(['title'=>'title'])->where(['id'=>$id])->asArray()->one();
    }

    public function getOccupationById($id)
    {
        $occupation = $this->find()
            ->where('id = ' .$id. ' AND is_active = 1')
            ->one();

        return $occupation->title;
    }

}
