<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "men".
 *
 * @property integer $id
 * @property integer $topdatesid
 * @property string $name
 * @property string $avatar
 * @property integer $age
 * @property integer $height
 * @property integer $weght
 * @property string $eyes_color
 * @property string $hair_color
 * @property string $education
 * @property string $language
 * @property string $profesion
 * @property string $country
 * @property string $city
 * @property string $education_state
 * @property string $religion
 * @property string $material_status
 * @property string $smoker
 * @property string $alcohol
 */
class Men extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'men';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topdatesid', 'name'], 'required'],
            [['topdatesid', 'age', 'height', 'weight'], 'integer'],
            [['name', 'avatar', 'profesion'], 'string', 'max' => 120],
            [['eyes_color', 'hair_color', 'education', 'language', 'country', 'city', 'education_state', 'religion', 'material_status'], 'string', 'max' => 50],
            [['smoker', 'alcohol'], 'string', 'max' => 15],
            [['topdatesid'],'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'topdatesid' => 'Topdatesid',
            'name' => 'Name',
            'avatar' => 'Avatar',
            'age' => 'Age',
            'height' => 'Height',
            'weight' => 'Weght',
            'eyes_color' => 'Eyes Color',
            'hair_color' => 'Hair Color',
            'education' => 'Education',
            'language' => 'Language',
            'profesion' => 'Profesion',
            'country' => 'Country',
            'city' => 'City',
            'education_state' => 'Education State',
            'religion' => 'Religion',
            'material_status' => 'Material Status',
            'smoker' => 'Smoker',
            'alcohol' => 'Alcohol',
        ];
    }
}
