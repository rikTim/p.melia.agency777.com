<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "religions".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 *
 * @property Girls[] $girls
 */
class Religions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'religions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'is_active'], 'required'],
            [['is_active'], 'integer'],
            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirls()
    {
        return $this->hasMany(Girls::className(), ['religionid' => 'id']);
    }

    public function getAllReligions()
    {
        $religions = $this->find()->select(['id' => 'id', 'title' => 'title'])->where(['is_active' => 1])->asArray()->all();
        $religionsList = [];
        foreach($religions as $religion) {
            $religionsList[$religion['id']] = $religion['title'];
        }
        return $religionsList;
    }

    public function getReligionById($id)
    {
        $religion = $this->find()
            ->where('id = ' .$id. ' AND is_active = 1')
            ->one();

        return $religion->title;
    }


}
