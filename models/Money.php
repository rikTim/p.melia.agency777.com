<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "money".
 *
 * @property integer $id
 * @property string $type
 * @property integer $money
 */
class Money extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'money';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'money'], 'required'],
            [['money'], 'integer'],
            [['type'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'money' => 'Money',
        ];
    }

    public function getMoneyByType($type)
    {
        $amount = $this->find()->where('type ='.$type)->one();
        return $amount->money;
    }
}
