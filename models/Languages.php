<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "languages".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 *
 * @property Girlslanguages[] $girlslanguages
 */
class Languages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'is_active'], 'required'],
            [['is_active'], 'integer'],
            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirlslanguages()
    {
        return $this->hasMany(Girlslanguages::className(), ['langid' => 'id']);
    }

    public function getAllLanguages()
    {
        $languages = $this->find()->select(array('id' => 'id', 'title' => 'title'))->where(['is_active' => 1])->asArray()->all();
        $languagesList = [];
        foreach($languages as $language) {
            $languagesList[$language['id']] = $language['title'];
        }
        return $languagesList;
    }

    public function getLanguageById($id, $id2)
    {
        $langArr = [];
        if (empty($id)) {
            $id = 1;
        }
        $lang = $this->find()->where('id ='.$id)->one();
        $langArr[] = $lang->title;

        if (empty($id2)) {
            $id2 = 2;
        }
        $lang = $this->find()->where('id ='.$id2)->one();
        $langArr[] = $lang->title;

        return $langArr;
    }
}
