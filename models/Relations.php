<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "relations".
 *
 * @property integer $id_girl
 * @property integer $id_men
 * @property integer $datetime
 */
class Relations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'relations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unique_column'],'unique'],
            [['id_girl', 'id_men', 'datetime','unique_column'], 'required'],
            [['id_girl', 'id_men', 'datetime','unique_column'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_girl' => 'Id Girl',
            'id_men' => 'Id Men',
            'datetime' => 'Datetime',
        ];
    }

    public  function insertRecords($girl,$man,$uniq) {
        $this->id_girl = $girl;
        $this->id_men = $man;
        $this->datetime = time();
        $this->unique_column = $uniq;
        $this->save();
    }

    public function deleteRecords($time)
    {
        Yii::$app->db->createCommand("DELETE FROM relations WHERE datetime < '$time'")->query();
    }

}
