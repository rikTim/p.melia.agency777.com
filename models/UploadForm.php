<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;


class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $avatarFile;
    public $identVideo;

    public function rules()
    {
        return [
            [['imageFile'], 'image', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'minWidth' => 2048, 'maxWidth' => 2048, 'minHeight' => 1600, 'maxHeight' => 1600, 'on' => 'bgimage'],
        ];
    }

    public function upload($type, $userId)
    {
        if ($this->validate()) {
            $fullPath = '../../melia.agency777.com/web/media/' . $userId;
            $path = 'media/' . $userId;
            $imgName = md5($this->imageFile->baseName) . '.' . $this->imageFile->extension;

            BaseFileHelper::createDirectory($fullPath);

            $file = $fullPath . '/' . $imgName;
            $fileDb = $path . '/' . $imgName;
            $this->imageFile->saveAs($file);


            $filesModel = new Files();
            $filesModel->addFile($fileDb, $type, false);
            return $filesModel->id;
        } else {
            return false;
        }
    }

    public function uploadavatar($isPartner = false, $userId = '')
    {
        if ($isPartner == true) {
            $userId = Yii::$app->user->id;
            $fullPath = 'media/' . $userId;
        } else {
            $fullPath = '../../melia.agency777.com/web/media/' . $userId;
        }
        $path = 'media/' . $userId;
        $imgName = md5($this->imageFile->baseName) . '.' . $this->imageFile->extension;

        if ($this->validate()) {
            BaseFileHelper::createDirectory($fullPath);
            $file = $fullPath . '/' . $imgName;
            $fileDb = $path . '/' . $imgName;
            $this->imageFile->saveAs($file);
            $filesModel = new Files();
            $filesModel->addFile($fileDb, 1, true, ($isPartner == true) ? true : false);
            return true;
        } else {
            return false;
        }
    }

    public function uploadbgimage()
    {
        $this->scenario = 'bgimage';

        if ($this->validate()) {
            $fullPath = '../../melia.agency777.com/web/images/bg/';
            $path = 'images/bg/';
            $fileName = md5($this->imageFile->baseName) . '.' . $this->imageFile->extension;
            $file = $fullPath . $fileName;
            $fileDb = $path . $fileName;
            $this->imageFile->saveAs($file);

            $filesModel = new Files();
            $filesModel->addFile($fileDb, 1, false);
            return $filesModel->id;
        } else {
            return 1;
        }
    }

}