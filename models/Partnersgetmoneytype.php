<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partnersgetmoneytype".
 *
 * @property integer $id
 * @property integer $userid
 * @property string $getmoneytype
 */
class Partnersgetmoneytype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partnersgetmoneytype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'getmoneytype'], 'required'],

            [['getmoneytype'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'getmoneytype' => 'Getmoneytype',
        ];
    }

    public function getMoneytype()
    {
        return $this->hasOne(Money::className(), ['id' => 'getmoneytype']);
    }

    public function getPartner()
    {
        return $this->hasOne(Partners::className(), ['userid' => 'userid']);
    }

    public function getGetMoneyTypeByUserId($userid)
    {
        $getMoneyType = $this->find()
            ->where('userid = '. $userid)
            ->one();

        if ($getMoneyType->getmoneytype == 1) {
            $type = 'По месячная оплата';
        } else {
            $type = 'Оплата по факту';
        }
        return $type;
    }

    public function getGetMoneyTypeIdByUserId($userid)
    {
        $getMoneyType = $this->find()
            ->where('userid = '. $userid)
            ->one();
        return $getMoneyType->getmoneytype;
    }
    public function getIdPartner($userid)
    {
        $partner = Partners::find()->where(['userid'=>$userid])->one();
        return $partner->id;
    }
}
