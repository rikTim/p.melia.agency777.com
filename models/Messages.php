<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;
use \yii\db\CDbConnection;
use yii\web\User;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property integer $fromuserid
 * @property integer $fromtypeid
 * @property integer $touserid
 * @property integer $usertypeid
 * @property string $text
 * @property integer $datetime
 *
 * @property Usertypes $usertype
 * @property Usertypes $fromtype
 */
class Messages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fromuserid', 'touserid', 'text', 'datetime'], 'required'],
            [['fromuserid', 'touserid', 'datetime', 'girlid'], 'integer'],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fromuserid' => Yii::t('app', 'Fromuserid'),
            'touserid' => Yii::t('app', 'Touserid'),
            'text' => Yii::t('app', 'Text'),
            'datetime' => Yii::t('app', 'Datetime'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsertype()
    {
        return $this->hasOne(Usertypes::className(), ['id' => 'totypeid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromtype()
    {
        return $this->hasOne(Usertypes::className(), ['id' => 'fromtypeid']);
    }

    public function userId($userid, $text, $status)
    {
        $this->fromuserid = $userid;
        $this->fromtypeid = $status;
        $this->touserid = 3;
        $this->totypeid = 1;
        $this->text = $text;
        $this->datetime = time();
        $this->save();
    }

    // @todo универсильный метод для отправки сообщений, убрать все отсальные
    public function writeMessage($fromuserid, $fromtypeid, $touserid, $totypeid, $text, $is_readed = 0, $girlid = null)
    {
        $this->fromuserid = $fromuserid;
        $this->fromtypeid = $fromtypeid;
        $this->touserid = $touserid;
        $this->totypeid = $totypeid;
        $this->text = $text;
        $this->is_readed = $is_readed;
        $this->datetime = time();

        $this->save();
    }

    public function messagesUser($user, $text, $statusid, $touser)
    {
        $this->fromuserid = $user;
        $this->fromtypeid = $statusid;
        $this->touserid = $touser;
        $this->totypeid = 3;
        $this->text = $text;
        $this->datetime = time();
        $this->save(false);
    }

    public function getContactsMessages($userid)
    {
        $messages = $this->find()
            ->where("( fromuserid = 3 AND fromtypeid = 1 ) AND ( touserid = " . $userid . " AND totypeid = 2 ) OR ( fromuserid = " . $userid . " AND fromtypeid = 2 ) AND ( touserid = 3 AND totypeid = 1 )")
            ->orderBy('datetime DESC')
            ->all();
        if (!empty($messages)) {
            return $messages;
        } else {
            return false;
        }
    }

    public function getUserMessages($userid)
    {
        return $messages = $this->find()
            ->where("(fromuserid = " . $userid . " AND fromtypeid = 3) OR (touserid = " . $userid . " AND totypeid = 3)")
            ->groupBy("fromuserid")
            ->orderBy('datetime DESC')
            ->all();
    }

    public function getNewMessages($userid)
    {
        return $messages = $this->find()
            ->select(['COUNT(*) AS cnt'])
            ->where("(fromuserid = " . $userid . " AND fromtypeid = 3) OR (touserid = " . $userid . " AND totypeid = 3) AND is_readed = 0")
            ->groupBy("fromuserid")
            ->orderBy('datetime DESC')
            ->all();
    }

    public function getFirstManMessage($girlId, $manId)
    {
        $firstManMessages = $this->find()
            ->where("( fromuserid = " . $manId . " AND fromtypeid = 3 ) AND ( touserid = " . $girlId . " AND totypeid = 3 ) OR ( fromuserid = " . $girlId . " AND fromtypeid = 3 ) AND ( touserid = " . $manId . " AND totypeid = 3  )")
            ->orderBy('datetime DESC')
            ->all();


        $fmm = [];
        $i = 0;
        foreach ($firstManMessages as $key => $message) {
            $fmm[$i]['fromid'] = $message['fromid'];
            $fmm[$i]['text'] = $message['text'];
            $fmm[$i]['datetime'] = Yii::$app->formatter->asDatetime($message['datetime'], "php:H:i d.m.Y");
            $fmm[$i]['girlId'] = $girlId;
            $i++;
        }


        return $fmm;
    }

    public function messagePartner($user, $text, $status, $touser, $girlid)
    {
        $this->fromuserid = $user;
        $this->fromtypeid = $status;
        $this->touserid = $touser;
        $this->totypeid = 2;
        $this->text = $text;
        $this->datetime = time();
        $this->girlid = $girlid;
        $this->save();
    }


    // @todo может и не нужно
    public function getMessagesToAdminPartner($userid)
    {
        return $messages = $this->find()
            ->where("(fromuserid = " . $userid . " AND fromtypeid = 2 ) AND (touserid = 3 AND totypeid = 1) OR (fromuserid = 3  AND fromtypeid = 1) AND ( touserid =" . $userid . " AND totypeid = 2) ")
            ->orderBy('datetime DESC')
            ->all();
    }

    public function  getAllPartner()
    {
        return $messages = $this->find()
            ->select('fromuserid, touserid, text, datetime ')
            ->where(" (touserid = 3 AND totypeid = 1) AND(fromtypeid = 2) OR (fromuserid = 3  AND fromtypeid = 1) AND (totypeid = 2)  ")
            ->groupBy('fromuserid')
            ->orderBy('datetime DESC')
            ->all();
    }

    public function getTextMessage($userid)
    {
        return $message = $this->find()
            ->select("text,datetime")
            ->where("(fromuserid = " . $userid . " AND fromtypeid = 2 ) AND (touserid = 3 AND totypeid = 1) OR (fromuserid = 3  AND fromtypeid = 1) AND ( touserid =" . $userid . " AND totypeid = 2) ")
            ->orderBy('datetime DESC')
            ->one();
    }

    public function getOneMessage()
    {
        return $messages = $this->find()
            ->select('fromuserid, touserid')
            ->where(" (touserid = 3 AND totypeid = 1) AND(fromtypeid = 2) OR (fromuserid = 3  AND fromtypeid = 1) AND (totypeid = 2)  ")
            ->orderBy('datetime DESC')
            ->one();
    }

    public function getOneMessageG()
    {
        return $messages = $this->find()
            ->select('fromuserid, touserid')
            ->where(" (touserid = 3 AND totypeid = 1) AND(fromtypeid = 3) OR (fromuserid = 3  AND fromtypeid = 1) AND (totypeid = 3)  ")
            ->orderBy('datetime DESC')
            ->one();
    }

    public function getAllGirls()
    {
        return $messages = $this->find()
            ->select('fromuserid , touserid, text , datetime ')
            ->where(" ((touserid = 3 AND totypeid = 1) AND(fromtypeid = 3)) OR ((fromuserid = 3  AND fromtypeid = 1) AND (totypeid = 3))  ")
            ->groupBy(['fromtypeid','touserid'])
            ->orderBy('datetime DESC')
            ->all();
    }

    public function getTextMessageG($userid)
    {
        return $message = $this->find()
            ->select("text,datetime")
            ->where("(fromuserid = " . $userid . " AND fromtypeid = 3 ) AND (touserid = 3 AND totypeid = 1) OR (fromuserid = 3  AND fromtypeid = 1) AND ( touserid =" . $userid . " AND totypeid = 3) ")
            ->orderBy('datetime DESC')
            ->one();
    }

    public function getMessagesToAdminGirl($userid)
    {
        return $messages = $this->find()
            ->where("(fromuserid = " . $userid . " AND fromtypeid = 3 ) AND (touserid = 3 AND totypeid = 1) OR (fromuserid = 3  AND fromtypeid = 1) AND ( touserid =" . $userid . " AND totypeid = 3) ")
            ->orderBy('datetime DESC')
            ->all();
    }

    public function getCountMessageToAdmin($usertypeid)
    {
        $count = $message = $this->find()
            ->select(['COUNT(*) AS count'])
            ->where('touserid = 3 AND totypeid = 1 AND fromtypeid = ' . $usertypeid . ' AND is_readed = 0')
            ->asArray()
            ->all();

        return $count[0]['count'];
    }

    public function getAllMessageToGirl($userid)
    {
        return $message = $this->find()
            ->where("(fromuserid = " . $userid . " AND fromtypeid = 3 ) OR ( touserid =" . $userid . " AND totypeid = 3)")
            ->all();
    }

    public function readMessage($toid, $fromid)
    {
        return $this->updateAll(['is_readed' => 1], ['touserid' => $toid, 'fromuserid' => $fromid]);
    }

    public function getCountOfUnreadedMessagesFromAdmin($userid)
    {
        $count = $this->find()
            ->select(['COUNT(*) AS count'])
            ->where(" touserid =  " . $userid . " AND totypeid = 2 AND fromuserid = 3 AND fromtypeid = 1 AND is_readed = 0")
            ->asArray()
            ->all();

        return $count[0]['count'];
    }
}
