<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reply".
 *
 * @property integer $id
 * @property integer $girl_id
 * @property integer $men_id
 * @property string $unique_column
 */
class Reply extends \yii\db\ActiveRecord
{
    public $text;
    public $girl_name;
    public $men_name;
    public $message_id;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['girl_id', 'men_id', 'unique_column'], 'required'],
            [['girl_id', 'men_id','answerMe'], 'integer'],
            [['unique_column'], 'string', 'max' => 50],
            [['unique_column'],'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'girl_id' => 'Girl ID',
            'men_id' => 'Men ID',
            'unique_column' => 'Unique Column',
            'answerMe' => 'Answer me',
        ];
    }

    public function getMen()
    {
        return $this->hasOne(Men::className(), ['topdatesid' => 'men_id']);
    }

    public function insertRecords($girl, $men, $uniq)
    {
        $this->girl_id = $girl;
        $this->men_id = $men;
        $this->unique_column = $uniq;
        $this->save();
    }

    public function getAnswerMe()
    {
        return $this->find()->where(['answerMe'=>1])->all();
    }

    public function getRefuse()
    {
        return $this->find()->where(['refuse'=>1])->all();
    }

    public function answerMe($toid, $fromid)
    {
        $this->updateAll(['answerMe' => 1], 'girl_id = "'.$toid.'" AND men_id = "'.$fromid.'"');
    }

    public function resuse($toid, $fromid)
    {
        $this->updateAll(['refuse' => 1], 'girl_id = "'.$toid.'" AND men_id = "'.$fromid.'"');
    }
}
