<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chats".
 *
 * @property integer $id
 * @property integer $girlid
 * @property integer $topdatesgirlid
 * @property integer $manid
 * @property integer $datetime
 * @property string $text
 * @property integer $statusid
 *
 * @property Statuses $status
 * @property Girls $girl
 */
class Chats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['girlid', 'topdatesgirlid', 'manid', 'datetime', 'text', 'statusid'], 'required'],
            [['girlid', 'topdatesgirlid', 'manid', 'datetime', 'statusid'], 'integer'],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'girlid' => Yii::t('app', 'Girlid'),
            'topdatesgirlid' => Yii::t('app', 'Topdatesgirlid'),
            'manid' => Yii::t('app', 'Manid'),
            'datetime' => Yii::t('app', 'Datetime'),
            'text' => Yii::t('app', 'Text'),
            'statusid' => Yii::t('app', 'Statusid'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'statusid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirl()
    {
        return $this->hasOne(Girls::className(), ['id' => 'girlid']);
    }
}
