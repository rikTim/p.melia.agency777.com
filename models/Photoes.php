<?php

namespace app\models;

use Yii;
use app\models\Girls;
use yii\debug\models\search\Base;
use yii\helpers\BaseVarDumper;

/**
 * This is the model class for table "photoes".
 *
 * @property integer $id
 * @property integer $fileid
 * @property integer $is_active
 * @property integer $girlid
 * @property integer $is_cover
 *
 * @property Girls $girl
 * @property Files $file
 */
class Photoes extends \yii\db\ActiveRecord
{
    public $filepath;
    public $filepathmini;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photoes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fileid', 'is_active', 'girlid', 'is_cover'], 'required'],
            [['fileid', 'is_active', 'girlid', 'is_cover', 'id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fileid' => Yii::t('app', 'Fileid'),
            'is_active' => Yii::t('app', 'Is Active'),
            'girlid' => Yii::t('app', 'Girlid'),
            'is_cover' => Yii::t('app', 'Is Cover'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirl()
    {
        return $this->hasOne(Girls::className(), ['id' => 'girlid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(Files::className(), ['id' => 'fileid']);
    }


    /**
     *
     */
    public function getAllPhotos($id)
    {
        return $this->find()->select(['fileid' => 'fileid'])->where(['girlid' => $id, 'is_cover' => 0])->asArray()->all();
    }

    /**
     *
     */
    public function getOnePhoto($id)
    {
        return $this->find()
            ->select(['fileid' => 'fileid'])
            ->where(['girlid' => $id, 'is_cover' => 1])
            ->asArray()
            ->one();
    }

    /**
     *
     */
    public function getPhotoById($id)
    {
        return $this->find()
            ->where('id = '.$id)
            ->one();
    }

    /**
     *
     */
    public function getPhotoByGirlId($girlid)
    {
        return $this->find()
            ->where('girlid = ' . $girlid . ' AND is_active = 1 AND is_cover = 1')
            ->one();
    }

    /**
     *
     */
    public function addCoverPhoto($fileId)
    {
        // get max girl id
        $girlsModel = new Girls();
        $girl = $girlsModel->getMaxGirlId();
        $maxGirlId = $girl['max'] + 1;

        $this->updateAll(['is_cover' => 0], 'girlid = '.$maxGirlId);

        $this->fileid    = $fileId;
        $this->girlid    = $maxGirlId;
        $this->is_active = 1;
        $this->is_cover  = 1;
        $this->save();
    }

    /**
     *
     */
    public function addPhoto($fileid, $girlId)
    {
        $this->fileid    = $fileid;
        $this->girlid    = $girlId;
        $this->is_active = 1;
        $this->is_cover  = 0;
        $this->save();
    }

    /**
     *
     */
    public function getAllPhotoByGirlId($girlid)
    {
        return $this->find()
            ->where('girlid = '.$girlid.' AND is_active = 1 AND is_cover = 0')
            ->all();
    }

    /**
     *
     */
    public function getAllPhotoByUpdate($girlid)
    {
        return $this->find()
            ->where('girlid = '.$girlid.' AND is_active = 1 ')
            ->all();
    }

    /**
     *
     */
    public function deletePhotoById($id)
    {
        $filesModel = new Files();

        $deletedPhoto = $this->getPhotoById($id);
        $deletedFileId = $deletedPhoto->fileid;

        $deletedFile = $filesModel->getFileById($deletedFileId);


        $pathMini = explode('/', $deletedFile->path);
        $pathMini = $pathMini[0].'/'.$pathMini[1].'/m_'.$pathMini[2];

        unlink('../../melia.agency777.com/web/' . $deletedFile->path);
        unlink('../../melia.agency777.com/web/' . $pathMini);

        $this->deleteAll(['id' => $id]);
        $filesModel->deleteFileById($deletedFileId);
    }


}
