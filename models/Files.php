<?php

namespace app\models;

use Yii;
use Imagine\Image\Point;
use yii\helpers\BaseFileHelper;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $path
 * @property integer $is_active
 *
 * @property Girls[] $girls
 * @property Girls[] $girls0
 * @property Mainbgimage[] $mainbgimages
 * @property Partners[] $partners
 * @property Photoes[] $photoes
 * @property Successstories[] $successstories
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path', 'is_active'], 'required'],
            [['is_active'], 'integer'],
            [['path'], 'string', 'max' => 120],
//            ['primaryImage', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'path' => Yii::t('app', 'Path'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirls()
    {
        return $this->hasMany(Girls::className(), ['passportimage' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirls0()
    {
        return $this->hasMany(Girls::className(), ['identificationvideo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainbgimages()
    {
        return $this->hasMany(Mainbgimage::className(), ['fileid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartners()
    {
        return $this->hasMany(Partners::className(), ['avatar' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotoes()
    {
        return $this->hasMany(Photoes::className(), ['fileid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuccessstories()
    {
        return $this->hasMany(Successstories::className(), ['fileid' => 'id']);
    }

    public function getOneFiles($id)
    {
        $files = $this->findOne(['id' => $id]);
        $filesList = [];
        $filesList[$files['id']] = $files['path'];
        return $filesList;
    }

    public function getLink($id)
    {
        $link = $this->find()->where('id = ' . $id)->one();
        return $link->path;
    }

    public function addFile($path, $type = 1, $isAvatar = false, $isPartner = false)
    {
        $this->path = $path;
        $this->is_active = 1;
        $this->type = $type;
        $this->save();
        if ($isAvatar == true) {
            if ($isPartner == true) {
                $partnerModel = new Partners();
                $partnerModel->updateAvatar($this->id);
            } else {
                $photoesModel = new Photoes();
                $photoesModel->addCoverPhoto($this->id);
            }
        }
    }

    // сохранение видео через d&d  uploader
    public function saveTmpVideo($file, $fileTmp)
    {
        $userId = Yii::$app->user->identity->id;
        $dir = '../../melia.agency777.com/web/media/tmpVideo/'.$userId;

        BaseFileHelper::createDirectory($dir);

        $fullPath = $dir . '/' . basename(md5($file));
        $fileExt = explode(".", $file);

        copy($fileTmp, $fullPath.'.'.strtolower(end($fileExt)));
        flush();

        return true;
    }

    public function saveTmpVideoToGirl($newGirlId, $tmpVideoDir)
    {
        $newDir = '../../melia.agency777.com/web/media/'.$newGirlId;

        $tmpFiles = BaseFileHelper::findFiles($tmpVideoDir);
        foreach ($tmpFiles as $key => $file) {
            $nFile = basename($file);
            rename($file, $newDir.'/'.$nFile);

            $this->saveGirlVideo('media/'.$newGirlId.'/'.$nFile, $newGirlId);

            flush();
        }
    }

    // сохранение файла через d&d  uploader и сразу делаем crop
    public static function saveTmpFiles($file, $fileTmp)
    {
        $userId = Yii::$app->user->identity->id;
        $ext = ['jpg', 'png', 'jpeg', 'gif'];
        $dir = '../../melia.agency777.com/web/media/tmp/'.$userId;

        BaseFileHelper::createDirectory($dir);

        $file = explode(".", $file);
        $fileExt = strtolower(end($file));
        $fileName = md5($file[0]) . '.' . $fileExt;
        $fullPath = $dir . '/' . $fileName;

        if (in_array(strtolower(end($file)), $ext)) {
            copy($fileTmp, $fullPath);
            Yii::$app->DLL->img_resize($fullPath, 160, $dir . '/m_' . $fileName);
            flush();

            return true;
        } else {
            return false;
        }
    }

    public function saveTmpFilesToGirl($newGirlId, $tmpDir)
    {
        $newDir = '../../melia.agency777.com/web/media/'.$newGirlId;
        $tmpFiles = BaseFileHelper::findFiles($tmpDir);
        foreach ($tmpFiles as $key => $file) {
            $nFile = basename($file);
            rename($file, $newDir.'/'.$nFile);

            if (!strpos($file, 'm_')) {
                $this->saveGirlAlbumPhotos('media/'.$newGirlId.'/'.$nFile, $newGirlId);
            }

            flush();
        }
    }


    public function saveGirlAlbumPhotos($path, $girlId)
    {
        // КОСТЫЛЬ
        $filesModel = new Files();
        $filesModel->path = $path;
        $filesModel->is_active = 1;
        $filesModel->type = 1;
        $filesModel->save();

        $photosModel = new Photoes();
        $photosModel->addPhoto($filesModel->id, $girlId);
    }

    public function saveGirlVideo($path, $girlId)
    {
        // КОСТЫЛЬ
        $filesModel = new Files();
        $filesModel->path = $path;
        $filesModel->is_active = 1;
        $filesModel->type = 2;
        $filesModel->save();

        $girlModel = new Girls();
        $girlModel->addGirlVideo($filesModel->id, $girlId);
    }

    public function getFileById($id)
    {
        return $this->find()
            ->where('id = ' . $id)
            ->one();
    }

    public function addBgimage($path)
    {
        $this->path = $path;
        $this->is_active = 1;
        $this->type = 1;
        $this->save();
    }

    public function deleteFileById($id)
    {
        return $this->deleteAll(['id' => $id]);
    }
}
