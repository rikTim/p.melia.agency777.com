<?php

namespace app\models;

use app\models\Statuses;
use Yii;
use \yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "girls".
 *
 * @property integer $id
 * @property integer $topdatesid
 * @property string $name
 * @property string $nameid
 * @property string $lastname
 * @property integer $birthdate
 * @property integer $passportnumber
 * @property integer $passportimage
 * @property integer $height
 * @property integer $weight
 * @property string $eyescolor
 * @property integer $alcohol
 * @property integer $materialstatusid
 * @property integer $smoking
 * @property integer $children
 * @property integer $identificationvideo
 * @property string $occupation
 * @property integer $religionid
 * @property integer $countryid
 * @property integer $cityid
 * @property string $livingaddress
 * @property string $postaddress
 * @property string $phone
 * @property string $email
 * @property integer $characterid
 * @property string $interests
 * @property string $mansdescr
 * @property string $firstletter
 * @property string $fblink
 * @property string $vklink
 * @property integer $partnerid
 * @property integer $statusid
 * @property integer $is_active
 * @property integer $is_aproved
 * @property integer $is_accepted
 * @property Chats[] $chats
 * @property Statuses $status
 * @property Files $passportimage0
 * @property Materialstatuses $materialstatus
 * @property Files $identificationvideo0
 * @property Religions $religion
 * @property Countries $country
 * @property Cities $city
 * @property Characters $character
 * @property Partners $partner
 * @property Girlslanguages[] $girlslanguages
 * @property Notifications[] $notifications
 * @property Photoes[] $photoes
 */
class Girls extends ActiveRecord
{
    public $languageid;
    public $languageid2;
    public $passportimg;
    public $avatarPath;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'girls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'nameid', 'lastname', 'birthdate', 'education', 'passportnumber',
                'height', 'weight', 'eyescolor',
                'haircolor', 'materialstatusid', 'occupationid',
                'religionid', 'country', 'city', 'cityid', 'livingaddress', 'postaddress', 'email', 'phone', 'characterid',
                'interests', 'hobbies', 'mansdescr'], 'required'],
            [['occupationid', 'topdatesid', 'height', 'alcohol', 'materialstatusid', 'smoking', 'children',
                'identificationvideo', 'religionid', 'characterid', 'partnerid', 'statusid', 'is_active', 'is_aproved',
                'is_accepted', 'registerdate'], 'integer'],
            ['email', 'email'],
            ['email', 'unique'],
            [['education', 'hobbies', 'country', 'city', 'mansdescr', 'firstletter'], 'string'],
            [['name', 'lastname', 'fblink', 'vklink'], 'string', 'max' => 120],
            [['eyescolor'], 'string', 'max' => 15],
            [['livingaddress', 'postaddress'], 'string', 'max' => 255],
            [['interests'], 'string', 'max' => 300],
            [['passportimg'], 'file', 'extensions' => 'png, jpg, jpeg'],
            [['weight'], 'number', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'topdatesid' => Yii::t('app', 'Topdatesid'),
            'name' => Yii::t('app', 'Name'),
            'nameid' => Yii::t('app', 'Name id'),
            'lastname' => Yii::t('app', 'Lastname'),
            'birthdate' => Yii::t('app', 'Birthdate'),
            'education' => Yii::t('app', 'Education'),
            'passportnumber' => Yii::t('app', 'Passportnumber'),
            'passportimage' => Yii::t('app', 'Passportimage'),
            'height' => Yii::t('app', 'Height'),
            'weight' => Yii::t('app', 'Weight'),
            'eyescolor' => Yii::t('app', 'Eyescolor'),
            'alcohol' => Yii::t('app', 'Alcohol'),
            'materialstatusid' => Yii::t('app', 'Materialstatusid'),
            'smoking' => Yii::t('app', 'Smoking'),
            'children' => Yii::t('app', 'Children'),
            'identificationvideo' => Yii::t('app', 'Identificationvideo'),
            'occupationid' => Yii::t('app', 'Occupationid'),
            'religionid' => Yii::t('app', 'Religionid'),
            'country' => Yii::t('app', 'Country'),
            'city' => Yii::t('app', 'City'),
            'cityid' => Yii::t('app', 'City id'),
            'livingaddress' => Yii::t('app', 'Livingaddress'),
            'postaddress' => Yii::t('app', 'Postaddress'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'characterid' => Yii::t('app', 'Characterid'),
            'interests' => Yii::t('app', 'Interests'),
            'hobbies' => Yii::t('app', 'Hobbies'),
            'mansdescr' => Yii::t('app', 'Mansdescr'),
            'firstletter' => Yii::t('app', 'Firstletter'),
            'fblink' => Yii::t('app', 'Fblink'),
            'vklink' => Yii::t('app', 'Vklink'),
            'partnerid' => Yii::t('app', 'Partnerid'),
            'statusid' => Yii::t('app', 'Statusid'),
            'is_active' => Yii::t('app', 'Is Active'),
            'is_aproved' => Yii::t('app', 'Is Aproved'),
            'is_accepted' => Yii::t('app', 'Is Accepted'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->registerdate = strtotime(date('d F Y'));
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chats::className(), ['girlid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'statusid']);
    }


    public function getName()
    {
        return $this->hasOne(Names::className(), ['id' => 'nameid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassportimage0()
    {
        return $this->hasOne(Files::className(), ['id' => 'passportimage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialstatus()
    {
        return $this->hasOne(Materialstatuses::className(), ['id' => 'materialstatusid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdentificationvideo()
    {
        return $this->hasOne(Files::className(), ['id' => 'identificationvideo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReligion()
    {
        return $this->hasOne(Religions::className(), ['id' => 'religionid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacter()
    {
        return $this->hasOne(Characters::className(), ['id' => 'characterid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partnerid']);

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirlslanguages()
    {
        return $this->hasMany(Girlslanguages::className(), ['girlid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notifications::className(), ['girlid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotoes()
    {
        return $this->hasMany(Photoes::className(), ['girlid' => 'id']);
    }

    public function getOccupationid()
    {
        return $this->hasMany(Occupations::ClassName(), ['id' => 'Occupationid']);
    }

    public function getUser($userid)
    {
        return $this->find()
            ->where("userid = :userid", [':userid' => $userid])
            ->one();
    }

    public function getGirlById($id)
    {
        return $this->find()
            ->where("id = :id", [':id' => $id])
            ->one();
    }

    public function getAllGirls()
    {
        return $this->find()->all();
    }

    public function getMaxGirlId()
    {
        $maxId = $this->find()
            ->select(['MAX(id) as max'])
            ->asArray()
            ->one();
        return $maxId;
    }

    public function getGirlsByPartnerId($partnerId)
    {
        $photosModel = new Photoes();
        $filesModel = new Files();

        $partnerGirls = $this->find()
            ->where('partnerId = :partnerId', [':partnerId' => $partnerId])
            ->orderBy(['id' => SORT_DESC, 'registerdate' => SORT_DESC])
            ->all();


        foreach ($partnerGirls as $girl) {
            $photo = $photosModel->getPhotoByGirlId($girl->userid);
            if (!empty($photo)) {
                $girl->avatarPath = $filesModel->getFileById($photo->fileid)->path;
            }
        }


        return $partnerGirls;
    }

    public function getStatistics($datebegin, $dateend, $partnerid, $statusid = false)
    {
        if ($statusid == false) {
            $statictic = $this->find()
                ->select(['registerdate, COUNT(*) AS count'])
                ->where("(registerdate >= :datebegin AND registerdate <= :dateend) AND partnerid = :partnerid", [':datebegin' => $datebegin, ':dateend' => $dateend, ':partnerid' => $partnerid])
                ->groupBy(" registerdate")
                ->orderBy('registerdate ASC')
                ->asArray()
                ->all();
        } else {
            $statictic = $this->find()
                ->select(['registerdate, COUNT(*) AS count'])
                ->where("(registerdate >= :datebegin AND registerdate <= :dateend) AND partnerid = :partnerid AND statusid = :statusid", [':datebegin' => $datebegin, ':dateend' => $dateend, ':partnerid' => $partnerid, ':statusid' => $statusid])
                ->groupBy("registerdate")
                ->orderBy('registerdate ASC')
                ->asArray()
                ->all();
        }
        return $statictic;
    }

    public function getAdminStatistics($datebegin, $dateend, $statusid = false)
    {
        if ($statusid == false) {
            $statictic = $this->find()
                ->select(['registerdate, COUNT(*) AS count'])
                ->where("registerdate >= :datebegin AND registerdate <= :dateend", [':datebegin' => $datebegin, ':dateend' => $dateend])
                ->groupBy(" registerdate")
                ->orderBy('registerdate ASC')
                ->asArray()
                ->all();
        } else {
            $statictic = $this->find()
                ->select(['registerdate, COUNT(*) AS count'])
                ->where("(registerdate >= :datebegin AND registerdate <= :dateend) AND statusid = :statusid", [':datebegin' => $datebegin, ':dateend' => $dateend, ':statusid' => $statusid])
                ->groupBy("registerdate")
                ->orderBy('registerdate ASC')
                ->asArray()
                ->all();
        }
        return $statictic;
    }

    public function getCountOfGirlsByPartnerId($partnerId)
    {
        $dateMonthBegin = strtotime(date('F Y'));
        $dateMonthEnd = strtotime(date('F Y', strtotime('+1 month')));

        $countOfRegisterGirls = $this->find()
            ->select(['COUNT(id) AS count'])
            ->where("partnerid = :partnerId AND statusid = 6 AND registerdate >= :dateMonthBegin AND registerdate <= :dateMonthEnd", [':partnerId' => $partnerId, ':dateMonthBegin' => $dateMonthBegin, ':dateMonthEnd' => $dateMonthEnd])
            ->asArray()
            ->one();

        return $countOfRegisterGirls['count'];
    }

    public function getStatusidTitle()
    {
        return $this->status->title;
    }

    public function getStatusesList()
    {

        return ArrayHelper::map(Girls::find()->asArray->all(), 'id', 'title');
    }


    public function getFullName()
    {
        $fullname =$this->partner->name . ' ' . $this->partner->lastname;
        return $fullname;
    }
    public function getPartnerName()
    {
        return $this->partner->name;
    }

    public function getPartnerLastname()
    {
        return $this->partner->lastname;
    }

    public function getNewGirl()
    {
        return $this->find()
            ->where(['statusid' => 5])
            ->asArray()
            ->all();
    }

    public function getApprovedGirl()
    {
        return $this->find()
            ->where(['statusid' => 6])
            ->asArray()
            ->all();
    }

    public function getRejectedGirl()
    {
        return $this->find()
            ->where(['statusid' => 7])
            ->asArray()
            ->all();
    }

    public function getLanguagesGirl($id)
    {
        $langGirlModel = new Girlslanguages();
        $langidall = $langGirlModel->find()
            ->where('girlid = :id', [':id' => $id])
            ->all();

        $langModel = new Languages();
        $langArr = [];
        foreach ($langidall as $langid) {
            $langArr[] = $langModel->find()
                ->where(['id' => $langid['langid']])
                ->one();
        }
        if (empty($langArr[1])) {
            $lang = $langArr[0]->title;
        } else {
            $lang = $langArr[0]->title . ' / ' . $langArr[1]->title;
        }
        return $lang;
    }

    public function getGirlLimit($limit, $offset)
    {
        return $this->find()
            ->limit($limit)
            ->offset($offset)
            ->all();
    }

    public function addTopdateIdToGirl($girlid, $topdatesid)
    {
        $this->updateAll(['topdatesid' => $topdatesid], 'id = ' . $girlid);
    }

    public function getGirlTopdates($topdatesid)
    {
        return $this->find()
            ->where(['topdatesid' => $topdatesid])
            ->one();
    }

    public function addGirlVideo($videoId, $girlId)
    {
        $this->updateAll(['identificationvideo' => $videoId], 'userid = ' . $girlId);
    }

    public function getAvatar($userid)
    {
        $photos = Photoes::find()->where(['girlid'=>$userid,'is_cover'=>1])->one();
        $avatarid = $photos->fileid;
        $file = Files::find()->where(['id'=>$avatarid])->one();
        $avatar = $file->path;
        if(empty($avatar))
        {
            $avatar = 'images/no-image.jpeg';
        }
        return $avatar;
    }
}
