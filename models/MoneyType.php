<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partnersgetmoneytype".
 *
 * @property integer $id
 * @property integer $userid
 * @property string $getmoneytype
 */
class MoneyType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partnersgetmoneytype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'getmoneytype'], 'required'],
            [['userid'], 'integer'],
            [['getmoneytype'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'getmoneytype' => 'Getmoneytype',
        ];
    }

    public function GetPartner($id)
    {
        return $this->find()
            ->where(['userid' => $id])
            ->one();
    }


    public function addPartnerMoneyType($userId, $getmoneytype)
    {
        $this->getmoneytype = $getmoneytype ;
        $this->userid = $userId;
        $this->save();

        return true;
    }


}
