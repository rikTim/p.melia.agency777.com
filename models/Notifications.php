<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $id
 * @property integer $girlid
 * @property integer $email
 * @property integer $phone
 * @property integer $vk
 * @property integer $fb
 *
 * @property Girls $girl
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['girlid', 'email', 'phone', 'vk', 'fb'], 'required'],
            [['girlid', 'email', 'phone', 'vk', 'fb'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'girlid' => Yii::t('app', 'Girlid'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'vk' => Yii::t('app', 'Vk'),
            'fb' => Yii::t('app', 'Fb'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirl()
    {
        return $this->hasOne(Girls::className(), ['id' => 'girlid']);
    }

    public function getGirlEmail($girl)
    {
        return $this->find()->where(['girlid'=>$girl])->asArray()->one();
    }
}
