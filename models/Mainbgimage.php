<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mainbgimage".
 *
 * @property integer $id
 * @property string $text
 * @property integer $fileid
 * @property integer $is_active
 *
 * @property Files $file
 */
class Mainbgimage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mainbgimage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text',  'is_active'], 'required'],
            [['fileid', 'is_active'], 'integer'],
            [['text_ru','text_en','text_es','text_cz'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Text'),
            'fileid' => Yii::t('app', 'Fileid'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(Files::className(), ['id' => 'fileid']);
    }

    public function  getHead($id)
    {
        return $this->findOne([
            'id'=>$id
        ]);
    }

    public function getHeaderText()
    {
        return $this->findOne(['id' => 14, 'is_active' => 1]);
    }
}
