<?php

namespace app\models;

use Yii;
use yii\helpers\BaseVarDumper;

/**
 * This is the model class for table "partners".
 *
 * @property integer $id
 * @property string $name
 * @property string $lastname
 * @property integer $countryid
 * @property integer $cityid
 * @property string $email
 * @property integer $phone
 * @property integer $avatar
 * @property integer $passportnumber
 * @property string $withdrawal
 * @property integer $is_active
 * @property integer $is_aproved
 * @property integer $is_accepted
 *
 * @property Girls[] $girls
 * @property Files $avatar0
 * @property Countries $country
 * @property Cities $city
 */
class Partners extends \yii\db\ActiveRecord
{
    public $balance;
    public $getMoney;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'lastname', 'country', 'city', 'email', 'phone', 'pay_data', 'pay_metod'], 'required'],
            [['avatar', 'statusid'], 'integer'],
            [['country', 'city', 'name', 'lastname', 'pay_metod'], 'string', 'max' => 120],
            ['passportnumber', 'string', 'max' => 20],
            ['email', 'email'],
            [['withdrawal', 'country', 'city', 'pay_data',], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'lastname' => Yii::t('app', 'Lastname'),
            'country' => Yii::t('app', 'Country'),
            'city' => Yii::t('app', 'City'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'avatar' => Yii::t('app', 'Avatar'),
            'statusid' => Yii::t('app', 'Statusid'),
            'passportnumber' => Yii::t('app', 'Passportnumber'),
            'withdrawal' => Yii::t('app', 'Withdrawal'),
        ];
    }

    public function beforeSave($insert)
    {
        $this->registerdate = strtotime(date('d F Y'));
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirls()
    {

        return $this->hasMany(Girls::className(), ['partnerid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar0()
    {
        return $this->hasOne(Files::className(), ['id' => 'avatar']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city']);
    }

    public function getBalances()
    {
        return $this->hasMany(Balances::className(), ['userid' => 'userid']);
    }

    public function getStatuses()
    {
        return $this->hasMany(Statuses::className(), ['id' => 'statusid']);
    }


    /**
     *
     */
    public function getPartnerById($id)
    {
        return $this->find()->where('id = :id', [':id' => $id])->one();
    }

    /**
     *
     */
    public function getUser($userid)
    {
        return $this->find()->where('userid = :userId', [':userId' => $userid])->one();
    }

    /**
     *
     */
    public function getAllPartners()
    {
        return $this->find()->all();
    }

    /**
     *
     */
    public function getStatusid()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'statusid']);
    }

    /**
     *
     */
    public function updateAvatar($fileid)
    {
        $userId = Yii::$app->user->id;
        $this->updateAll(['avatar' => $fileid], 'userid = ' . $userId);
    }

    /**
     *
     */
    public function getUserStatus($id)
    {
        return $this->find()->select(['statusid' => 'statusid'])->where(['userid' => $id])->asArray()->one();
    }

    /**
     *
     */
    public function getAdminStatistics($datebegin, $dateend, $statusid = false)
    {
        if ($statusid == false) {
            return $this->find()
                ->select(['registerdate, COUNT(*) AS count'])
                ->where('(registerdate >= :dateBegin AND registerdate <= :dateEnd)', [':dateBegin' => $datebegin, ':dateEnd' => $dateend])
                ->groupBy('registerdate')
                ->orderBy('registerdate ASC')
                ->asArray()
                ->all();
        } else {
            return $this->find()
                ->select(['registerdate, COUNT(*) AS count'])
                ->where('(registerdate >= :dateBegin AND registerdate <= :dateEnd) AND statusid = :statusId', [':dateBegin' => $datebegin, ':dateEnd' => $dateend, ':statusId' => $statusid])
                ->groupBy('registerdate')
                ->orderBy('registerdate ASC')
                ->asArray()
                ->all();
        }
    }

    /**
     *
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'statusid']);
    }

    /**
     *
     */
    public function getStatusName()
    {
        return $this->status->title;
    }

    /**
     *
     */
    public function getGirlNew($id)
    {
        $new = Girls::find()
            ->where(['statusid' => 5, 'partnerid' => $id])
            ->all();

        return count($new);
    }

    /**
     *
     */
    public function getGirlAproved($id)
    {
        $aproved = Girls::find()
            ->where(['statusid' => 6, 'partnerid' => $id])
            ->all();

        return count($aproved);
    }

    /**
     *
     */
    public function getGirlrejected($id)
    {
        $rejected = Girls::find()
            ->where(['statusid' => 7, 'partnerid' => $id])
            ->all();

        return count($rejected);
    }

    /**
     *
     */
    public function getGirlStatus($id)
    {
        $all = $this->getGirlNew($id) . '/' . $this->getGirlAproved($id) . '/' . $this->getGirlrejected($id);
        return $all;
    }

    /**
     *
     */
    public function getNewPartner()
    {
        return $this->find()
            ->where(['statusid' => 5])
            ->asArray()
            ->all();
    }

    /**
     *
     */
    public function getApprovedPartner()
    {
        return $this->find()
            ->where(['statusid' => 6])
            ->asArray()
            ->all();
    }

    /**
     *
     */
    public function getRejectedPartner()
    {
        return $this->find()
            ->where(['statusid' => 7])
            ->asArray()
            ->all();
    }

    /**
     *
     */
    public static function getPartnerName($id)
    {
        $partner = Partners::find()->where(['userid' => $id])->one();

        if (!empty($partner)) {
            return $partner->name . ' ' . $partner->lastname;
        } else {
            return '';
        }
    }

    /**
     *
     */
    public static function getIdPartner($userid)
    {
        $partner = Partners::find()->where(['userid' => $userid])->one();
        return $partner->id;
    }

    /**
     *
     */
    public function getBalancesForBill()
    {
        $partners = $this->getAllPartners();

        $dateMonthBegin = strtotime(date('F Y'));
        $dateMonthEnd = strtotime(date('F Y', strtotime('+1 month')));

        $partnersBill = [];

        foreach ($partners as $key => $partner) {
            $balance = 0;

            foreach ($partner->balances as $balances) {
                if ($balances->datetime < $dateMonthBegin || $balances->datetime > $dateMonthEnd) {
                    continue;
                } elseif ($balances->transaction < 0) {
                    $balance += $balances->transaction;
                }
            }

            if ($balance != 0) {
                $partnersBill[$key]['partner'] = $partner->name . ' ' . $partner->lastname;
                $partnersBill[$key]['country'] = $partner->country;
                $partnersBill[$key]['city'] = $partner->city;
                $partnersBill[$key]['phone'] = $partner->phone;
                $partnersBill[$key]['payment method'] = $partner->pay_metod;
                $partnersBill[$key]['other'] = $partner->pay_data;
                $partnersBill[$key]['total'] = abs($balance);
            }

        }

        return $partnersBill;
    }

    public function getOnePartner($id)
    {
        return $partner = $this->find()
            ->where(['id' => $id])
            ->one();
    }



}
