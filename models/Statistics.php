<?php

namespace app\models;

use Yii;
use yii\helpers\BaseVarDumper;

class Statistics extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'statistics';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'IP',
            'datetime' => 'Date'
        ];
    }

    /**
     *
     */
    public function addStat()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $dayBegin = strtotime(date('d F Y'));
        $dayEnd = strtotime(date('d F Y', strtotime('+1 day')));

        $res = $this->find()
            ->where('ip = :ip AND datetime >= :begin AND datetime <= :end', [':ip' => $ip, ':begin' => $dayBegin, ':end' => $dayEnd])
            ->one();

        if (empty($res)) {
            $this->ip = $ip;
            $this->datetime = time();
            $this->save();
        }



    }
}