<?php

namespace app\models;

use Yii;

class Names extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'names';
    }

    public function rules()
    {
        return [];
    }

    public function attributeLabels()
    {
        return [];
    }

    public function getAllNames()
    {
        $names = $this->find()->select(['id' => 'id', 'name' => 'name'])->where(['is_active' => 1])->asArray()->all();
        $namesList = [];
        foreach($names as $name) {
            $namesList[$name['id']] = $name['name'];
        }
        return $namesList;
    }
}
