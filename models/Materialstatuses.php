<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materialstatuses".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 *
 * @property Girls[] $girls
 */
class Materialstatuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'materialstatuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'is_active'], 'required'],
            [['is_active'], 'integer'],
            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirls()
    {
        return $this->hasMany(Girls::className(), ['materialstatusid' => 'id']);
    }

    public function getAllStatuses()
    {
        $statuses = $this->find()->select(['id' => 'id', 'title' => 'title'])->where(['is_active' => 1])->asArray()->all();
        $statusesList = [];
        foreach($statuses as $status) {
            $statusesList[$status['id']] = $status['title'];
        }
        return $statusesList;
    }

    public function getMaterialStatusById($id)
    {
        $mStatus = $this->find()
            ->where('id ='.$id)
            ->one();

        return $mStatus->title;
    }
}
