<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Girls;

/**
 * GirlsSeach represents the model behind the search form about `app\models\Girls`.
 */
class GirlsSeach extends Girls
{
    public $statustitle;
    public $languages;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userid', 'topdatesid', 'birthdate', 'passportnumber', 'passportimage', 'height', 'weight', 'alcohol', 'materialstatusid', 'smoking', 'children', 'identificationvideo', 'occupationid', 'religionid', 'characterid', 'statusid', 'is_active', 'is_aproved', 'is_accepted'], 'integer'],
            [['name', 'lastname', 'eyescolor', 'haircolor', 'education', 'country', 'city', 'livingaddress', 'postaddress', 'phone', 'email', 'interests', 'hobbies', 'mansdescr', 'firstletter', 'fblink', 'vklink','statustitle','languages', 'partnerid'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Girls::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);





        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'userid' => $this->userid,
            'topdatesid' => $this->topdatesid,
            'birthdate' => $this->birthdate,
            'passportnumber' => $this->passportnumber,
            'passportimage' => $this->passportimage,
            'height' => $this->height,
            'weight' => $this->weight,
            'alcohol' => $this->alcohol,
            'materialstatusid' => $this->materialstatusid,
            'smoking' => $this->smoking,
            'children' => $this->children,
            'identificationvideo' => $this->identificationvideo,
            'occupationid' => $this->occupationid,
            'religionid' => $this->religionid,
            'characterid' => $this->characterid,
            'partnerid' => $this->partnerid,
            'statusid' => $this->statusid,
            'is_active' => $this->is_active,
            'is_aproved' => $this->is_aproved,
            'is_accepted' => $this->is_accepted,

        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'eyescolor', $this->eyescolor])
            ->andFilterWhere(['like', 'haircolor', $this->haircolor])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'livingaddress', $this->livingaddress])
            ->andFilterWhere(['like', 'postaddress', $this->postaddress])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'interests', $this->interests])
            ->andFilterWhere(['like', 'hobbies', $this->hobbies])
            ->andFilterWhere(['like', 'mansdescr', $this->mansdescr])
            ->andFilterWhere(['like', 'firstletter', $this->firstletter])
            ->andFilterWhere(['like', 'fblink', $this->fblink])
            ->andFilterWhere(['like', 'vklink', $this->vklink]);


        return $dataProvider;
    }
}
