<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usertypes".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 *
 * @property Balances[] $balances
 * @property Logs[] $logs
 * @property Messages[] $messages
 * @property Messages[] $messages0
 */
class Usertypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usertypes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'is_active'], 'required'],
            [['is_active'], 'integer'],
            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalances()
    {
        return $this->hasMany(Balances::className(), ['usertypeid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Logs::className(), ['usertypeid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Messages::className(), ['usertypeid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages0()
    {
        return $this->hasMany(Messages::className(), ['fromtypeid' => 'id']);
    }
}
