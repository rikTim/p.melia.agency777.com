<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "characters".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 *
 * @property Girls[] $girls
 */
class Characters extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'characters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'is_active'], 'required'],
            [['is_active'], 'integer'],
            [['title'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirls()
    {
        return $this->hasMany(Girls::className(), ['characterid' => 'id']);
    }

    public function getAllCharacters()
    {
        $characters = $this->find()->select(array('id' => 'id', 'title' => 'title'))->where(['is_active' => 1])->asArray()->all();
        $charactersList = [];
        foreach ($characters as $character) {
            $charactersList[$character['id']] = $character['title'];
        }
        return $charactersList;
    }

    public function getCharacterById($id)
    {
        $character = $this->find()
            ->where('id = :id AND is_active = 1', [':id' => $id])
            ->one();

        return $character->title;
    }
}
