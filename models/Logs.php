<?php

namespace app\models;

use Yii;
use yii\helpers\BaseVarDumper;

/**
 * This is the model class for table "logs".
 *
 * @property integer $id
 * @property integer $userid
 * @property integer $usertypeid
 * @property integer $acceptedtime
 * @property integer $acceptedip
 *
 * @property Usertypes $usertype
 */
class Logs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'usertypeid', 'acceptedtime', 'acceptedip'], 'required'],
            [['userid', 'usertypeid', 'acceptedtime'], 'integer'],
            [['acceptedip'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userid' => Yii::t('app', 'Userid'),
            'usertypeid' => Yii::t('app', 'Usertypeid'),
            'acceptedtime' => Yii::t('app', 'Acceptedtime'),
            'acceptedip' => Yii::t('app', 'Acceptedip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsertype()
    {
        return $this->hasOne(Usertypes::className(), ['id' => 'usertypeid']);
    }
    public function getPartner()
    {
        return $this->hasOne(Partners::className(), ['userid' => 'userid']);

    }

    public function getLogs()
    {
        return $this->hasOne(Users::className(), ['id' => 'userid']);
    }

    public function getUserHostAddress()
    {
        if (!empty($_SERVER['HTTP_X_REAL_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        } elseif (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function addLog()
    {
        $userid = Yii::$app->user->identity->id;
        if (!$this->getLogsByUserId($userid)) {
            $this->userid = $userid;
            $this->usertypeid = 2;
            $this->acceptedtime = time();
            $this->acceptedip = $this->getUserHostAddress();
            $this->save();
        }
    }

    public static function getIP($userid)
    {
        $log = Logs::find()->where(['userid' => $userid])->one();
        if (!empty($log)) {
            $ip = $log->acceptedip;
        } else $ip = '';
        return $ip;
    }

    public static function getIpCountry($userid)
    {
        $log = Logs::find()->where(['userid' => $userid])->one();
        if (!empty($log)) {
            $ip = $log->acceptedip;
            $langArr = yii::$app->geolocation->getInfo($ip);
            $countryCode = $langArr['geoplugin_countryName'];

        } else $countryCode = '';

        return $countryCode;
    }

    public static function getCountry($ip)
    {
        $langArr = yii::$app->geolocation->getInfo($ip);

        return $langArr['geoplugin_countryName'];
    }

    public function getLogsByUserId($userid)
    {
        $res = $this->findOne(['userid' => $userid]);
        if (!empty($res)) {
            return true;
        } else {
            return false;
        }

    }
    public function getIdPartner($userid)
    {
        $partner = Partners::find()->where(['userid'=>$userid])->one();
        return $partner->id;
    }
}
