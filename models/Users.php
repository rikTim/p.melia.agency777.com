<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\base\ErrorException;
use yii\base\NotSupportedException;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $statusid
 * @property string auth_key
 * @property string password_reset_token
 * @property Statuses $status
 * @property integer $status_user
 *
 */
class Users extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */

    public $profile;
    public $rpassword;
    public $authKey;

    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usertypeid'], 'integer'],
            ['username', 'required'],
            ['username', 'unique', 'targetAttribute' => 'username'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetAttribute' => 'email'],
            ['email', 'string', 'max' => 255],
            [['password', 'rpassword'], 'required'],
            ['rpassword', 'compare', 'compareAttribute' => 'password']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'email' => Yii::t('app', 'Email'),
            'usertypeid' => Yii::t('app', 'Usertypeid'),
            'is_greeting' => Yii::t('app', 'Is greeting')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'usertypeid']);
    }


    /**
     * @param int|string $id
     * @return null|static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @param \nodge\eauth\ServiceBase $service
     * @return User
     * @throws ErrorException
     */
    public static function findByEAuth($service) {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName().'-'.$service->getId();
        $attributes = [
            'id' => $id,
            'username' => $service->getAttribute('name'),
            'authKey' => md5($id),
            'profile' => $service->getAttributes(),
        ];
        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-'.$id, $attributes);
        return new self($attributes);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Finds user by username
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Validates password
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function toUser($data, $save = true)
    {
        $this->username = $data['username'];
        $this->password = $data['password'];
        $this->rpassword = $data['password'];
        $this->email = $data['email'];
        $this->auth_key = Yii::$app->security->generateRandomString();
        $this->usertypeid = 2; // 2 - usertype партнера

        $this->uniqueUsername($this->attributes);
        if ($this->validate() == true) {
            if ($save == true) {
                if ($this->save()) {
                    $this->authUser($this->username, $this->password);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return $this->getErrors();
        }
    }

    public function addGirlUser($name, $lastname, $email)
    {
        $pass = Yii::$app->security->generateRandomString(8);
        $this->username = strtolower($name) . '_' . strtolower($lastname) . '_' . mt_rand (100 , 999);
        $this->password = md5($pass);
        $this->rpassword = md5($pass);
        $this->email = $email;
        $this->auth_key = Yii::$app->security->generateRandomString();
        $this->usertypeid = 3; // 3 - usertype девушки

        $this->uniqueUsername($this->attributes);
        if ($this->validate() == true) {
            if ($this->save()) {

                $text = 'Вы зарегестрировали новую девушку ' .$name . ' ' .$lastname . '<br /> ' .
                        'Для авторизации <a href="http://meliaagency.com/">на сайте</a>  воспользуйтесь логином ' .
                        $this->username . ' и паролем ' . $pass;
                Yii::$app->DLL->sendEmail($text, 'Users registrations', 'mixalleach@gmail.com');

                return $this->id;
            } else {
                return false;
            }
        } else {
            return $this->getErrors();
        }
    }

    public function uniqueUsername($attribute)
    {
        if (!$this->hasErrors()) {
            $usernameArr = $this->find()->where(['username' => $this->username])->asArray()->one();
            $username = $usernameArr['username'];
            if ($username == $this->username) {
                $this->addError($this->username, 'The user already exists with that Username ');
            }
        }
    }

    public function addSocialUser($data, $social)
    {
        $this->username = $data['name'];
        $this->usertypeid = 2; // 2 - id  usertype партнера
        if ($social == 'vkontakte') {
            $this->vkid = $data['id'];
        } else if ($social == 'facebook') {
            $this->fbid = $data['id'];
        }
        $this->save(false);
        $this->authUser($data['name'], '');
    }

    public function checkSocialUser($data, $social)
    {
        if ($social == 'vkontakte') {
            $user = $this->find()->where(['username' => $data['name'], 'vkid' => $data['id']])->all();
        } else if ($social == 'facebook') {
            $user = $this->find()->where(['username' => $data['name'], 'fbid' => $data['id']])->all();
        }
        if (!empty($user)) {
            $this->authUser($data['name'], '');
            return true;
        } else {
            return false;
        }
    }


    // авторизация пользователей
    public function authUser($username, $password)
    {
        $login = array(
            '_csrf' => 'LVhCaGJBZVZ7ahEuEg5dORoiJQs4EiQRFAoWHAEvCCRuN3UCGwwMNA==',
            'LoginForm' => array(
                'username' => $username,
                'password' => $password,
                'rememberMe' => 1,
            ),
            'login-button' => '',
        );
        $loginFormModel = new LoginForm();
        $loginFormModel->load($login);
        $loginFormModel->login(false);
        return true;
    }

    public function toLogin($post)
    {
        $this->username = $post['username'];
        $this->password = md5($post['password']);
        $this->rpassword = md5($post['password']);
        $this->email = $post['email'];
        $this->auth_key = Yii::$app->security->generateRandomString();
        $this->usertypeid = $post['status'];
        if ($this->validate() == true) {
            $this->save();
            return false;
        } else {
            return $this->getErrors();
        }
    }

    public function getMaxUserId()
    {
        $maxId = $this->find()
            ->select(['MAX(id) as max'])
            ->asArray()
            ->one();
        return $maxId;
    }

    public static function checkUser($username, $email)
    {
        $user = static::find()
            ->where('username = "'.$username.'" AND email = "'.$email.'" AND usertypeid = 2')
            ->asArray()
            ->all();

        if (!empty($user)) {
            $token = static::addPasswordToken($user[0]['id']);
            $get = "token=" .md5($token). "&username=" . $user[0]['username'] . "&email=" . $user[0]['email'];

            //@todo узнать на каком языке отправлять сообщение с ссылкой
            $text = 'Для востановления пароля перейдите по ';
            $text .= '<a href="http://meliapartner.com/web/remindpass?' . $get . '">ссылке</a>';

            Yii::$app->mail
                ->compose('remindpass', ['text' => $text])
                ->setFrom(['admin@meliapartner.com'=>'Meliapartner'])
                ->setTo($user[0]['email'])
                ->setSubject('Password reminder')
                ->send();

            return 'Email was send to '.$email;
        } else {
            return 'Incorrect username or password.';
        }
    }

    public static function addPasswordToken($userid)
    {
        $token = Yii::$app->security->generateRandomString();
        static::updateAll(['password_reset_token' => $token], 'id = '.$userid);
        return $token;
    }

    public function changePass($data, $username, $email, $token)
    {
        $user = $this->find()
            ->where('username = "'.$username.'" AND email =  "'.$email.'"')
            ->asArray()
            ->all();

        if(md5($user[0]['password_reset_token']) == $token) {
            $this->updateAll(['password' => md5($data['password']), 'password_reset_token' => ''], 'id = '.$user[0]['id']);
            return 1;
        } else {
            return 0;
        }
    }

    public function getUserByUserId($id)
    {
        return $this->find()
            ->where('id = :id', [':id' => $id])
            ->one();
    }

    public function greetedByUserId($id)
    {
        $this->updateAll(['is_greeting' => 1], 'id = '.$id);
    }


    public function checkEmail($email)
    {
        $res = $this->find()
            ->where('email = :email', [':email' => $email])
            ->one();


        return $res;
    }

    public function checkUsername($username)
    {
        $res = $this->find()
            ->where('username = :username', [':username' => $username])
            ->one();


        return $res;
    }

}
