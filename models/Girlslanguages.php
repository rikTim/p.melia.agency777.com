<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "girlslanguages".
 *
 * @property integer $id
 * @property integer $girlid
 * @property integer $langid
 *
 * @property Languages $lang
 * @property Girls $girl
 */
class Girlslanguages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'girlslanguages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['girlid', 'langid'], 'required'],
            [['girlid', 'langid'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'girlid' => Yii::t('app', 'Girlid'),
            'langid' => Yii::t('app', 'Langid'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'langid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGirl()
    {
        return $this->hasOne(Girls::className(), ['id' => 'girlid']);
    }

    public function getLanguageByGirlId($girlId)
    {
        $lang = $this->find()
            ->where('girlid ='.$girlId)
            ->all();

        $langModel = new Languages();
        if (empty($lang)) {
            return $langModel->getLanguageById(1);
        } else {
            return $langModel->getLanguageById($lang[0]->langid, $lang[1]->langid);
        }
    }


    public function addGirlLanguage($girlid, $langid, $langid2)
    {
        $girlLang = $this->find()
            ->where('girlid ='. $girlid)
            ->all();

        if (!empty($girlLang)) {
            $girlLang[0]->langid = $langid;
            $girlLang[0]->update();
            if (!empty($girlLang[1])) {
                $girlLang[1]->langid = $langid2;
                $girlLang[1]->update();
            } else {
                $this->setAttributes(['girlid' => $girlid, 'langid' => $langid2]);
                $this->save();
            }
        } else {
            $this->setAttributes(['girlid' => $girlid, 'langid' => $langid]);
            $this->save();

            //  по другому не получалось. сохранялась только одна запись
            $model = new Girlslanguages();
            $model->setAttributes(['girlid' => $girlid, 'langid' => $langid2]);
            $model->save();
        }
    }
}
