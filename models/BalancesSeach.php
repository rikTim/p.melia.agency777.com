<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Balances;

/**
 * BalancesSeach represents the model behind the search form about `app\models\Balances`.
 */
class BalancesSeach extends Balances
{
    public $date;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userid', 'usertypeid', 'transaction'], 'integer'],
            [['datetime'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Balances::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if(!empty($this->datetime)) {
            // date to search
            $date = \DateTime::createFromFormat('d/m/Y', $this->datetime);
            $date->setTime(0, 0, 0);

// set lowest date value
            $unixDateStart = $date->getTimeStamp();

// add 1 day and subtract 1 second
            $date->add(new \DateInterval('P1D'));
            $date->sub(new \DateInterval('PT1S'));

// set highest date value
            $unixDateEnd = $date->getTimeStamp();

            $query->andFilterWhere(
                ['between', 'datetime', $unixDateStart, $unixDateEnd]);
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'userid' => $this->userid,
            'usertypeid' => $this->usertypeid,
            'transaction' => $this->transaction,

        ]);

        return $dataProvider;
    }

    public function getDatatime($datetime)
    {
        return Yii::$app->formatter->asDatetime($datetime, "php:d.m.Y ");
    }
}
