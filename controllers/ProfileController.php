<?php

namespace app\controllers;

use app\models\Logs;
use app\models\MoneyType;
use app\models\Partners;
use app\models\Statuses;
use app\models\UploadForm;
use app\models\Files;
use Yii;
use yii\helpers\BaseVarDumper;
use yii\web\Controller;
use yii\web\UploadedFile;

class ProfileController extends Controller
{
    /**
     * index profile action
     */
    public function actionIndex()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        $userId = Yii::$app->user->identity->id;

        if (!empty($userType) && $userType == 2) {


            $partnersModel = new Partners();
            $filesModel = new Files();

            $partner = $partnersModel->getUser($userId);
            if (!empty($partner)) {
                $partner->statusid = $partner->statuses[0];

                return $this->render('index', [
                    'model' => $partner,
                    'file' => $filesModel->getLink($partner->avatar)
                ]);
            } else {
                return $this->redirect('edit');
            }
        } else {
            return $this->redirect(['site/out']);
        }
    }


    /**
     * edit profile action
     */
    public function actionEdit()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        $userId = Yii::$app->user->identity->id;

        if (!empty($userType) && $userType == 2) {

            $userStatus = 6;
            $partnersModel = new Partners();
            $uploadFormModel = new UploadForm();
            $filesModel = new Files();

            $partner = $partnersModel->getUser($userId);

            // approved partners cant't edit profile
//            if ($partner->statusid == 6) {
//                $this->redirect(['index']);
//            }

            $data = $_POST['Partners'];
            if (!empty($data)) {


                // update partner info
                if (!empty($partner)) {
                    $partner->attributes = $data;
                    $partnersModel->setAttribute('userid', $userId);
                    $partnersModel->setAttribute('statusid', $userStatus);
                    $partner->save();
                } else {
                    $partnersModel->attributes = $data;
                    $partnersModel->setAttribute('userid', $userId);
                    $partnersModel->setAttribute('statusid', $userStatus);
                    $partnersModel->save();
                }
                return $this->redirect(['index']);
            }

            if (!empty($partner)) {
                $statusesModel = new Statuses();
                $partner->statusid = $statusesModel->getStatusById($partner->statusid);
                $file = $filesModel->getLink($partner->avatar);
            } else {
                $partnersModel->setAttribute('email', Yii::$app->user->identity->email);
            }

            return $this->render('edit', [
                'model' => (empty($partner)) ? $partnersModel : $partner,
                'file' => $file,
                'fileModel' => $uploadFormModel,
                'payMoney' => Yii::$app->params['payMoney'],
            ]);
        } else {
            return $this->redirect(['site/out']);
        }
    }


    /**
     * Avatar upload action
     */
    public function actionUploadavatar()
    {
        $model = new UploadForm();
        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->uploadavatar(true)) {
                return $this->redirect(['profile/edit']);
            }
        }
        return $this->render('upload', ['model' => $model]);
    }
}
