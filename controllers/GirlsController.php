<?php

namespace app\controllers;

use app\models\Balances;
use app\models\Files;
use app\models\Girlslanguages;
use app\models\Logs;
use app\models\Messages;
use app\models\Notifications;
use app\models\Occupations;
use app\models\Partners;
use app\models\Photoes;
use app\models\Statuses;
use app\models\Languages;
use app\models\Materialstatuses;
use app\models\Religions;
use app\models\Characters;
use app\models\UploadForm;
use app\models\Partnersgetmoneytype;
use app\models\Girls;
use app\models\GirlsSeach;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseVarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\User;

/**
 * GirlsController implements the CRUD actions for Girls model.
 */
class GirlsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->layout = 'admin';
            return true;
        }
        return false;
    }

    /**
     * Lists all Girls models.
     * @return mixed
     */
    public function actionIndex()
    {
        $userTypeId = Yii::$app->user->identity->usertypeid;
        if (!empty($userTypeId) && $userTypeId == 1) {
            $girlsModel = new Girls();
            $searchModel = new GirlsSeach();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $statusModel = new Statuses();
            $users = $statusModel->getAllUser();
            $allGirl = count($girlsModel->getAllGirls());
            $newGirl = count($girlsModel->getNewGirl());
            $approvedGirl = count($girlsModel->getApprovedGirl());
            $rejectedGirl = count($girlsModel->getRejectedGirl());


            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'allGirl' => $allGirl,
                'newGirl' => $newGirl,
                'approvedGirl' => $approvedGirl,
                'rejectedGirl' => $rejectedGirl,
            ]);
        } else {
            return $this->goHome();
        }
    }

    /**
     * Displays a single Girls model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $userTypeId = Yii::$app->user->identity->usertypeid;
        if (!empty($userTypeId) && $userTypeId == 1) {
            $model = $this->findModel($id);
            $image = $model->passportimage;
            $videoid = $model->identificationvideo;
            $userUpdate = $model->id;
            $userid = $model->userid;

            $userModel = new Users();
            $filesModel = new Files();
            $languagesModel = new Languages();
            $characterModel = new Characters();
            $materialStatus = new Materialstatuses();
            $statusesUser = new Statuses();
            $photosModel = new Photoes();
            $occupationsModel = new Occupations();
            $languages = $model->getLanguagesGirl($id);

//            $languages = $languagesModel->getAllLanguages();
            $user = $userModel->find()->where(['id'=>$userid])->one();
            $username = $user->username;
            $characters = $characterModel->getAllCharacters();
            $users = $statusesUser->getAllUser();
            $files = $filesModel->getOneFiles($image);
            $massArr = $photosModel->getOnePhoto($userUpdate);
            $mainphoto = $massArr['fileid'];
            $allphoto = $photosModel->getAllPhotos($userid);
            $avatarArr = $filesModel->getOneFiles($mainphoto);
            $videopath = $filesModel->getOneFiles($videoid);
            $occupation = $occupationsModel->getOneOccupations($model->occupationid);
            $file = $files[$image];
            $videolink = $videopath[$videoid];

            $avatar = current($avatarArr);
            $avatarKey = key($avatarArr);

            $photosArr = [];
            foreach ($allphoto as $keys => $values) {
                foreach ($values as $key => $val) {
                    $photosArr[$keys] = $val;
                }
            }


            $photos = $photosModel->getAllPhotoByGirlId($userid);
            foreach ($photos as $photo) {
                $photo->filepath = $filesModel->getFileById($photo->fileid)->path;
                $pathMini = explode('/', $filesModel->getFileById($photo->fileid)->path);

                $photo->filepathmini = $pathMini[0].'/'.$pathMini[1].'/m_'.$pathMini[2];
            }


            $eyesColor = [
                'Blue' => 'Blue',
                'Brown' => 'Brown',
                'Green' => 'Green',
                'Hazel' => 'Hazel',

            ];
            $hairColor = [
                'Black' => 'Black',
                'Blond' => 'Blond',
                'Brown' => 'Brown',
                'Fair' => 'Fair',
                'Chestnut' => 'Chestnut',
                'Red' => 'Red',
            ];
            $education = [
                'University degree' => 'University degree',
                'University (unfinished)' => 'University (unfinished)',
                'Medical degree' => 'Medical degree',
                'Student' => 'Student',
                'College degree' => 'College degree',
                'High school' => 'High school'
            ];
            $filelink = "/melia.agency777.com/web/" . $file;




            return $this->render('view', [
                'model' => $model,
                'languages' => $languages,
                'characters' => $characters,
                'eyescolor' => $eyesColor,
                'haircolor' => $hairColor,
                'file' => $file,
                'users' => $users,
                'filelink' => $filelink,
                'photos' => $photos,
                'avatar' => $avatar,
                'avatarKey' => $avatarKey,
                'occupation' => $occupation,
                'education' => $education,
                'videolink' => $videolink,
                'username'=>$username,
            ]);
        } else {
            return $this->goHome();
        }
    }

    /**
     * Creates a new Girls model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $userTypeId = Yii::$app->user->identity->usertypeid;
        if (!empty($userTypeId) && $userTypeId == 1) {
            $model = new Girls();
            $filesModel = new UploadForm();
            if (!empty($_POST['Girls'])) {
                $data = $_POST['Girls'];

                $filesModel->imageFile = UploadedFile::getInstance($filesModel, 'imageFile');
                $filesModel->identVideo = UploadedFile::getInstance($filesModel, 'identVideo');

                $model->attributes = $data;
                $model->passportimage = $filesModel->upload($filesModel->imageFile, 1);
                $model->identificationvideo = $filesModel->upload($filesModel->identVideo, 2);

                if ($model->validate()) {
                    $model->save();
                } else {
                    print_r($model->getErrors());
                    die();
                }
            }

            $religionsModel = new Religions();
            $languagesModel = new Languages();
            $characterModel = new Characters();
            $materialStatus = new Materialstatuses();
            $statusesUser = new Statuses();


            $religions = $religionsModel->getAllReligions();
            $languages = $languagesModel->getAllLanguages();
            $characters = $characterModel->getAllCharacters();
            $statuses = $materialStatus->getAllStatuses();
            $users = $statusesUser->getAllUser();


            $eyesColor = [
                'Blue' => 'Blue',
                'Brown' => 'Brown',
                'Green' => 'Green',
                'Hazel' => 'Hazel',

            ];
            $hairColor = [
                'Black' => 'Black',
                'Blond' => 'Blond',
                'Brown' => 'Brown',
                'Fair' => 'Fair',
                'Chestnut' => 'Chestnut',
                'Red' => 'Red',
            ];
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'religions' => $religions,
                    'languages' => $languages,
                    'characters' => $characters,
                    'eyescolor' => $eyesColor,
                    'haircolor' => $hairColor,
                    'statuses' => $statuses,
                    'users' => $users,
                    'fileModel' => $filesModel,
                ]);
            }
        } else {
            return $this->goHome();
        }
    }

    /**
     * Updates an existing Girls model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $userTypeId = Yii::$app->user->identity->usertypeid;
        if (!empty($userTypeId) && $userTypeId == 1) {
            $model = $this->findModel($id);
            $image = $model->passportimage;
            $userUpdate = $model->id;
            $videoid = $model->identificationvideo;
            $userid = $model->userid;

            $religionsModel = new Religions();
            $languagesModel = new Languages();
            $characterModel = new Characters();
            $materialStatus = new Materialstatuses();
            $statusesUser = new Statuses();
            $modelMessages = new Messages();
            $filesModel = new Files();
            $photosModel = new Photoes();
            $occupationsModel = new Occupations();
            $partnerModel = new Partners();
            $balancesModel = new Balances();
            $partnersGetMoneyTypeModel = new Partnersgetmoneytype();


            $religions = $religionsModel->getAllReligions();
            $languages = $languagesModel->getAllLanguages();
            $characters = $characterModel->getAllCharacters();
            $statuses = $materialStatus->getAllStatuses();
            $users = $statusesUser->getAllUser();
            $files = $filesModel->getOneFiles($image);
            $massArr = $photosModel->getOnePhoto($userid);
            $mainphoto = $massArr['fileid'];
            $allphoto = $photosModel->getAllPhotos($userid);
            $avatarArr = $filesModel->getOneFiles($mainphoto);
            $videopath = $filesModel->getOneFiles($videoid);
            $occupation = $occupationsModel->getOccupationsAll();
            $file = $files[$image];
            $videolink = $videopath[$videoid];


            $avatar = current($avatarArr);
            $avatarKey = key($avatarArr);

            $photosArr = [];
            foreach ($allphoto as $keys => $values) {
                foreach ($values as $key => $val) {
                    $photosArr[$keys] = $val;
                }
            }
            $linkArr = [];
            foreach ($photosArr as $photo) {
                $linkArr[$photo] = $filesModel->getLink($photo);
            }


            $eyesColor = [
                'Blue' => 'Blue',
                'Brown' => 'Brown',
                'Green' => 'Green',
                'Hazel' => 'Hazel',

            ];
            $hairColor = [
                'Black' => 'Black',
                'Blond' => 'Blond',
                'Brown' => 'Brown',
                'Fair' => 'Fair',
                'Chestnut' => 'Chestnut',
                'Red' => 'Red',
            ];
            $education = [
                'University degree' => 'University degree',
                'University (unfinished)' => 'University (unfinished)',
                'Medical degree' => 'Medical degree',
                'Student' => 'Student',
                'College degree' => 'College degree',
                'High school' => 'High school'
            ];


            if ($model->load(Yii::$app->request->post())) {
                $hair = $model->haircolor;
                $eyes = $model->eyescolor;
                $model->haircolor = $hairColor[$hair];
                $model->eyescolor = $eyesColor[$eyes];
                $post = $_POST['Messages'];
                $text = $post['text'];
                $partnerId = $model->partnerid;



                if (!empty($text) ) {
                    $userArr = $_POST['Girls'];
                    $touser = $userArr['userid'];
                    $user = Yii::$app->user->id;
                    $usertypeid = Yii::$app->user->identity->usertypeid;
                    $model->partnerid;
                    if (!empty($partnerId)) {
                        $partnerModel = new Partners();
                        $partnerArr = $partnerModel->getOnePartner($partnerId);
                        $partnerUserid = $partnerArr->userid;

                        $modelMessages->messagePartner($user, $text, $usertypeid, $partnerUserid, $touser);
                    } else {
                        $modelMessages->messagesUser($user, $text, $usertypeid, $touser);
                    }
                }

                if (!empty($partnerId)) {
                    // get partner money type
                    $partner = $partnerModel->getPartnerById($partnerId);
                    $getMoneyType = $partnersGetMoneyTypeModel->getGetMoneyTypeIdByUserId($partner->userid);

                    // если оплата по факту, зачисляем узазанную сумму из значения что указаны в таблице money
                    if ($getMoneyType == 2 && $model->statusid == 6) {
                        $balancesModel->addMoneyPerGirl($partner->userid);
                    }
                }

                $model->save();
                return $this->redirect(['index']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'religions' => $religions,
                    'languages' => $languages,
                    'characters' => $characters,
                    'eyescolor' => $eyesColor,
                    'haircolor' => $hairColor,
                    'statuses' => $statuses,
                    'users' => $users,
                    'messages' => $modelMessages,
                    'file' => $file,
                    'paths' => $linkArr,
                    'avatar' => $avatar,
                    'avatarKey' => $avatarKey,
                    'occupation' => $occupation,
                    'education' => $education,
                    'videolink' => $videolink,


                ]);
            }
        } else {
            return $this->goHome();
        }
    }

    /**
     * Deletes an existing Girls model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $userTypeId = Yii::$app->user->identity->usertypeid;
        if (!empty($userTypeId) && $userTypeId == 1) {
            $girl = $this->findModel($id);
//
//            $photosModel        = new Photoes();
//            $messagesModel      = new Messages();
//            $filesModel         = new Files();
//            $userModel          = new Users();
//            $notificationModel  = new Notifications();
//            $logModel           = new Logs();
//            $langModel          = new Girlslanguages();
//            $allphotos    = $photosModel->find()->where(['girlid'=>$id])->all();
//            $allmessage   = $messagesModel->getAllMessageToGirl($id);
//            $notification = $notificationModel->find()->where(['girlid'=>$id])->one();
//            $log          = $logModel->find()->where(['userid'=>$girl->userid])->one();
//            $lang         = $langModel->find()->where(['girlid'=>$id])->one();
//            $pasportimage = $filesModel->find()->where(['id'=>$girl->passportimage])->one();
//            $video        = $filesModel->find()->where(['id'=>$girl->identificationvideo])->one();
//            $user         = $userModel->find()->where(['id'=>$girl->userid])->one();
//
//            if(!empty($allphotos)) {
//                foreach ($allphotos as $photo) {
//                    $image = $filesModel->find()->where(['id' => $photo->fileid])->one();
////                    unlink('../../melia.agency777.com/web/' . $image->path);
//                    $image->delete();
//                    $photo->delete();
//                }
//            }
//            if(!empty($allmessage)) {
//                foreach ($allmessage as $message) {
//                    $message->delete();
//                }
//            }
//            if(!empty($notification)){
//                $notification->delete();
//            }
//            if(!empty($log)){
//                $log->delete();
//            }
//            if(!empty($lang))
//            {
//                $lang->delete();
//            }
//            if(!empty($pasportimage))
//            {
//                unlink('../../melia.agency777.com/web/' . $pasportimage->path);
//            }
//            if(!empty($video)){
//                $video->delete();
//            }
//            $user->delete();

            $girl->statusid = 8;
            $girl->save(false);
            return $this->redirect(['index']);
        } else {
            return $this->goHome();
        }
    }

    /**
     * Finds the Girls model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Girls the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $userTypeId = Yii::$app->user->identity->usertypeid;
        if (!empty($userTypeId) && $userTypeId == 1) {
            if (($model = Girls::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        } else {
            return $this->goHome();
        }
    }


    public function actionUpdatephotos()
    {
        $userTypeId = Yii::$app->user->identity->usertypeid;
        if (!empty($userTypeId) && $userTypeId == 1) {
            $photosModel = new Photoes();
            $new = $photosModel->findOne(['fileid' => $_POST['photoId']]);
            $id = $new->girlid;

            $all = $photosModel->getAllPhotoByUpdate($id);
            foreach ($all as $one) {
                if ($one->fileid == $_POST['photoId']) {
                    $one->is_cover = 1;
                    $one->save();
                } else {
                    $one->is_cover = 0;
                    $one->save();
                }
            }
            return true;
        } else {
            return $this->goHome();
        }
    }

    public function actionDelphotos()
    {
        $userTypeId = Yii::$app->user->identity->usertypeid;
        if (!empty($userTypeId) && $userTypeId == 1) {
            $photosModel = new Photoes();

            $photoId = $_POST['photoId'];

            $delete = $photosModel->findOne(['fileid' => $photoId]);
            $fileModel = new Files();
            $file = $fileModel->getOneFiles($photoId);

            unlink('../../melia.agency777.com/web/' . $file[$photoId]);
            $delete->delete();
        } else {
            return $this->goHome();
        }
    }

    public function actionRotatePhoto()
    {
        $userTypeId = Yii::$app->user->identity->usertypeid;
        if (!empty($userTypeId) && $userTypeId == 1) {

            $fileModel = new Files();
            $file = $fileModel->getOneFiles($_POST['photoId']);

            $filename = '../../melia.agency777.com/web/' . $file[$_POST['photoId']];

            echo $filename;

            $degrees = 90;

            $source = imagecreatefromjpeg($filename);
            $rotate = imagerotate($source, $degrees, 0);


//            imagejpeg($rotate);


            imagedestroy($source);
            imagedestroy($rotate);


            die();


        } else {
            return $this->goHome();
        }
    }



    public function actionPush()
    {
        $girlId = Yii::$app->request->post('girlId');
        $pusher = new Yii::$app->profilePusher();
        $pusher->girlId = $girlId;
        $pusher->push();
    }
}