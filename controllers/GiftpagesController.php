<?php

namespace app\controllers;

use app\models\Giftpages;
use app\models\GiftpagesSeach;
use app\models\Messages;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\Girls;
use app\models\Partners;

/**
 * GiftpagesController implements the CRUD actions for Giftpages model.
 */
class GiftpagesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->layout = 'admin';
            return true;
        }
        return false;
    }

    /**
     * Lists all Giftpages models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid = 1) {
                $searchModel = new GiftpagesSeach();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            } else {
                return $this->goHome();
            }
        } else $this->goHome();
    }

    /**
     * Displays a single Giftpages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid = 1) {
                return $this->render('view', [
                    'model' => $this->findModel($id),
                ]);

            } else {
                return $this->goHome();
            }
        } else $this->goHome();
    }

    /**
     * Creates a new Giftpages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid = 1) {
                $model = new Giftpages();
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            } else {
                return $this->goHome();
            }
        } else $this->goHome();
    }

    /**
     * Updates an existing Giftpages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid = 1) {
                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            } else {
                return $this->goHome();
            }
        } else $this->goHome();
    }

    /**
     * Deletes an existing Giftpages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid = 1) {
                $this->findModel($id)->delete();

                return $this->redirect(['index']);
            } else {
                return $this->goHome();
            }
        } else $this->goHome();
    }

    /**
     * Finds the Giftpages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Giftpages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid = 1) {
                if (($model = Giftpages::findOne($id)) !== null) {
                    return $model;
                } else {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
            } else {
                return $this->goHome();
            }
        } else $this->goHome();

    }
}
