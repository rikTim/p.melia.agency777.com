<?php

namespace app\controllers;

use app\components\Curl;
use app\models\Balances;
use app\models\BalancesSeach;
use app\models\ChatTopDates;
use app\models\Contacts;
use app\models\ContactsSeach;
use app\models\Files;
use app\models\Girls;
use app\models\LogsSeach;
use app\models\Mainbgimage;
use app\models\Messages;
use app\models\Partners;
use app\models\Money;
use app\models\MoneySeach;
use app\models\PartnersgetmoneytypeSeach;
use app\models\Photoes;
use app\models\Reply;
use app\models\Staticpages;
use app\models\StaticpagesSeach;
use app\models\UploadForm;
use app\models\Partnersgetmoneytype;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\BaseVarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\User;
use app\components\YandexMetrica;

class AdminController extends Controller
{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->layout = 'admin';
            return true;
        }
        return false;
    }

    public function actionIndex()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {

            $filterFrom = time() - 1314871;
            $filterTo = time() + 1314871;
            return $this->render('index', [
                'from' => Yii::$app->formatter->asDatetime($filterFrom, "php:d.m.Y"),
                'to' => Yii::$app->formatter->asDatetime($filterTo, "php:d.m.Y"),
            ]);
        } else {
            return $this->goHome();
        }
    }

    public function actionGmessages($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $girlsModel = new Girls();
            $messagesModel = new Messages();
            $filesModel = new Files();
            $photosModel = new Photoes();

            $messagesToAdmin = $messagesModel->getMessagesToAdminGirl($id);
            $messagesModel->readMessage(3, $id);

            $girl = $girlsModel->getUser($id);

            $userid = $girl->userid;
            $photos = $photosModel->getOnePhoto($girl->id);

            $avatarArr = $filesModel->getOneFiles($photos['fileid']);
            $avatar = $avatarArr[$photos['fileid']];

            $allmessage = $messagesModel->getAllGirls();

            $fromuseridArr = [];

            if (!empty($allmessage)) {
                foreach ($allmessage as $key => $message) {
                    if (!in_array($message->fromuserid, $fromuseridArr)) {
                        if ($message->fromuserid == 3) {
                            if (!in_array($message->touserid, $fromuseridArr)) {
                                $fromuseridArr[] = $message->touserid;
                            }
                        } else {
                            if (!in_array($message->fromuserid, $fromuseridArr)) {
                                $fromuseridArr[] = $message->fromuserid;
                            }
                        }
                    } else {
                        unset($allmessage[$key]);
                    }
                }
            }
            $girls = [];
            $texts = [];
            $pathArr = [];


            if (!empty($fromuseridArr)) {
                $count = count($fromuseridArr);
                for ($i = 0; $i < $count; $i++) {

                    $girls[$i] = $girlsModel->getUser($fromuseridArr[$i]);
                    $photo = $photosModel->getOnePhoto($girls[$i]->id);
                    $file = $filesModel->getOneFiles($photo['fileid']);
                    $path = $file[$photo['fileid']];
                    $pathArr[$i] = $path;
                    $texts[] = $messagesModel->getTextMessageG($fromuseridArr[$i]);
                }
            }

            return $this->render('gmessages', [
                'girls' => $girls,
                'texts' => $texts,
                'messagesToAdmin' => $messagesToAdmin,
                'avatar' => $avatar,
                'girl' => $girl,
                'pathArr' => $pathArr,
            ]);

        } else {
            return $this->goHome();
        }
    }

    public function actionPartmessages()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            if (empty($id)) {
                $messagesModel = new Messages();
                $list = $messagesModel->getOneMessage();;
                if ($list->fromuserid == 3) {
                    $id = $list->touserid;
                } else {
                    $id = $list->fromuserid;
                }
            }
            if (!empty($id)) {
                return $this->redirect(['admin/pmessages?id=' . $id]);
            } else {
                return $this->render('pmessages');
            }
        } else {
            return $this->goHome();
        }
    }

    public function actionGirlmessages()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            if (empty($id)) {
                $messagesModel = new Messages();
                $girlModel = new Girls();
                $list = $messagesModel->getOneMessageG();;
                if ($list->fromuserid == 3) {
                    $id = $list->touserid;
                } else {
                    $id = $list->fromuserid;
                }
            }

            if (!empty($id)) {
                return $this->redirect(['admin/gmessages?id=' . $id]);
            } else {
                return $this->render('gmessages');
            }


        } else {
            return $this->goHome();
        }
    }

    public function actionPmessages($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $partnersModel = new Partners();
            $messagesModel = new Messages();
            $filesModel = new Files();

            $messagesToAdmin = $messagesModel->getMessagesToAdminPartner($id);
            // read all new messages by admin
            $messagesModel->readMessage(3, $id);

            $partner = $partnersModel->getUser($id);


//            $partner->statusid = 2; @todo понять зачем
            $userid = $partner->avatar;
            $avatarArr = $filesModel->getOneFiles($userid);
            $avatar = $avatarArr[$userid];
            $allmessage = $messagesModel->getAllPartner();
            $fromuseridArr = [];

            if (!empty($allmessage)) {
                foreach ($allmessage as $key => $message) {
                    if (!in_array($message->fromuserid, $fromuseridArr)) {
                        if ($message->fromuserid == 3) {
                            if (!in_array($message->touserid, $fromuseridArr)) {
                                $fromuseridArr[] = $message->touserid;
                            }
                        } else {
                            if (!in_array($message->fromuserid, $fromuseridArr)) {
                                $fromuseridArr[] = $message->fromuserid;
                            }
                        }

                    } else {
                        unset($allmessage[$key]);
                    }
                }
            }

            $partners = [];
            $texts = [];
            if (!empty($fromuseridArr)) {
                $count = count($fromuseridArr);
                for ($i = 0; $i < $count; $i++) {
                    $partners[$i] = $partnersModel->getUser($fromuseridArr[$i]);
                    $file = $filesModel->getOneFiles($partners[$i]->avatar);
                    $path = $file[$partners[$i]->avatar];
//                    $partners[$i] = $path;
                    $texts[] = $messagesModel->getTextMessage($fromuseridArr[$i]);
                }
            }

            return $this->render('pmessages', [
                'partners' => $partners,
                'texts' => $texts,
                'messagesToAdmin' => $messagesToAdmin,
                'avatar' => $avatar,
                'partner' => $partner,
            ]);

        } else {
            return $this->goHome();
        }

    }

    public function actionMainsettings()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {

            $textModel = new Mainbgimage();
            $filesModel = new Files();
            $searchModel = new MoneySeach();

            $melia = $textModel->getHead('2');
            $pmelia = $textModel->getHead('9');
            $meliaimage = $melia->fileid;
            $pmeliaimage = $pmelia->fileid;
            $Arrmelia = $filesModel->getOneFiles($meliaimage);
            $Arrpmelia = $filesModel->getOneFiles($pmeliaimage);
            $pathmelia = $Arrmelia[$meliaimage];
            $pathpmelia = $Arrpmelia[$pmeliaimage];


            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


            return $this->render('mainsettings', [
                "melia" => $pathmelia,
                "pmelia" => $pathpmelia,
                "textmelia" => $melia->text,
                "textpmelia" => $pmelia->text,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->goHome();
        }
    }

    public function actionStaticpages()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $searchModel = new StaticpagesSeach();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('staticPages', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->goHome();
        }

    }

    //Static Pages
    public function actionCreate()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $model = new Staticpages();
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->goHome();
        }
    }

    public function actionUpdate($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->goHome();
        }

    }

    protected function findModel($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            if (($model = Staticpages::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');

            }
        } else {
            return $this->goHome();
        }

    }

    public function actionView($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            return $this->goHome();
        }

    }

    public function actionDelete($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $this->findModel($id)->delete();
            return $this->redirect(['staticpages']);
        } else {
            return $this->goHome();
        }
    }

    public function actionHeadmelia()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $model = new UploadForm();
            $textModel = new Mainbgimage();
            $main = $textModel->getHead('2');
            $image = $main->fileid;
            $text = $main->text;
            if (Yii::$app->request->isPost) {
                $newtext = $_POST['Mainbgimage'];
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                $imageId = $model->uploadbgimage();

                if ($imageId == 1) {
                    if ($text == $newtext['text']) {

                        return $this->render('headmelia', [
                            'model' => $model,
                            'text' => $main,
                        ]);

                    } else {

                        $main->text = $newtext['text'];
                        $main->fileid = $image;

                        $main->save();
                        return $this->redirect('mainsettings');
                    }
                }
                $main->fileid = $imageId;
                $main->save();
                return $this->redirect('mainsettings');
            }
            return $this->render('headmelia', ['model' => $model, 'text' => $main]);
        } else {
            return $this->goHome();
        }
    }

    public function actionHeadpmelia()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $model = new UploadForm();
            $textModel = new Mainbgimage();
            $main = $textModel->getHead('9');
            $image = $main->fileid;
            $text = $main->text;
            if (Yii::$app->request->isPost) {
                $newtext = $_POST['Mainbgimage'];
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                $imageId = $model->uploadbgimage();
                if ($imageId == 1) {

                    if ($text == $newtext['text']) {

                        return $this->render(' headpmelia', [
                            'model' => $model,
                            'text' => $main,
                        ]);

                    } else {

                        $main->text = $newtext['text'];
                        $main->fileid = $image;

                        $main->save();
                        return $this->redirect('mainsettings');
                    }
                }
                $main->fileid = $imageId;
                $main->save();
                return $this->redirect('mainsettings');
            }
            return $this->render(' headpmelia', ['model' => $model, 'text' => $main]);
        } else {
            return $this->goHome();
        }
    }

    public function actionBalances()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $searchModel = new PartnersgetmoneytypeSeach();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('balances', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->goHome();
        }
    }

    public function actionBalance($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $partnerModel = new Partners();
            $balancesModel = new Balances();
            $partnersGetMoneyTypeModel = new Partnersgetmoneytype();
            $data = $_POST['Balances'];
            if (!empty($data)) {
                $userId = $data['userid'];
                $transaction = $data['transaction'];
                $balancesModel->addTransaction($userId, $transaction);
                return $this->redirect('balances');
            } else {
                $partner = $partnerModel->getPartnerById($id);
                if (!empty($partner)) {
                    $partner->balance = $balancesModel->getPartnerBalance($partner->userid);
                    $partner->getMoney = $partnersGetMoneyTypeModel->getGetMoneyTypeByUserId($partner->userid);
                    $isMonthRecount = $balancesModel->checkMonthrecountByUserId($partner->userid);

                    $alltransaction = $balancesModel->getTransactionUser($partner->userid);

                    return $this->render('balance', [
                        'partner' => $partner,
                        'isRecount' => (!empty($isMonthRecount)) ? 1 : 0,
                        'alltransaction' => $alltransaction,
                    ]);
                } else {
                    return $this->redirect('balances');
                }
            }
        } else {
            return $this->goHome();
        }
    }

    public function actionPaymentbill()
    {

        $partnerModel = new Partners();

        return $this->render('paymentbill', [
            'paymentbill' => $partnerModel->getBalancesForBill()
        ]);
    }

    public function actionGetexcelbill()
    {
        $partnerModel = new Partners();

        $payBill = $partnerModel->getBalancesForBill();

        $xls = new \PHPExcel();

        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);

        // Получаем активный лист
        $sheet = $xls->getActiveSheet();

        // Подписываем лист
        $sheet->setTitle('Выписка счетов за ' . date('F Y'));


        $sheet->setCellValueByColumnAndRow(0, 1, "Выписка счетов за " . date('F Y'));
        $sheet->mergeCells('A1:G1');

        // Заголовки столбцов
        $cols = ['Partner', 'Country', 'City', 'Phone', 'Payment method', 'Other', 'Total $'];
        for ($i = 0, $len = sizeof($cols); $i < $len; $i++) {
            $sheet->setCellValueByColumnAndRow($i, 2, $cols[$i]);
        }

        // Заносим сами данные
        $i = 3;
        foreach ($payBill as $partnerRow) {
            $j = 0;
            foreach ($partnerRow as $pr) {
                $sheet->setCellValueByColumnAndRow($j, $i, $pr);
                $j++;
            }
            $i++;
        }


        // Выводим HTTP-заголовки
        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=Выписка счетов за " . date('F Y') . ".xls");

        // Выводим содержимое файла
        $objWriter = new \PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');
    }

    public function actionRecountmoney()
    {
        $partnerId = $_POST['partnerId'];
        $partnersModel = new Partners();
        $balancesModel = new Balances();
        $userId = $partnersModel->getPartnerById($partnerId)->userid;
        $balancesModel->recountMoneyPerMonthByUserId($userId, $partnerId);
        return true;
    }

    protected function findModelmoney($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            if (($model = Money::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');

            }
        } else {
            return $this->goHome();
        }

    }

    public function actionUpdatemoney($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $model = $this->findModelmoney($id);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['mainsettings']);
            } else {
                return $this->render('updatemoney', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->goHome();
        }

    }

    public function actionAddmessage()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $userId = Yii::$app->request->post('userId');
            $typeId = Yii::$app->request->post('typeId');
            $text = Yii::$app->request->post('text');

            if (!empty($userId) && !empty($typeId) && !empty($text)) {
                $messageModel = new Messages();
                $messageModel->writeMessage(3, 1, $userId, $typeId, $text);
            }
        } else {
            return $this->goHome();
        }
    }

    public function actionIpaddress()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $searchModel = new LogsSeach();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('ipaddress', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->goHome();
        }
    }

    public function actionStatistic()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $domain = 'https://vk.com/murblska';
            $needle = '.com/';
            $pos = strpos($domain, $needle);
            $pos = $pos + 5;
            $vklink = substr($domain, $pos);
            print_r($vklink);


        } else {
            return $this->goHome();
        }
    }

    public function actionContacts()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $searchModel = new ContactsSeach();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $contactsModel = new Contacts();
            $contactsModel->updateAll(['is_readed' => 1], ['is_readed' => 0]);
            return $this->render('contacts', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->goHome();
        }
    }

    public function actionMetrica()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $metrica = new YandexMetrica([
                'user_login' => Yii::$app->params['Yandex']['user_login'],
                'user_password' => Yii::$app->params['Yandex']['user_password'],
                'oauth_id' => Yii::$app->params['Yandex']['oauth_id'],
                'oauth_password' => Yii::$app->params['Yandex']['oauth_password']
            ]);

            $trafficSummary = $metrica->trafficSummary(Yii::$app->params['Yandex']['counter_id']);
            $trafficDeepness = $metrica->trafficDeepness(Yii::$app->params['Yandex']['counter_id']);
            $trafficHourly = $metrica->trafficHourly(Yii::$app->params['Yandex']['counter_id']);
            $trafficLoad = $metrica->trafficLoad(Yii::$app->params['Yandex']['counter_id']);
            $geo = $metrica->geo(Yii::$app->params['Yandex']['counter_id']);

            return $this->render('metrica', [
                'trafficSummary' => $trafficSummary,
                'trafficDeepness' => $trafficDeepness,
                'trafficHourly' => $trafficHourly,
                'trafficLoad' => $trafficLoad,
                'geo' => $geo
            ]);
        } else {
            return $this->goHome();
        }
    }

    public function actionReply()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {


            $replyModel = new Reply();
            $girlModel = new Girls();
            $chatModel = new ChatTopDates();
            $reply = $replyModel->getAnswerMe();

            foreach ($reply as $rep) {
                $girl_id = $rep->girl_id;
                $men_id = $rep->men_id;
                $girl = $girlModel->find()->where(['topdatesid' => $girl_id])->one();
                $rep->girl_name = $girl->name . ' ' . $girl->lastname;
                $chat = $chatModel->find()->where(['girl_id' => $girl_id, 'men_id' => $men_id])->orderBy('datetime DESC')->one();
                $rep->text = $chat->text;
                $rep->men_name = $chat->men_name;
                $rep->message_id = $chat->id;
                $is_men_letter = $chat->is_men_letter;
                if ($is_men_letter == 1) {
                    $answer_me[] = $rep;
                }
            }

            return $this->render('reply', [
                'reply' => $answer_me,
            ]);

        } else {
            return $this->goHome();

        }
    }

    public function actionAnswerme($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $chatModel = new ChatTopDates();
            $girlModel = new Girls();
            $chat = $chatModel->find()->where(['id' => $id])->one();
            $girl_id = $chat->girl_id;
            $men_id = $chat->men_id;
            if ($chatModel->load(Yii::$app->request->post())) {
                $text = $chatModel->text;
                $chatModel->girl_id = $chat->girl_id;
                $chatModel->men_id = $chat->men_id;
                $chatModel->men_name = $chat->men_name;
                $chatModel->image = $chat->image;
                $chatModel->is_men_letter = 0;
                $chatModel->is_readed = 1;
                $chatModel->is_aproved = 1;
                $chatModel->datetime = time();
                $chatModel->is_notification = 1;

                $chatModel->save();
                Curl::sendMessage($girl_id, $men_id, $text);
                return $this->redirect(['reply']);
            }


            $girl = $girlModel->getGirlTopdates($girl_id);
            $girlName = $girl->name . ' ' . $girl->lastname;

            $newMessage = $chatModel;


            return $this->render('answer', [
                'chat' => $chat,
                'girlName' => $girlName,
                'newMessage' => $newMessage,
            ]);
        } else {
            return $this->goHome();
        }


    }

    public function actionRefuse()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {


            $replyModel = new Reply();
            $girlModel = new Girls();
            $chatModel = new ChatTopDates();
            $reply = $replyModel->getRefuse();
            $answer_me = '';

            foreach ($reply as $rep) {
                $girl_id = $rep->girl_id;
                $men_id = $rep->men_id;
                $girl = $girlModel->find()->where(['topdatesid' => $girl_id])->one();
                $rep->girl_name = $girl->name . ' ' . $girl->lastname;
                $chat = $chatModel->find()->where(['girl_id' => $girl_id, 'men_id' => $men_id])->orderBy('datetime DESC')->one();
                $rep->text = $chat->text;
                $rep->men_name = $chat->men_name;
                $rep->message_id = $chat->id;
                $is_men_letter = $chat->is_men_letter;
                if ($is_men_letter == 1) {
                    $answer_me[] = $rep;


                }
            }
            return $this->render('refuse', [
                'reply' => $answer_me,
            ]);
        } else {
            return $this->goHome();
        }
    }

    public function actionGetgirlstatistics()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $girlsModel = new Girls();

            $filterFrom = Yii::$app->request->post('filterFrom');
            $filterTo = Yii::$app->request->post('filterTo');


            if (!empty($filterFrom)) {
                $filterFrom = strtotime($filterFrom);
            } else {
                $filterFrom = time() - 1314871; // текущая дата - 15 дней
            }

            if (!empty($filterTo)) {
                $filterTo = strtotime($filterTo);
            } else {
                $filterTo = time() + 1314871; // текущая дата + 15 дней
            }

            $statistics = $girlsModel->getAdminStatistics($filterFrom, $filterTo, false);
            foreach ($statistics as &$statictic) {
                $statictic['registerdate'] = Yii::$app->formatter->asDatetime($statictic['registerdate'], "php:d.m.Y");
            }

            $apStatistics = $girlsModel->getAdminStatistics($filterFrom, $filterTo, 6);
            foreach ($apStatistics as &$statictic) {
                $statictic['registerdate'] = Yii::$app->formatter->asDatetime($statictic['registerdate'], "php:d.m.Y");
            }

            $reStatistics = $girlsModel->getAdminStatistics($filterFrom, $filterTo, 7);
            foreach ($reStatistics as &$statictic) {
                $statictic['registerdate'] = Yii::$app->formatter->asDatetime($statictic['registerdate'], "php:d.m.Y");
            }

            $reStatisticsCount = count($reStatistics);
            $apStatisticsCount = count($apStatistics);
            for ($i = 0, $len = count($statistics); $i < $len; $i++) {
                if (!Yii::$app->DLL->inMultiarray($statistics[$i]['registerdate'], $apStatistics)) {
                    $apStatistics[$apStatisticsCount]['registerdate'] = $statistics[$i]['registerdate'];
                    $apStatistics[$apStatisticsCount]['count'] = 0;
                    $apStatisticsCount++;
                }
                if (!Yii::$app->DLL->inMultiarray($statistics[$i]['registerdate'], $reStatistics)) {
                    $reStatistics[$reStatisticsCount]['registerdate'] = $statistics[$i]['registerdate'];
                    $reStatistics[$reStatisticsCount]['count'] = 0;
                    $reStatisticsCount++;
                }
            }

            $apStatistics = Yii::$app->DLL->bubbleSort($apStatistics);
            $reStatistics = Yii::$app->DLL->bubbleSort($reStatistics);


            echo json_encode([$statistics, $apStatistics, $reStatistics]);
        } else {
            return $this->goHome();
        }
    }

    public function actionGetpartnerstatistics()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 1) {
            $partnersModel = new Partners();

            $filterFrom = Yii::$app->request->post('filterFrom');
            $filterTo = Yii::$app->request->post('filterTo');


            if (!empty($filterFrom)) {
                $filterFrom = strtotime($filterFrom);
            } else {
                $filterFrom = time() - 1314871; // текущая дата - 15 дней
            }

            if (!empty($filterTo)) {
                $filterTo = strtotime($filterTo);
            } else {
                $filterTo = time() + 1314871; // текущая дата + 15 дней
            }

            $statistics = $partnersModel->getAdminStatistics($filterFrom, $filterTo, false);
            foreach ($statistics as &$statictic) {
                $statictic['registerdate'] = Yii::$app->formatter->asDatetime($statictic['registerdate'], "php:d.m.Y");
            }

            $apStatistics = $partnersModel->getAdminStatistics($filterFrom, $filterTo, 6);
            foreach ($apStatistics as &$statictic) {
                $statictic['registerdate'] = Yii::$app->formatter->asDatetime($statictic['registerdate'], "php:d.m.Y");
            }

            $reStatistics = $partnersModel->getAdminStatistics($filterFrom, $filterTo, 7);
            foreach ($reStatistics as &$statictic) {
                $statictic['registerdate'] = Yii::$app->formatter->asDatetime($statictic['registerdate'], "php:d.m.Y");
            }

            $reStatisticsCount = count($reStatistics);
            $apStatisticsCount = count($apStatistics);
            for ($i = 0, $len = count($statistics); $i < $len; $i++) {
                if (!Yii::$app->DLL->inMultiarray($statistics[$i]['registerdate'], $apStatistics)) {
                    $apStatistics[$apStatisticsCount]['registerdate'] = $statistics[$i]['registerdate'];
                    $apStatistics[$apStatisticsCount]['count'] = 0;
                    $apStatisticsCount++;
                }
                if (!Yii::$app->DLL->inMultiarray($statistics[$i]['registerdate'], $reStatistics)) {
                    $reStatistics[$reStatisticsCount]['registerdate'] = $statistics[$i]['registerdate'];
                    $reStatistics[$reStatisticsCount]['count'] = 0;
                    $reStatisticsCount++;
                }
            }

            $apStatistics = Yii::$app->DLL->bubbleSort($apStatistics);
            $reStatistics = Yii::$app->DLL->bubbleSort($reStatistics);


            echo json_encode([$statistics, $apStatistics, $reStatistics]);
        } else {
            return $this->goHome();
        }
    }

    public function actionFinance()
    {
        $searchModel = new BalancesSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('finance', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionNoanswered()
    {
        $chatModel = new ChatTopDates();
        $messages = $chatModel->find()->where(['is_answer' => 0, 'is_men_letter' => 1])->all();


        return $this->render('noanswered', ['messages' => $messages]);
    }
}
