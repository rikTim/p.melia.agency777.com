<?php

namespace app\controllers;

use app\models\Balances;
use app\models\Files;
use app\models\Girls;
use app\models\Logs;
use app\models\Messages;
use app\models\MoneyType;
use app\models\PartnersSeach;
use app\models\Statuses;
use app\models\Users;
use Yii;
use app\models\Partners;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PartnersController implements the CRUD actions for Partners model.
 */
class PartnersController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->layout = 'admin';
            return true;
        }
        return false;
    }

    /**
     * Lists all Partners models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid == 1) {


                $partnerModel = new Partners();
                $statusModel = new Statuses();
                $searchModel = new PartnersSeach();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $allPartner = count($partnerModel->getAllPartners());
                $newPartner = count($partnerModel->getNewPartner());
                $approvedPartner = count($partnerModel->getApprovedPartner());
                $rejectedPartner = count($partnerModel->getRejectedPartner());

                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'allPartner' => $allPartner,
                    'newPartner' => $newPartner,
                    'approvedPartner' => $approvedPartner,
                    'rejectedPartner' => $rejectedPartner,
                ]);
            } else {
                return $this->goHome();
            }
        } else $this->goHome();
    }

    /**
     * Displays a single Partners model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid == 1) {
                $model = $this->findModel($id);
                $userUpdate = $model->id;
                $userid = $model->userid;


                $statuses = new Statuses();
                $partnerModel = new Partners();
                $filesModel = new Files();
                $moneytype = new MoneyType();
                $userModel = new Users();

                $partner = $partnerModel->getOnePartner($userUpdate);
                $avatarArr = $filesModel->getOneFiles($partner->avatar);
                $status = $statuses->getOneStatus($model->statusid);
                $user = $userModel->find()->where(['id' => $userid])->one();
                $username = $user->username;
                $avatar = $avatarArr[$partner->avatar];
                $moneyArr = $moneytype->GetPartner($userid);
                $type = $moneyArr->getmoneytype;
                if ($type = 1) {
                    $money = 'per month';
                } else {
                    $money = 'per month';
                }


                return $this->render('view', [
                    'model' => $model,
                    'status' => $status,
                    'avatar' => $avatar,
                    'money' => $money,
                    'username' => $username,

                ]);
            } else {
                return $this->goHome();
            }
        } else $this->goHome();
    }

    /**
     * Creates a new Partners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid == 1) {
                $model = new Partners();
                $statuses = new Statuses();

                $users = $statuses->getAllUser();

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                        'users' => $users,
                    ]);
                }
            } else {
                return $this->goHome();
            }
        } else $this->goHome();

    }

    /**
     * Updates an existing Partners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid == 1) {
                $model = $this->findModel($id);
                $statuses = new Statuses();
                $partnerModel = new Partners();

                $userUpdate = $model->id;

                $filesModel = new Files();
                $partner = $partnerModel->getPartnerById($userUpdate);

                $avatarArr = $filesModel->getOneFiles($partner->avatar);
                $avatar = $avatarArr[$partner->avatar];


                $users = $statuses->getAllUser();
                $payMoney = ([
                    'Western Union' => 'Western Union',
                    'Visa' => 'Visa',
                    'Money Gram' => 'Money Gram',
                ]);
                if ($model->load(Yii::$app->request->post()) && $model->save()) {

                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                        'users' => $users,
                        'avatar' => $avatar,
                        'payMoney' => $payMoney,

                    ]);
                }
            } else {
                return $this->goHome();
            }
        } else $this->goHome();
    }

    /**
     * Deletes an existing Partners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid == 1) {

                $partner = $this->findModel($id);
                $girlModel = new Girls();
                $filesModel = new Files();
                $typemoneyModel = new MoneyType();
                $balancesModel = new Balances();
                $logModel = new Logs();
                $messagesModel = new Messages();
                $userModel = new Users();


                $allmessages = $messagesModel->getAllMessageToGirl($id);
                $allgirl = $girlModel->find()->where(['partnerid' => $id])->all();
                $avatar = $filesModel->find()->where(['id' => $partner->avatar])->one();
                $typeMoney = $typemoneyModel->find()->where(['userid' => $partner->userid])->one();
                $Allbalances = $balancesModel->find()->where(['userid' => $partner->userid])->all();
                $log = $logModel->find()->where(['userid' => $partner->userid])->one();
                $user = $userModel->find()->where(['id' => $partner->userid])->one();

                if (!empty($allmessages)) {
                    foreach ($allmessages as $messages) {
                        $messages->delete();
                    }
                }
                if (!empty($allgirl)) {
                    foreach ($allgirl as $girl) {
                        $girl->partnerid = '';
                        $girl->save();
                    }
                }
                if (!empty($avatar)) {
                    unlink('../../p.melia.agency777.com/web/' . $avatar->path);
                    $avatar->delete();
                }
                if (!empty($typeMoney)) {
                    $typeMoney->delete();
                }
                if (!empty($Allbalances)) {
                    foreach ($Allbalances as $balances) {
                        $balances->delete();
                    }
                }
                if (!empty($log)) {
                    $log->delete();
                }
                $user->delete();
                $this->findModel($id)->delete();

                return $this->redirect(['index']);
            } else {
                return $this->goHome();
            }
        } else $this->goHome();
    }

    /**
     * Finds the Partners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Partners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid == 1) {
                if (($model = Partners::findOne($id)) !== null) {
                    return $model;
                } else {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
            } else {
                return $this->goHome();
            }
        } else $this->goHome();
    }
}
