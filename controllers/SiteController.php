<?php

namespace app\controllers;

use app\models\Authkeys;
use app\models\Balances;
use app\models\ContactForm;
use app\models\Contacts;
use app\models\Files;
use app\models\Girls;
use app\models\LoginForm;
use app\models\Mainbgimage;
use app\models\Messages;
use app\models\MoneyType;
use app\models\Partners;
use app\models\Partnersgetmoneytype;
use app\models\Staticpages;
use app\models\Users;
use Faker\Provider\Base;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\User;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * index page action
     */
    public function actionIndex()
    {
        $this->checkAccess();
        $mainHeader = new Mainbgimage();
        $fileModel = new Files();
        $userModel = new Users();
        $model = new LoginForm();
        $header = $mainHeader->getHeaderText();
        $imageid = $header->fileid;
        $arrimage = $fileModel->getOneFiles($imageid);
        $path = $arrimage[$imageid];

        if (!empty($_COOKIE['lang'])) {
            $lang = strtolower($_COOKIE['lang']);
        } else {
            $lang = 'en';
        }
        $lntext = 'text_' . $lang;

        $params = [
            'model' => $model,
            'userModel' => $userModel,
            'header' => $header,
            'image' => $path,
            'lntext' => $lntext,
        ];

        $post = Yii::$app->request->post('Users');
        $loginPost = Yii::$app->request->post('LoginForm');

        // пробуем регестрировать нового партнера
        if (!empty($post)) {
            $register = $userModel->toUser($post, false);
            // уже есть такой партнер
            if ($userModel->hasErrors() == true) {
                return $this->render('index', $params);
            } else {
                $authKeyModel = new Authkeys();
                $authKey = $authKeyModel->createKey();

                $get = "auth=" . $authKey . "&password=" . md5($post['password']) . "&username=" . $post['username'] . "&email=" . $post['email'];
                $text = 'Hello dear ' . $post['username'] . '<br />' .
                    'Your email was filled in registration form. To complete registration please go on this ' .
                    '<a href="http://meliapartner.com/web/register?' . $get . '">link</a><br />' .
                    'If you do not regístrate in our site, please ignore this email. <br /><br />' .
                    'Best wishes <a href="http://meliapartner.com/">meliapartner.com</a>';


                $sendMess = Yii::$app->DLL->sendEmail($text, 'Users registrations', $post['email']);
                if ($sendMess == false) {
                    // выводим. что сообщение успешно отправлено
                    $params['message'] = 'Try one more time please.';
                } else {
                    // выводим. что сообщение успешно отправлено
                    $params['message'] = "Thank you for signing up , we have sent you a letter to confirm your mailbox. Our letter comes from admin@meliapartner.com and sometimes gets in the spam , please go to our letter and click on the link . In this way, we understand that you are a real person and not a robot . If, however, it is not a letter , please write to us here and we'll try to help you as soon as possible .";
                }

//                var_dump($sendMess); die();
                return $this->render('index', $params);
            }
        } else if (!empty($loginPost)) {
            // пробуем авторизоваться
            $username = $loginPost['username'];
            $password = $loginPost['password'];
            if (!empty($username) && !empty($password)) {
                $login = array(
                    '_csrf' => 'LVhCaGJBZVZ7ahEuEg5dORoiJQs4EiQRFAoWHAEvCCRuN3UCGwwMNA==',
                    'LoginForm' => array(
                        'username' => $username,
                        'password' => $password,
                        'rememberMe' => 1,
                    ),
                    'login-button' => '',
                );
                $model->load($login);
                $model->login(true);

                if ($model->hasErrors()) {
                    return $this->render('index', $params);
                } else {
                    $userType = Yii::$app->user->identity->usertypeid;
                    if ($userType == 1) {
                        return $this->redirect(['admin/index']);
                    } else {
                        return $this->redirect(['/main']);
                    }
                }
            } else {
                return $this->render('index', $params);
            }
        } else {
            return $this->render('index', $params);
        }
    }

    /**
     *
     */
    public function checkAccess()
    {
        if (!\Yii::$app->user->isGuest) {
            if (yii::$app->user->identity->usertypeid == 1) {
                return $this->redirect(['/admin/index']);
            }
            if (yii::$app->user->identity->usertypeid == 2) {
                $partnerid = Yii::$app->user->identity->id;
                $parterModel = new Partners();
                $partnerStatusid = $parterModel->getUserStatus($partnerid);
                $status = $partnerStatusid['statusid'];
                if ($status == 7) {
                    return $this->redirect(['/contact']);
                }
                if ($status == 6) {
                    return $this->redirect(['/main']);
                } else {
                    return $this->redirect(['/profile/edit']);
                }
            }
        }

        return true;
    }

    /**
     *
     */
    public function actionRegister()
    {
        $password = Yii::$app->request->get('password');
        $username = Yii::$app->request->get('username');
        $email = Yii::$app->request->get('email');
        $key = Yii::$app->request->get('auth');
        $get = Yii::$app->request->get();

        if (!empty($key) && !empty($password) && !empty($username) && !empty($email)) {
            $usersModel = new Users();
            $authKeyModel = new Authkeys();
            // проверяем ключ
            $checkKey = $authKeyModel->checkKey($key);
            if ($checkKey == true) {
                // используем ключ
                $authKeyModel->useKey($key);
                $check = $usersModel->toUser($get, true);
                if ($check) {
                    $text = 'На сайте meliapartner.com зарегестрирован новый партнер: ' . $username;
                    Yii::$app->DLL->sendEmail($text, 'Зарегестрировался новый партнер', 'agencykievodessa@gmail.com ');
                    return $this->redirect('main');
                } else {
                    return $this->redirect('index');
                }
            } else {
                return $this->redirect('index');
            }
        } else {
            return $this->redirect('index');
        }
    }


    /**
     *
     */
    public function actionMain()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        $userid = Yii::$app->user->identity->id;

        if (!empty($userType) && $userType == 2) {
            $partnersgetmoneytypeModel = new Partnersgetmoneytype();
            $getMoneyType = $partnersgetmoneytypeModel->getGetMoneyTypeIdByUserId($userid);
            if (empty($getMoneyType)) {
                $userModel = new Users();
                $user = $userModel->getUserByUserId($userid);

                $thanksText = ' ';

                $pageModel = new Staticpages();
                $aboutPage = $pageModel->getText('32');
                $lntext = 'text_' . Yii::$app->params['language'];

                $userModel->greetedByUserId($userid);

                return $this->render('main', [
                    'thanksText' => $thanksText,
                    'aboutPage' => $aboutPage,
                    'lntext' => $lntext,
                    'user' => $user
                ]);

            } else {
                $from = Yii::$app->formatter->asDatetime(time() - 1314871, "php:d.m.Y");
                $to = Yii::$app->formatter->asDatetime(time() + 1314871, "php:d.m.Y");
                return $this->render('main', [
                    'from' => $from,
                    'to' => $to
                ]);
            }
        } else {
            return $this->goHome();
        }
    }


    /**
     *
     */
    public function actionPassword()
    {
        return $this->render('password');
    }


    /**
     *
     */
    public function actionRemindpass()
    {
        if (Yii::$app->user->isGuest) {
            return $this->render('remindpass');
        } else {
            return $this->redirect('index');
        }
    }


    /**
     *
     */
    public function actionGetStatistics()
    {
        $userType = Yii::$app->user->identity->usertypeid;

        if (!empty($userType) && $userType == 2) {
            $userid = Yii::$app->user->id;
            $partnersModel = new Partners();
            $girlsModel = new Girls();
            $partner = $partnersModel->getUser($userid);
            $partnerId = $partner->id;

            $filterFrom = Yii::$app->request->post('filterFrom');
            $filterTo = Yii::$app->request->post('filterTo');


            if (!empty($filterFrom)) {
                $filterFrom = strtotime($filterFrom);
            } else {
                $filterFrom = time() - 1314871; // текущая дата - 15 дней
            }

            if (!empty($filterTo)) {
                $filterTo = strtotime($filterTo);
            } else {
                $filterTo = time() + 1314871; // текущая дата + 15 дней
            }

            $statistics = $girlsModel->getStatistics($filterFrom, $filterTo, $partnerId, false);
            foreach ($statistics as &$statictic) {
                $statictic['registerdate'] = Yii::$app->formatter->asDatetime($statictic['registerdate'], "php:d.m.Y");
            }

            $apStatistics = $girlsModel->getStatistics($filterFrom, $filterTo, $partnerId, 6);
            foreach ($apStatistics as &$statictic) {
                $statictic['registerdate'] = Yii::$app->formatter->asDatetime($statictic['registerdate'], "php:d.m.Y");
            }

            $reStatistics = $girlsModel->getStatistics($filterFrom, $filterTo, $partnerId, 7);
            foreach ($reStatistics as &$statictic) {
                $statictic['registerdate'] = Yii::$app->formatter->asDatetime($statictic['registerdate'], "php:d.m.Y");
            }

            $reStatisticsCount = count($reStatistics);
            $apStatisticsCount = count($apStatistics);
            for ($i = 0, $len = count($statistics); $i < $len; $i++) {
                if (!Yii::$app->DLL->inMultiarray($statistics[$i]['registerdate'], $apStatistics)) {
                    $apStatistics[$apStatisticsCount]['registerdate'] = $statistics[$i]['registerdate'];
                    $apStatistics[$apStatisticsCount]['count'] = 0;
                    $apStatisticsCount++;
                }
                if (!Yii::$app->DLL->inMultiarray($statistics[$i]['registerdate'], $reStatistics)) {
                    $reStatistics[$reStatisticsCount]['registerdate'] = $statistics[$i]['registerdate'];
                    $reStatistics[$reStatisticsCount]['count'] = 0;
                    $reStatisticsCount++;
                }
            }


            $apStatistics = Yii::$app->DLL->bubbleSort($apStatistics);
            $reStatistics = Yii::$app->DLL->bubbleSort($reStatistics);


            echo json_encode([$statistics, $apStatistics, $reStatistics]);
        } else {
            return $this->goHome();
        }
    }


    /**
     *
     */
    public function actionMessages()
    {
        $userType = Yii::$app->user->identity->usertypeid;

        if (!empty($userType) && $userType == 2) {
            return $this->render('messages');
        } else {
            return $this->goHome();
        }
    }


    /**
     *
     */
    public function actionGetmessages()
    {
        $userType = Yii::$app->user->identity->usertypeid;

        if (!empty($userType) && $userType == 2) {
            $userid = Yii::$app->user->id;
            $manId = Yii::$app->request->post('manId');
            $girlModel = new Girls();
            $messagesModel = new Messages();

            $girl = $girlModel->getUser($userid);
            $girlId = $girl->id;

            $firstManMessages = $messagesModel->getFirstManMessage($girlId, $manId);

            $fmm = [];
            $i = 0;
            foreach ($firstManMessages as $key => $message) {
                $fmm[$i]['fromid'] = $message['fromid'];
                $fmm[$i]['text'] = $message['text'];
                $fmm[$i]['datetime'] = Yii::$app->formatter->asDatetime($message['datetime'], "php:H:i d.m.Y");
                $fmm[$i]['girlId'] = $girlId;
                $i++;
            }
            echo json_encode($fmm);
        } else {
            return $this->goHome();
        }
    }


    /**
     *
     */
    public function actionAddmessage()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 2) {
            $text = Yii::$app->request->post('text');
            $toid = Yii::$app->request->post('toid');
            $fromid = Yii::$app->request->post('fromid');

            $messageModel = new Messages();
            $messageModel->chatMessage($toid, $fromid, $text);
            return $this->redirect(['messages']);
        } else {
            return $this->goHome();
        }
    }


    /**
     * contacts page action
     */
    public function actionContact()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        $userId = Yii::$app->user->identity->id;


        if (!empty($userType) && $userType == 2) {

            $lang = !empty($_COOKIE['lang']) ? 'text_' . strtolower($_COOKIE['lang']) : 'text_en';

            $messagesModel = new Messages();
            $partnerModel = new Partners();
            $filesModel = new Files();

            $partner = $partnerModel->getUser($userId);
            if (!empty($partner)) {
                $messages = $messagesModel->getContactsMessages($userId);
                $messagesModel->readMessage($userId, 3);
                $file = $filesModel->getLink($partner->avatar);
            }

            $model = new ContactForm();
            if ($model->load(Yii::$app->request->post())) {
                $text = nl2br($_POST['ContactForm']['text']);
                $messagesModel->writeMessage($partner->userid, 2, 3, 1, $text);

                return Yii::$app->getResponse()->redirect(Yii::$app->getRequest()->getUrl());

            } else {
                return $this->render('contact', [
                    'partnerId' => $userId,
                    'model' => $model,
                    'messages' => $messages,
                    'contactPage' => Staticpages::find()->where(['id' => '34'])->one()->$lang,
                    'file' => $file,
                ]);
            }
        } else {
            return $this->goHome();
        }
    }


    /**
     *
     */
    public function actionAbout()
    {
        $userType = Yii::$app->user->identity->usertypeid;

        if (!empty($userType) && $userType == 2) {
            $lang = !empty($_COOKIE['lang']) ? 'text_' . strtolower($_COOKIE['lang']) : 'text_en';

            return $this->render('about', [
                'aboutPage' => Staticpages::find()->where(['id' => '31'])->one()->$lang
            ]);
        } else {
            return $this->goHome();
        }
    }


    /**
     * training page action
     */
    public function actionTraining()
    {
        return $this->render('training');
    }


    /**
     *
     */
    public function actionBalance()
    {
        $userType = Yii::$app->user->identity->usertypeid;

        if (!empty($userType) && $userType == 2) {
            $userId = Yii::$app->user->id;
            $balancesModel = new Balances();

            $balanceList = $balancesModel->getPartnerBalanceList($userId);

            return $this->render('balance', [
                'total' => $sendMess = Yii::$app->DLL->totalBalance(),
                'balanceList' => $balanceList
            ]);
        } else {
            return $this->goHome();
        }
    }


    /**
     * ajax get money from balance
     */
    public function actionGetMoney()
    {
        $wantedSum = Yii::$app->request->post('wantedSum');
        $userId = Yii::$app->user->id;
        $userType = Yii::$app->user->identity->usertypeid;


        $messagesModel = new Messages();
        $partnersModel = new Partners();
        $balancesModel = new Balances();

        $partner = $partnersModel->getUser($userId);

        if (!empty($_COOKIE['lang'])) {
            $lang = strtolower($_COOKIE['lang']);
        } else {
            $lang = 'en';
        }

        switch ($lang) {
            case 'en':
                $text1 = 'Fill out your profile before you withdraw the funds .';
                $text2 = 'Your profile is not active . Wait for the activation .';
                $text3 = 'Your request has been sent to the administrator .';
                break;
            case 'ru':
                $text1 = 'Заполните свой профиль, перед тем, как снять средства.';
                $text2 = 'Ваш профайл не активный. Дождитесь активации.';
                $text3 = 'Ваш запрос отправлен администратору.';
                break;
            case 'cz':
                $text1 = 'Vyplňte svůj profil , než vytáhnete finančních prostředků.';
                $text2 = 'Váš profil není aktivní . Počkejte na aktivaci .';
                $text3 = 'Váš požadavek byl odeslán správci .';
                break;
        }

//        print_r($lang);die();
        if (empty($partner)) {
            Yii::$app->getSession()->setFlash('error', 'Заполните свой профиль, перед тем, как снять средства.');
            return $this->redirect('profile/edit');
        } else if ($partner->statusid != 6) {
            Yii::$app->getSession()->setFlash('error', 'Ваш профайл не активный. Дождитесь активации.');
            return $this->redirect('profile/index');
        }


        $textA = 'Хочу снять со своего баланса $' . $wantedSum; // текст который будет писаться в админку
        $messagesModel->writeMessage($userId, $userType, 3, 1, $textA);

        $textE = 'Партнер ' . $partner->name . ' ' . $partner->lastname . ' хочет снять со своего баланса $' . $wantedSum; // текст, который летит на емаил
        $subject = 'Партнер ' . $partner->name . ' ' . $partner->lastname . ' хочет снять деньги со своего баланса';
        Yii::$app->DLL->sendEmail($textE, $subject);


        $balancesModel->addTransaction($userId, $wantedSum);


        Yii::$app->getSession()->setFlash('msg', 'Ваш запрос отправлен администратору.');
        return true;
    }


    /**
     *
     */
    public function actionTypeMoney()
    {
        $userId = Yii::$app->user->id;

        $getMoneyTypeModel = new MoneyType();
        $typeMoney = $getMoneyTypeModel->GetPartner($userId);

        if (empty($typeMoney)) {
            $getMoneyTypeModel->addPartnerMoneyType($userId, $_POST['getMoneyType']);
        }
    }


    /**
     * ???
     */
    public function actionContacts()
    {
        $data = Yii::$app->request->post('Contacts');
        if (!empty($data)) {
            $contactsModel = new Contacts();
            if ($contactsModel->message($data)) {
                return $this->goHome();
            }
        }

        return true;
    }


    /**
     * denied access from some countries
     */
    public function actionDenied()
    {
        return $this->render('denied');
    }


    /**
     * accept access
     */
    public function actionAgency()
    {
        $pass = Yii::$app->request->post('pass');

        if (!empty($pass) && md5($pass . 'agency') == 'd4a090b4384440fd9a0ec526868beae7') {
            setcookie(md5('access' . 'agency'), md5('accept' . 'agency'));
            $this->redirect('index');
        }

        return $this->render('agency');
    }


    /**
     * social network login
     */
    public function actionLogin()
    {
        $serviceName = Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {
                    $usersModel = new Users();
                    $check = $usersModel->checkSocialUser($eauth->getAttributes(), $_GET['service']);
                    if ($check == false) {
                        $usersModel->addSocialUser($eauth->getAttributes(), $_GET['service']);
                        $text = 'На сайте meliapartner.com зарегестрирован новый партнер';
                        Yii::$app->DLL->sendEmail($text, 'Зарегестрировался новый партнер');
                    }
                    $eauth->redirect();
                } else {
                    $eauth->cancel();
                }
            } catch (\nodge\eauth\ErrorException $e) {
                Yii::$app->getSession()->setFlash('error', 'EAuthException: ' . $e->getMessage());
                $eauth->redirect($eauth->getCancelUrl());
            }
        }
    }

    /**
     * logout
     */
    public function actionLogout()
    {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->usertypeid = 2 || Yii::$app->user->identity->usertypeid = 1) {
                Yii::$app->user->logout();
                return $this->goHome();
            } else {
                return $this->goHome();
            }
        } else {
            return $this->goHome();
        }
    }


    /**
     * out
     */
    public function actionOut()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}