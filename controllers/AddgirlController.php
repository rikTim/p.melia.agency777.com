<?php

namespace app\controllers;

use app\models\Characters;
use app\models\Files;
use app\models\Girls;
use app\models\Languages;
use app\models\Girlslanguages;
use app\models\Logs;
use app\models\Materialstatuses;
use app\models\Occupations;
use app\models\Religions;
use app\models\Staticpages;
use app\models\Statuses;
use app\models\UploadForm;
use app\models\Photoes;
use app\models\Partners;
use app\models\Users;
use app\models\Names;
use app\models\Cities;
use Yii;
use yii\helpers\BaseFileHelper;
use yii\helpers\BaseVarDumper;
use yii\web\Controller;
use yii\web\UploadedFile;

class AddgirlController extends Controller
{
    public function actionIndex()
    {
        $userType = Yii::$app->user->identity->usertypeid;

        if (!empty($userType) && $userType == 2) {
            $userId = Yii::$app->user->identity->id;
            $partnerModel = new Partners();
            $girlsModel = new Girls();


            $partner = $partnerModel->getUser($userId);

            if (!empty($partner)) {
                $partnerGirls = $girlsModel->getGirlsByPartnerId($partner->id);

                return $this->render('index', [
                    'girls' => $partnerGirls,
                    'partner' => $partner
                ]);
            } else {
                return $this->render('index', [
                    'partner' => $partner
                ]);
            }
        } else {
            return $this->goHome();
        }
    }


    /**
     * View girl action
     */
    public function actionView($id)
    {
        $userType = Yii::$app->user->identity->usertypeid;


        if (!empty($userType) && $userType == 2) {
            $girlModel = new Girls();
            $partnerModel = new Partners();
            $characterModel = new Characters();
            $occupationModel = new Occupations();
            $photosModel = new Photoes();
            $filesModel = new Files();
            $girlsLanguageModel = new Girlslanguages();

            $girl = $girlModel->getGirlById($id);
            $userId = Yii::$app->user->identity->id;
            $partner = $partnerModel->getUser($userId);


            if ($girl->partnerid != $partner->id) {
                return $this->goHome();
            }

            $girl->occupationid = $occupationModel->getOccupationById($girl->occupationid);
            $girl->characterid = $characterModel->getCharacterById($girl->characterid);
            $languages = $girlsLanguageModel->getLanguageByGirlId($girl->id);
            $girl->languageid = $languages[0];
            $girl->languageid2 = $languages[1];

            $photo = $photosModel->getPhotoByGirlId($girl->userid);
            if (!empty($photo)) {
                $girl->avatarPath = $filesModel->getFileById($photo->fileid)->path;
            }

            $photos = $photosModel->getAllPhotoByGirlId($girl->userid);
            foreach ($photos as $photo) {
                $photo->filepath = $filesModel->getFileById($photo->fileid)->path;
                $pathMini = explode('/', $filesModel->getFileById($photo->fileid)->path);
                $photo->filepathmini = $pathMini[0] . '/' . $pathMini[1] . '/m_' . $pathMini[2];
            }

            return $this->render('view', [
                'model' => $girl,
                'photos' => $photos
            ]);
        } else {
            return $this->goHome();
        }
    }


    /**
     * Edit girl action
     */
    public function actionEdit($id)
    {
        $userId = Yii::$app->user->identity->id;
        $tmpDir = '../../melia.agency777.com/web/media/tmp/' . $userId;
        $userType = Yii::$app->user->identity->usertypeid;

        if (!empty($userType) && $userType == 2) {
            $girlModel = new Girls();
            $partnerModel = new Partners();
            $girlsLanguagesModel = new Girlslanguages();
            $religionsModel = new Religions();
            $languagesModel = new Languages();
            $characterModel = new Characters();
            $materialStatus = new Materialstatuses();
            $occupationModel = new Occupations();
            $photosModel = new Photoes();
            $filesModel = new Files();
            $namesModel = new Names();
            $citiesModel = new Cities();
            $fileUploadModel = new UploadForm();

            $girl = $girlModel->getGirlById($id);
            $partner = $partnerModel->getUser(Yii::$app->user->id);
            if ($girl->partnerid != $partner->id) {
                return $this->goHome();
            }


            // анкета уже подтверждена и ее нельзя уже изменять
            if ($girl->statusid == 6) {
                return $this->redirect('index');
            } else {
                if (!empty($_POST['Girls'])) {
                    $data = $_POST['Girls'];
                    $girl = $girlModel->getGirlById($data['id']);
                    $data['passportimage'] = $girl->passportimage;
                    $data['identificationvideo'] = $girl->identificationvideo;

                    $girlModel->attributes = $data;
                    $girl->attributes = $data;
                    $girl->setAttributes([
                        'statusid' => 5,
                        'is_active' => 1,
                        'is_aproved' => 0,
                        'is_accepted' => 1,
                        'partnerid' => $partner->id
                    ]);
                    if ($girl->validate()) {
                        $girl->save();

                        $girlsLanguagesModel->addGirlLanguage($girl->id, $data['languageid'], $data['languageid2']);

                        if (file_exists($tmpDir)) {
                            $filesModel->saveTmpFilesToGirl($girl->userid, $tmpDir);
                            BaseFileHelper::removeDirectory($tmpDir);
                        }

                        return $this->redirect(['addgirl/index']);
                    } else {
                        print_r($girl->getErrors());
                    }
                } else {
                    BaseFileHelper::removeDirectory($tmpDir);

                    // show girl profile
                    $religions = $religionsModel->getAllReligions();
                    $languages = $languagesModel->getAllLanguages();
                    $characters = $characterModel->getAllCharacters();
                    $statuses = $materialStatus->getAllStatuses();
                    $occupations = $occupationModel->getAllOccupations();
                    $names = $namesModel->getAllNames();
                    $cities = $citiesModel->getAllCities();

                    $photo = $photosModel->getPhotoByGirlId($girl->userid);
                    if (!empty($photo)) {
                        $girl->avatarPath = $filesModel->getFileById($photo->fileid)->path;
                    }

                    $photos = $photosModel->getAllPhotoByGirlId($girl->userid);
                    foreach ($photos as $photo) {
                        $photo->filepath = $filesModel->getFileById($photo->fileid)->path;
                        $pathMini = explode('/', $filesModel->getFileById($photo->fileid)->path);
                        $photo->filepathmini = $pathMini[0] . '/' . $pathMini[1] . '/m_' . $pathMini[2];
                    }

                    return $this->render('edit', [
                        'model' => $girl,
                        'names' => $names,
                        'cities' => $cities,
                        'photos' => $photos,
                        'fileModel' => $fileUploadModel,
                        'religions' => $religions,
                        'languages' => $languages,
                        'characters' => $characters,
                        'eyescolor' => Yii::$app->params['eyesColor'],
                        'haircolor' => Yii::$app->params['hairColor'],
                        'education' => Yii::$app->params['education'],
                        'statuses' => $statuses,
                        'occupations' => $occupations,
                    ]);
                }
            }
        } else {
            return $this->goHome();
        }
    }

    /**
     * Create new girl action by partner
     */
    public function actionCreate()
    {
        set_time_limit(0);

        $userId = Yii::$app->user->identity->id;

        $logModel = new Logs();
        $isLogs = $logModel->getLogsByUserId($userId);
        $statispagesModel = new Staticpages();

        $tmpDir = '../../melia.agency777.com/web/media/tmp/' . $userId;
        $tmpVideoDir = '../../melia.agency777.com/web/media/tmpVideo/' . $userId;
        $userType = Yii::$app->user->identity->usertypeid;

        if (!empty($userType) && $userType == 2) {
            $partnerModel = new Partners();
            $uploadFormModel = new UploadForm();
            $filesModel = new Files();
            $girlModel = new Girls();
            $girlsLanguagesModel = new Girlslanguages();
            $partner = $partnerModel->getUser(Yii::$app->user->id);

            if (!empty($_POST['Girls']) && !empty($_POST['Users'])) {
                $data = $_POST['Girls'];

                $girlModel->attributes = $data;
                if ($girlModel->validate()) {
                    $userModel = new Users();
                    $newGirlId = $userModel->addGirlUser($data['name'], $data['lastname'], $data['email']);

                    // upload passport image
                    $uploadFormModel->imageFile = UploadedFile::getInstance($uploadFormModel, 'imageFile');
                    $girlModel->passportimage = $uploadFormModel->upload(1, $newGirlId);

                    $girlModel->setAttribute('userid', $newGirlId);
                    $girlModel->setAttributes([
                        'partnerid' => $partner->id,
                        'statusid' => 5,
                        'is_active' => 1,
                        'is_aproved' => 0,
                        'is_accepted' => 1
                    ]);
                    $girlModel->save();

//                  write girl langusge
                    $girlsLanguagesModel->addGirlLanguage($girlModel->id, $data['languageid'], $data['languageid2']);

                    if (file_exists($tmpDir)) {
                        $filesModel->saveTmpFilesToGirl($newGirlId, $tmpDir);
                        BaseFileHelper::removeDirectory($tmpDir);
                    }

                    if (file_exists($tmpVideoDir)) {
                        $filesModel->saveTmpVideoToGirl($newGirlId, $tmpVideoDir);
                        BaseFileHelper::removeDirectory($tmpVideoDir);
                    }

                    return $this->redirect(['addgirl/index']);
                } else {
                    print_r($girlModel->getErrors());
                }
            } else {
                BaseFileHelper::removeDirectory($tmpDir);
                $religionsModel = new Religions();
                $languagesModel = new Languages();
                $characterModel = new Characters();
                $materialStatus = new Materialstatuses();
                $occupationModel = new Occupations();
                $userModel = new Users();
                $namesModel = new Names();
                $citiesModel = new Cities();

                $religions = $religionsModel->getAllReligions();
                $languages = $languagesModel->getAllLanguages();
                $characters = $characterModel->getAllCharacters();
                $statuses = $materialStatus->getAllStatuses();
                $occupations = $occupationModel->getAllOccupations();
                $names = $namesModel->getAllNames();
                $cities = $citiesModel->getAllCities();

                $eyesColor = [
                    'Blue' => 'Blue',
                    'Brown' => 'Brown',
                    'Green' => 'Green',
                    'Hazel' => 'Hazel',

                ];
                $hairColor = [
                    'Black' => 'Black',
                    'Blond' => 'Blond',
                    'Brown' => 'Brown',
                    'Fair' => 'Fair',
                    'Chestnut' => 'Chestnut',
                    'Red' => 'Red',
                ];
                $education = [
                    'University degree' => 'University degree',
                    'University (unfinished)' => 'University (unfinished)',
                    'Medical degree' => 'Medical degree',
                    'Student' => 'Student',
                    'College degree' => 'College degree',
                    'High school' => 'High school'
                ];

                return $this->render('create', [
                    'model' => $girlModel,
                    'names' => $names,
                    'cities' => $cities,
                    'userModel' => $userModel,
                    'partner' => $partner,
                    'fileModel' => $uploadFormModel,
                    'religions' => $religions,
                    'languages' => $languages,
                    'characters' => $characters,
                    'eyescolor' => $eyesColor,
                    'haircolor' => $hairColor,
                    'education' => $education,
                    'statuses' => $statuses,
                    'occupations' => $occupations,
                    'isLogs' => $isLogs,
                    'aboutPage' => $statispagesModel->getText('31'),
                    'lntext' => 'text_' . Yii::$app->params['language']
                ]);
            }
        } else {
            return $this->goHome();
        }
    }

    /**
     * Delete photo from girl album
     */
    public function actionDelAlbumPhoto()
    {
        $photoId = $_POST['photoId'];
        $photosModel = new Photoes();

        $photosModel->deletePhotoById($photoId);
    }

    /**
     * D&D files upload
     */
    public function actionUploadalbums()
    {
        $filesModel = new Files();
        $filesModel->saveTmpFiles($_FILES['files']['name'][0], $_FILES['files']['tmp_name'][0]);
    }


    /**
     * D&D video upload
     */
    public function actionUploadvideo()
    {
        $filesModel = new Files();
        $filesModel->saveTmpVideo($_FILES['files']['name'], $_FILES['files']['tmp_name']);
    }

    /**
     * Check emails
     */
    public function actionCheckemail()
    {
        $usersModel = new Users();
        $email = Yii::$app->request->post('email');

        $user = $usersModel->checkEmail($email);
        if (!empty($user)) {
            echo json_encode('Email ' . $email . ' is already in used');
        }
    }

    /**
     * Check username
     */
    public function actionCheckusername()
    {
        $usersModel = new Users();
        $username = Yii::$app->request->post('username');

        $user = $usersModel->checkUsername($username);
        if (!empty($user)) {
            echo json_encode('Username ' . $username . ' is already in used');
        }
    }

    /**
     * Write logs
     */
    public function actionLog()
    {
        $userType = Yii::$app->user->identity->usertypeid;
        if (!empty($userType) && $userType == 2) {
            $logModel = new Logs();
            $logModel->addLog();
            return true;
        } else {
            return $this->goHome();
        }
    }
}
