<?php

namespace app\controllers;


use app\components\Curl;
use Yii;
use app\models\ChatTopDates;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TopdatesController implements the CRUD actions for ChatTopDates model.
 */
class TopdatesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->layout = 'admin';
            return true;
        }
        return false;
    }

    public function actionIndex()
    {
        $chatModel = new ChatTopDates();
        $girls = $chatModel->getGirl();


        return $this->render('index', [

            'girls' => $girls,
        ]);
    }

    public function actionGirlmessage($id)
    {
        $chatModel = new ChatTopDates();
        $messages = $chatModel->getMessageFromGirl($id);
        return $this->render('message', [
            'messages' => $messages,
        ]);
    }

    public function actionUpdate($id)
    {


        $chatModel = new ChatTopDates();
        $model = $this->findModel($id);

        if($model->is_men_letter == 1){
            $model->is_men_letter = 0;
            $model->text = '';

        }

        $girl_id = $model->girl_id;
        $men_id = $model->men_id;



        $message = $chatModel->getMessageMen($girl_id, $men_id);
        if ($model->load(Yii::$app->request->post())) {
            $chatModel->answerAllMessages($men_id, $girl_id);

            $model->is_aproved = 1;
            $text = $model->text;
            $model->save();

            Curl::sendMessage($girl_id, $men_id, $text);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'message' => $message,
            ]);
        }
    }


    /**
     * Deletes an existing ChatTopDates model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ChatTopDates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ChatTopDates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ChatTopDates::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
