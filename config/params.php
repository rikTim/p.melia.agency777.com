<?php

return [
    'adminEmail' => 'admin@example.com',
    'siteName' => 'MeliaPartner.com',
    'language' => 'ru',


    'loginTopDates' => '',
    'passTopDates' => '',

    //время для выборки id девушки по активности
//    'TimeLimit' => '600',
    'TimeLimit' => '84600',
    //время жизни relations
    'TimeLimitRelations' => '600',

    'WorkerCount' => '2',
    'Yandex' => [
        'counter_id' => '',
        'oauth_id' => '',
        'oauth_password' => '',
        'user_login' => '',
        'user_password' => '',
    ],


    // girl profile params
    'eyesColor' => [
        'Blue' => 'Blue',
        'Brown' => 'Brown',
        'Green' => 'Green',
        'Hazel' => 'Hazel',

    ],
    'hairColor' => [
        'Black' => 'Black',
        'Blond' => 'Blond',
        'Brown' => 'Brown',
        'Fair' => 'Fair',
        'Chestnut' => 'Chestnut',
        'Red' => 'Red',
    ],
    'education' => [
        'University degree' => 'University degree',
        'University (unfinished)' => 'University (unfinished)',
        'Medical degree' => 'Medical degree',
        'Student' => 'Student',
        'College degree' => 'College degree',
        'High school' => 'High school'
    ],

    'payMoney' => [
        'Western Union' => 'Western Union',
        'Visa' => 'Visa',
        'Money Gram' => 'Money Gram',
    ],



];
