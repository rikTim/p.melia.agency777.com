<?php

namespace app\components;
use yii\base\Component;
use Yii;

class langs extends Component
{
    public function init()
    {
        $lang = $_COOKIE['lang'];
        if (empty($lang)) {
            $id = $_SERVER['REMOTE_ADDR'];

            if ($id == '::1') {
                $id = '64.233.160.0'; // если заходим с локальной машины, присваемваем id адрес google
            }

            $langArr = yii::$app->geolocation->getInfo($id);
            $countryCode = $langArr['geoplugin_countryCode'];

            $ruLangCountries = ['AZ', 'AM', 'BY', 'GE', 'KZ', 'LT', 'MD', 'RU', 'TJ', 'TM', 'UZ', 'UA', 'EE'];
            $esLangCountries = ['AR', 'BO', 'VE', 'HT', 'GT', 'HN', 'DO', 'CO', 'CR', 'CU', 'MX', 'NI', 'PA', 'PY', 'PE', 'UY', 'CL', 'EC', 'PR', 'ES'];

            $resRu = array_search($countryCode, $ruLangCountries);
            $resEs = array_search($countryCode, $esLangCountries);

            if (!empty($resRu)) {
                setcookie("lang", 'Ru', time()+60*60*24*30, '/');
                Yii::$app->params['language'] = 'ru';
//            } else if (!empty($resEs)) {
//                setcookie("lang", 'Es', time()+60*60*24*30, '/');
//                Yii::$app->params['language'] = 'es';
            } else if ($countryCode == 'CZ') {
                setcookie("lang", 'Cz',time()+60*60*24*30, '/');
                Yii::$app->params['language'] = 'cz';
            } else {
                setcookie("lang", 'En',time()+60*60*24*30, '/');
                Yii::$app->params['language'] = 'en';
            }

        }
        parent::init();
    }
}
