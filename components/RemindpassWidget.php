<?php

namespace app\components;

use Yii;
use yii\base\Widget;
use app\models\Users;

class RemindpassWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $data = Yii::$app->request->post('Users');
        $username = Yii::$app->request->get('username');
        $email = Yii::$app->request->get('email');
        $token = Yii::$app->request->get('token');

        if (!empty($data) && !empty($username) && !empty($email) && !empty($token)) {
            $userModel = new Users();
            $res = $userModel->changePass($data, $username, $email, $token);

            if ($res == 1) {
                $userModel->authUser($username, $data['password']);
                header('Location: http://meliapartner.com/');
            } else {
                echo 'error!!';
            }

            die();
        } else {
            return $this->render('remindPass', [
                'model' => new Users()
            ]);
        }
    }
}
