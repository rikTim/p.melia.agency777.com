<?php

namespace app\components;

use Yii;
use yii\base\Widget;
use app\models\Balances;
use app\models\Messages;


class MenuWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $user = explode(' ', Yii::$app->user->identity->username)[0];


        return $this->render('menu', [
            'user' => $user,
            'total' => $this->totalBalance(),
            'unreadedmessages' => $this->unreadedMessages()
        ]);
    }

    public function totalBalance()
    {
        $userId = Yii::$app->user->id;
        $balancesModel = new Balances();
        $balance = $balancesModel->getPartnerBalance($userId);

        return !empty($balance) ? $balance : 0;
    }

    public function unreadedMessages()
    {
        $userId = Yii::$app->user->identity->id;
        $messagesModel = new Messages();
        return $messagesModel->getCountOfUnreadedMessagesFromAdmin($userId);
    }
}