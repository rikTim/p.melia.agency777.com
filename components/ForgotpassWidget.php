<?php

namespace app\components;

use Yii;
use yii\base\Widget;
use app\models\LoginForm;
use app\models\Users;

class ForgotpassWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $data = Yii::$app->request->post('LoginForm');
        if (!empty($data)) {
            $check = Users::checkUser($data['username'], $data['email']);
        } else {
            unset($check);
        }

        $model = new LoginForm();
        return $this->render('forgotPass', [
            'model' => $model,
            'message' => $check
        ]);
    }
}