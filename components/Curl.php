<?php


namespace app\components;

use app\models\ChatTopDates;
use app\models\Girls;
use app\models\Men;
use Yii;

class Curl
{
    public static function  getCurl($url, $postData = '')
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_ENCODING, '');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE,'topdates.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR,'topdates.txt');
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
        if ($postData != '') {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        }
        $out = curl_exec($curl);
        curl_close($curl);

        return $out;
    }


    public static function getViewstate($out, $text = '', $id = '', $forDelivered = false)
    {

        $user = Yii::$app->params['loginTopDates'];
        $pass = Yii::$app->params['passTopDates'];


        if (preg_match_all("/id=\"__VIEWSTATE\" value=\"(.*?)\"/", $out, $arr_viewstate)) {
            $res['viewstate'] = urlencode($arr_viewstate[1][0]);
        }

        if (preg_match_all("/id=\"__VIEWSTATEGENERATOR\" value=\"(.*?)\"/", $out, $arr_viewstate)) {
            $res['viewstateGenerator'] = urlencode($arr_viewstate[1][0]);
        }
        $postDataArr = [
            '__EVENTTARGET=',
            '__EVENTARGUMENT=',
            '__VIEWSTATE=' . $res['viewstate'],
            '__VIEWSTATEGENERATOR=' . $res['viewstateGenerator'],
            'ctl00$ContentPlaceHolder1$txtBoxLogin=' . $user,
            'ctl00$ContentPlaceHolder1$txtBoxPWD=' . $pass,
            'ctl00$ContentPlaceHolder1$btnLogin=Enter'
        ];

        if (preg_match_all("/id=\"__LASTFOCUS\" value=\"(.*?)\"/", $out, $arr_viewstate)) {
            $res['csrfToken'] = urlencode($arr_viewstate[1][0]);
            $postDataArr[] = '__LASTFOCUS=' . $res['csrfToken'];
        }

        if (preg_match_all("/id=\"__VIEWSTATEFIELDCOUNT\" value=\"(.*?)\"/", $out, $arr_viewstate)) {
            $res['csrfToken'] = urlencode($arr_viewstate[1][0]);
            $postDataArr[] = '__VIEWSTATEFIELDCOUNT=' . $res['csrfToken'];
        }

        if (preg_match_all("/id=\"__CSRFTOKEN\" value=\"(.*?)\"/", $out, $arr_viewstate)) {
            $res['csrfToken'] = urlencode($arr_viewstate[1][0]);
            $postDataArr[] = '__CSRFTOKEN=' . $res['csrfToken'];
        }
        if ($text != '') {
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$hdnPhotosReply=';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$btnLetterReplySend=Send';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$txtBoxLetterText=' . $text;
            $postDataArr[] = 'ctl00_ctl00_ctl00_ContentPlaceHolder1_nestedContentPlaceHolder_ContentIndex_cntrlViewLadyCorrespondence_scriptManager_HiddenField=';
        }

        if ($forDelivered == true) {
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl00$cntrlLetterInfo$btnChkBox=';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl00$cntrlLetterInfo$chkBoxManLetterDeliveryConfirm=on';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl00$cntrlLetterInfo$hdnDeclinedLetterText=';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl00$cntrlLetterInfo$hdnPhotosReply=';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl00$cntrlLetterInfo$hdnTranslate=';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl00$cntrlLetterInfo$txtBoxLetterText=';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl01$cntrlLetterInfo$hdnDeclinedLetterText=';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl01$cntrlLetterInfo$hdnPhotosReply=';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl01$cntrlLetterInfo$hdnTranslate=';
            $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl01$cntrlLetterInfo$txtBoxLetterText=';
            $postDataArr[] = 'ctl00_ctl00_ctl00_ContentPlaceHolder1_nestedContentPlaceHolder_ContentIndex_cntrlViewLadyCorrespondence_scriptManager_HiddenField=';
            if ($id != '') {
                $postDataArr[] = 'ctl00$ctl00$ctl00$ContentPlaceHolder1$nestedContentPlaceHolder$ContentIndex$cntrlViewLadyCorrespondence$rptLadyCorrespondence$ctl00$cntrlLetterInfo$mailAttachmentsInfo1$lstAttachments$ctl00$hdnAttachmentID=' . $id;
            }
        }
        return implode('&', $postDataArr);
    }

    public static function login()
    {
        $url = 'http://top-dates.net/index/Login.aspx';

        $out = Curl::getCurl($url);
        $postData = Curl::getViewstate($out);

        $out = Curl::getCurl($url, $postData);

        return $out;
    }

    // универсальный метод поиска в html позиции входжения необходимого елемента
    public static function Positions($out, $needle)
    {
        $lastPos = 0;
        $positions = array();

        while (($lastPos = strpos($out, $needle, $lastPos)) !== false) {
            $positions[] = $lastPos;
            $lastPos = $lastPos + strlen($needle);
        }
        return $positions;
    }

    // розбираем html и собираем массив одного сообщения
    public static function Substr($data, $girl,$men, $menProfile= '', $image = '')
    {

        //Время
        if(preg_match_all('|<td.+class="usual".+align="right"+>\r?\n+(.*).+nbsp;\r?\n.+textGrey">(.*)<|m', $data, $Arrdate)) {
            $datetime = $Arrdate[1][0] . ' ' . $Arrdate[2][0];
            $datetime = strtotime($datetime);
        }
        //  Текст
        $needle = 'colspan="2">';
        $Arr_date = Curl::Positions($data, $needle);
        $Arr = $Arr_date[1];
        $text = substr($data, $Arr);
        if(preg_match_all('|colspan="2">\r?\n+(.*)|', $text, $Arrtext)){
            $text = $Arrtext[1][0];
            $text = ltrim($text);
        }


        if(preg_match_all('|<td>.+.</td>\r?\n<td>+(.*)</td>|', $menProfile,$arrMan)){
            $name = $arrMan[1][0];
            $height = $arrMan[1][2];
            $weight = $arrMan[1][3];
            $age  =$arrMan[1][1];
        }


        //Ставим не прочитано, от мужика, проверено
        $chat = new ChatTopDates();
        $menModel = new Men();



        $chat->text = $text;
        $chat->men_id = $men;
        $chat->girl_id = $girl;
        $chat->men_name = $name;
        $chat->is_readed = 0;
        $chat->is_aproved = 1;
        $chat->is_notification = 0;
        $chat->is_men_letter = 1;
        $chat->datetime = $datetime;
        $chat->image = $image;
        $chat->save();

        $menModel->topdatesid = $men;
        $menModel->avatar = $image;
        $menModel->name = $arrMan[1][0];
        $menModel->age = $age;
        $menModel->height = $height;
        $menModel->weight = $weight;
        $menModel->eyes_color = $arrMan[1][4];
        $menModel->hair_color = $arrMan[1][5];
        $menModel->education = $arrMan[1][6];
        $menModel->language = $arrMan[1][7];
        $menModel->profesion = $arrMan[1][8];
        $menModel->city = $arrMan[1][9];
        $menModel->education_state = $arrMan[1][10];
        $menModel->country = $arrMan[1][11];
        $menModel->religion = $arrMan[1][12];
        $menModel->material_status = $arrMan[1][13];
        $menModel->smoker = $arrMan[1][14];
        $menModel->alcohol = $arrMan[1][15];

        $menModel->save();
    }

// по id девушек собираем  пару girl_id men_id
    public static function getIdMen($girl)
    {
        $Page = 1;
        $TimeLimit = Yii::$app->params['TimeLimit'];
        $time = time() - $TimeLimit;
        $Id_array = [];

        while (true) {
            $url = 'http://top-dates.net/Mail/GirlsCorrespondenceView.aspx?ladyID=' . $girl . '&groupByMan=1&pageNum=' . $Page;
            $out = Curl::getCurl($url);

            if(preg_match_all('/Date_Inbox".+ladyID=' . $girl . '.+manID=([0-9]+)">(.+)....+</', $out, $Arr)) {
                $men = $Arr[1];
                $dates = $Arr[2];
            }
            $limit = 0;
            if(!empty($dates)) {
                foreach ($dates as $date) {
                    $id = $men[$limit];
                    $date = strtotime($date);

//    проверка на активность
                    if ($date < $time) {
                        break;
                    }
                    $Id_array[] = $id;
                    $limit++;
                }
            }else
                break;
            $count = count($men);
            if ($count % 10 == 0) {
                $Page++;
            } else break;
        }
        return $Id_array;
    }



// сбор сообщений по  Relation с не нажатой  Delivered
    public static function getMessage($girl, $men)
    {
        $url = 'http://top-dates.net/index/ViewLadyCorrespondence.aspx?ladyID=' . $girl . '&manID=' . $men;
        $out = Curl::getCurl($url);

        //парсим имя мужика с кем диалог

        if(preg_match_all('|_cntrlViewLadyCorrespondence_cntMan_imgManThumbnail".+src="(.*)".+alt=|m',$out,$Arrmen)){
            $image = $Arrmen[1][0];
        }

        $needleMen = '<table cellspacing="0" border="0" class="text">';
        $positionsMen = Curl::Positions($out, $needleMen);


        $needleMen2 = '<input type="hidden" id="hdnCurrentImageIndex" value="0" />';
        $positionsMen2 = Curl::Positions($out, $needleMen2);
        $length = $positionsMen2[0] - $positionsMen[0] ;
        $menProfile = substr($out,$positionsMen[0], $length);

        //ищем позиции начала диалогов
        $needle1 = 'class="compose-mail">';
        $positions1 = Curl::Positions($out, $needle1);

        //позиции конца диалогов
        $needle2 = '_cntrlLetterInfo_trLetterOptions';
        $positions2 = Curl::Positions($out, $needle2);

        //количество сообщений в текущем диалоге
        $count = count($positions2);


        //розбиваем html страницу по диалогам для подальшего парсинга
        $Arr_dialogs = [];


        for ($i = 0; $i < $count; $i++) {
            $length = $positions2[$i] - $positions1[$i];

            $Arr_dialogs[] = substr($out, $positions1[$i], $length);

        }


        //Розбираем массив по отдельним сообщениям
        foreach ($Arr_dialogs as $Arr_dialog) {

            //проверяем присутствует не нажатый флажок Delivered
            if (preg_match_all('/_pnlManLetterDeliveryOptions/', $Arr_dialog,$deliv)) {
                //возвращем готовый массив одного сообщения

                Curl::Substr($Arr_dialog, $girl,$men, $menProfile, $image);

                //вибираем id сообщения для установки флажка Delivered
                if(preg_match_all('/_hdnAttachmentID" value="([0-9]+)"/', $Arr_dialog, $id_letter)) {
                    $idletter = $id_letter[1];
                }
                // возможно что id сообщения нету когда первое сообщение в диалоге от мужика
                if (!empty($idletter)) {
                    $id = $id_letter[1][0];
                } else {
                    $id = '';
                }


                //собираем postData  для нажатия флажка и отправляем запрос на его нажатие
                $forDelivered = true;
                $postData = Curl::getViewstate($out,$text ='',$id,$forDelivered);
                $out = Curl::getCurl($url, $postData);
            }

        }
    }




// сбор id девушек
    public static function getIdGirls()
    {

        $Page = 1;
        $TimeLimit = Yii::$app->params['TimeLimit'];

        $time = time() - $TimeLimit;
        $Id_array = [];

        while (true) {

            $url = 'http://top-dates.net/Mail/GirlsCorrespondenceView.aspx?pageNum=' . $Page;
            $out = Curl::getCurl($url);

            //парсим дату и id девушки
            if(preg_match_all('/<a.+Date_Inbox".+ladyID=([0-9]+)&.+>(.+)....+</', $out, $Arr)) {
                $girls = $Arr[1];
                $dates = $Arr[2];
            }
            $limit = 0;
            //перебираем массив id
            if(!empty($dates)) {
                foreach ($dates as $date) {
                    $id = $girls[$limit];
                    $date = strtotime($date);

//            //проверка на дату активности
                    if ($date < $time) {
                        break;
                    }
                    $Id_array[] = $id;
                    $limit++;

                }
            }
            else{
                break;
            }
            $Id_array = $girls;

            $count = count($girls);

            if ($count % 10 == 0) {

                $Page++;

            } else break;
        }
        return $Id_array;
    }

// отправка сообщения
    public static function sendMessage($girl, $men, $text)
    {
        $url = 'http://top-dates.net/index/ViewLadyCorrespondence.aspx?ladyID=' . $girl . '&manID=' . $men;
        $out = Curl::getCurl($url);
        $postData = Curl::getViewstate($out, $text);
        Curl::getCurl($url, $postData);
    }

    public function getGirlDb()
    {
        $girlModel = new Girls();
        $girls = $girlModel->find()->all();
        $id_girls = [];
        foreach($girls as $girl){
            $Arr_girl = [];
            $topdatespass = $girl->topdatespass;
            if(!empty($topdatespass)) {
                $Arr_girl['topdatesid'] = $girl->topdatesid;
                $Arr_girl['topdatespass'] = $girl->topdatespass;
                $id_girls[] = $Arr_girl;

            }
        }
    }
}