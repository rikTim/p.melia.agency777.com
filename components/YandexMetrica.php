<?php

namespace app\components;

use yii\base\Component;
use yii\helpers\Json;

/**
 * YandexMetrica
 *
 * Класс для работы с API Яндекс Метрики
 *
 * @author Vladislav Pozdnyakov <scary_donetskiy@live.com>
 * @version 1.0
 */
class YandexMetrica extends Component
{
    private $oauthToken;

    /**
     * Конструктор класса
     *
     * Получает OAuth токен для последующей работы
     *
     * @param array $config Массив с параметрами приложения
     */
    public function __construct($config)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://oauth.yandex.ru/token');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=password&username=' . $config['user_login'] .
            '&password=' . $config['user_password'] .
            '&client_id=' . $config['oauth_id'] .
            '&client_secret=' . $config['oauth_password']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $token = curl_exec($ch);
        curl_close($ch);

        $this->oauthToken = Json::decode($token)['access_token'];
    }

    /**
     * curlExecute
     *
     * Выполняет cURL запрос по заданному адрессу
     *
     * @param string $url URL запроса
     * @return string Ответ
     */
    private function curlExecute($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    /**
     * trafficSummary
     *
     * Возвращает данные о посещаемости сайта.
     *
     * @param string $counterId ID счетчика
     * @return string JSON ответ
     */
    public function trafficSummary($counterId)
    {
        return $this->curlExecute('https://api-metrika.yandex.ru/stat/traffic/summary.json?id=' . $counterId . '&oauth_token=' . $this->oauthToken);
    }

    /**
     * trafficDeepness
     *
     * Возвращает данные о количестве просмотренных страниц и времени, проведенном посетителями на сайте.
     *
     * @param string $counterId ID счетчика
     * @return string JSON ответ
     */
    public function trafficDeepness($counterId)
    {
        return $this->curlExecute('https://api-metrika.yandex.ru/stat/traffic/deepness.json?id=' . $counterId . '&oauth_token=' . $this->oauthToken);
    }

    /**
     * trafficHourly
     *
     * Возвращает данные о распределении трафика на сайте по времени суток, за каждый часовой период.
     *
     * @param string $counterId ID счетчика
     * @return string JSON ответ
     */
    public function trafficHourly($counterId)
    {
        return $this->curlExecute('https://api-metrika.yandex.ru/stat/traffic/hourly.json?id=' . $counterId . '&oauth_token=' . $this->oauthToken);
    }

    /**
     * trafficLoad
     *
     * Возвращает максимальное количество запросов (срабатываний счетчика) в секунду и максимальное количество посетителей онлайн за каждый день выбранного периода времени.
     *
     * @param string $counterId ID счетчика
     * @return string JSON ответ
     */
    public function trafficLoad($counterId)
    {
        return $this->curlExecute('https://api-metrika.yandex.ru/stat/traffic/load.json?id=' . $counterId . '&oauth_token=' . $this->oauthToken);
    }

    /**
     * geo
     *
     * Возвращает данные о принадлежности посетителей к географическим регионам.
     *
     * @param string $counterId ID счетчика
     * @return string JSON ответ
     */
    public function geo($counterId)
    {
        return $this->curlExecute('https://api-metrika.yandex.ru/stat/geo.json?id=' . $counterId . '&oauth_token=' . $this->oauthToken);
    }
}
