<?php
namespace app\components;
use Yii;

class access extends \yii\base\Component
{
    public function init()
    {
        $hash = md5('access' . 'agency'); //9d55760a0385b65741a0f849fef81f55
        $denied = md5('denied' . 'agency'); //9b8054ec7f9a1fc95d0c7c0c4219be5a
        $accept = md5('accept' . 'agency'); //1ed0f5e29d5ad1ec4feca6ce316f5e00

        $access = $_COOKIE[$hash];

        if (strpos($_SERVER['REQUEST_URI'], 'agency')) {
            return true;
        }
        elseif (empty($access)) {
            $id = $_SERVER['REMOTE_ADDR'];
            if ($id == '::1') {
                $id = '64.233.160.0'; // если заходим с локальной машины, присваемваем id адрес google
            }

            $country = yii::$app->geolocation->getInfo($id);
            $countryCode = $country['geoplugin_countryCode'];

            $deniedCountries = ['US', 'UA', 'RU'];

            if (in_array($countryCode, $deniedCountries)) {
                setcookie($hash, $denied, time()+60*60*24*30, '/');
                header('Location: http://meliapartner.com/web/denied');
            } else {
                setcookie($hash, $accept, time()+60*60*24*30, '/');
            }
        }
        elseif (strpos($_SERVER['REQUEST_URI'], 'denied') && $access == $denied) {
            return true;
        }
        elseif ($access == $denied) {
           header('Location: http://meliapartner.com/web/denied');
        }


        parent::init();
    }
}
