<?php

namespace app\components;

use app\models\Contacts;
use Yii;
use yii\base\Widget;

class PopupContactsWidget extends Widget
{
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }

    public function init()
    {
        parent::init();
    }

    public function run()
    {
     $lang = $_COOKIE['lang'];

        $reasons = [
            1 =>Yii::t('app','Question'),
            2 => Yii::t('app','Сrash'),
            3 => Yii::t('app','Other')
        ];



        return $this->render('popupContacts', [
            'model' => new Contacts(),
            'reasons' => $reasons,
        ]);
    }

}
