<?php
use yii\helpers\Url;
?>
<section class="well__05">
    <div class="container sectionPadding">
        <nav>
            <ul>
                <li class="menu-item"><a href="<?= Url::toRoute(['site/main']); ?>"><?= Yii::t('app','Main')?></a></li>
                <li class="menu-item"><a href="<?= Url::toRoute(['profile/index']); ?>"><?= Yii::t('app','Profile')?></a></li>
<!--                <li class="menu-item"><a href="--><?//= Url::toRoute(['site/messages']); ?><!--">--><?//= Yii::t('app','Messages')?><!--</a></li>-->
                <li class="menu-item">
                    <a href="<?= Url::toRoute(['/contact']); ?>">
                        <?= Yii::t('app','Contacts');?>
                        <?php if($unreadedmessages != 0): ?>
                            <span class="contacts-count"><?= $unreadedmessages; ?></span>
                        <?php endif; ?>
                    </a>
                </li>
                <li class="menu-item"><a href="<?= Url::toRoute(['addgirl/index']); ?>"><?= Yii::t('app','Register Women')?></a></li>
                <li class="menu-item"><a href="<?= Url::toRoute(['site/about']); ?>"><?= Yii::t('app','About agency')?></a></li>
                <li class="menu-item"><a href="<?= Url::toRoute(['site/training']); ?>"><?= Yii::t('app','Training')?></a></li>
                <li class="menu-item balance"><a href="<?= Url::toRoute(['site/balance']); ?>"><i class="fa fa-money"></i> <?= $total; ?></a></li>
                <li class="menu-item"><a href="<?= Url::toRoute(['site/out']); ?>"><?= Yii::t('app','Logout')?> ( <?= $user; ?> )</a></li>



                <li class="lang-select"><span class="selected-lang"></span>
                    <ul>
<!--                        <li>Es</li>-->
                        <li>En</li><li>Ru</li><li>Cz</li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</section>