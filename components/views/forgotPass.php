<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<main>
    <section class="well__05">
        <div class="container sectionPadding">
            <h2>Forgot the password</h2>
        </div>
    </section>


    <section class="well center well__06 bg01 shadow m-height sectionPadding">
        <div class="container">
            <div class="row">
                <?php $form = ActiveForm::begin([
                    'id' => 'forgetPass',
                    'options' => ['class' => 'booking-form']]);?>
                <div class="tmInput">
                    <?= $form->field($model, 'username')->textInput(['placeholder' => Yii::t('app','Login')])->label();?>
                </div>

                <div class="tmInput">
                    <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app','Email')])->label(); ?>
                </div>

                <?php if(!empty($message)): ?>
                    <p style="color:#f00;"><?= $message ?></p>
                <?php endif; ?>

                <?= Html::submitButton('Forgot password', ['class' => 'btn', 'name' => 'forgotpass-button']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>
</main>