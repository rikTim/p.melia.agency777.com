<?php use yii\helpers\Url; ?>

<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li>
                <a href="<?= Url::toRoute(['admin/']); ?>/"><i class="fa fa-home"></i> <span>Main</span></a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['girls/index?sort=statusid']); ?>">
                    <i class="fa fa-female"></i>
                    <span>Girls<span class="label pull-right bg-yellow"><?= $newGirls; ?></span></span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['partners/index?sort=statusid']); ?>">
                    <i class="fa fa-male"></i>
                    <span>Partners<span class="label pull-right bg-yellow"><?= $newPartners; ?></span></span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['admin/finance']); ?>"><i class="fa fa-money"></i><span>Finance</span></a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['admin/balances']); ?>"><i class="fa fa-money"></i><span>Balances</span></a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['admin/reply']); ?>">
                    <i class="fa fa-envelope"></i><span>Answer Me</span>
                    <small class="label label-primary pull-right"><?= $newAnswerMe; ?></small>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['admin/refuse']); ?>">
                    <i class="fa fa-envelope"></i><span>Say goodbye</span>
                    <small class="label label-primary pull-right"><?= $newRefuse; ?></small>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['admin/contacts?sort=-datetime']); ?>">
                    <i class="fa fa-envelope"></i><span>Contacts</span>
                    <small class="label pull-right bg-green"><?= $newContactsMessage; ?></small>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['topdates/index']); ?>">
                    <i class="fa fa-envelope"></i><span>Top-Dates Messages</span>
                    <small class="label pull-right bg-red"><?= $newMessagesTopDates ?></small>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['admin/noanswered']); ?>">
                    <i class="fa fa-envelope"></i><span>No answered messages</span>
                    <small class="label pull-right bg-red"><?= $noAnsweredMessages ?></small>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['admin/girlmessages']); ?>">
                    <i class="fa fa-envelope"></i><span>Messages from girls</span>
                    <small class="label pull-right bg-green"><?= $newMessagesFromGirls; ?></small>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['admin/partmessages']); ?>">
                    <i class="fa fa-envelope"></i><span>Messages from partners</span>
                    <small class="label pull-right bg-green"><?= $newMessagesFromPartners; ?></small>
                </a>
            </li>
<!--            <li>-->
<!--                <a href="--><?//= Url::toRoute(['admin/girlslist']); ?><!--">-->
<!--                    <i class="fa fa-envelope"></i><span>Messages for autoanswer</span>-->
<!--                    <small class="label pull-right bg-green"></small>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="--><?//= Url::toRoute(['admin/statistic']); ?><!--">-->
<!--                    <i class="fa fa-cogs"></i><span>Statistic</span>-->
<!--                </a>-->
<!--            </li>-->
            <li>
                <a href="<?= Url::toRoute(['admin/ipaddress']); ?>">
                    <i class="fa fa-cogs"></i><span>Ip address</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['admin/metrica']) ?>">
                    <i class="fa fa-bar-chart"></i><span>Metrica</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['giftpages/index']); ?>">
                    <i class="fa fa-newspaper-o"></i><span>News</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['admin/staticpages']); ?>">
                    <i class="fa fa-file-text-o"></i><span>Static pages</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['admin/mainsettings']); ?>">
                    <i class="fa fa-cogs"></i><span>Main settings</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
