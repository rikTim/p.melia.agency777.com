<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
?>
<?php Yii::$app->language = $_COOKIE['lang']; ?>

<div class="preffix_4 grid_4 contactpopup">
    <?php $form = ActiveForm::begin([
        'action' => Url::toRoute('/site/contacts'),
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['placeholder' => Yii::t('app','Name')])->label(false)?>

    <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app','Email')])->label(false)?>

    <?= $form->field($model, 'text')->textArea(['placeholder'=> Yii::t('app','Message')])->label(false)?>

    <?= $form->field($model, 'reason')->dropDownList($reasons)->label(false); ?>

    <?= $form->field($model, 'captcha')->label(false)->widget(Captcha::className()); ?>

    <center>
        <?= Html::submitButton(Yii::t('app','Submit'), ['class' => 'btn btn-success btn-block']) ?>
    </center>

    <?php ActiveForm::end(); ?>
</div>