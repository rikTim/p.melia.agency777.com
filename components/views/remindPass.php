<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<main>
    <section class="well__05">
        <div class="container sectionPadding">
            <h2>Forgot the password</h2>
        </div>
    </section>


    <section class="well center well__06 bg01 shadow m-height sectionPadding">
        <div class="container">
            <div class="row">
                <?php $form = ActiveForm::begin([
                    'id' => 'remindPass',
                    'options' => ['class' => 'booking-form']]);?>
                <div class="tmInput">
                    <?= $form->field($model, 'password')->textInput(['placeholder' => 'Password'])->label();?>
                </div>

                <div class="tmInput">
                    <?= $form->field($model, 'rpassword')->textInput(['placeholder' => 'Repeat password'])->label(); ?>
                </div>

                <?= Html::submitButton('Remind password', ['class' => 'btn', 'name' => 'remindpass-button']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>
</main>