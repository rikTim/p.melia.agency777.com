<?php

namespace app\components;

use app\models\ChatTopDates;
use app\models\Contacts;
use app\models\Reply;
use Yii;
use yii\base\Widget;
use app\models\Girls;
use app\models\Partners;
use app\models\Messages;


class AsideWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('aside', [
            'newPartners' => $this->newPartners(),
            'newGirls' => $this->newGirls(),
            'newMessagesFromPartners' => $this->newMessagesFromPartners(),
            'newMessagesFromGirls' => $this->newMessagesFromGirls(),
            'newMessagesTopDates' => $this->newMessagesTopDates(),
            'newContactsMessage' => $this->newContactsMessage(),
            'newAnswerMe' => $this->newAnswerMe(),
            'newRefuse' => $this->newRefuse(),
            'noAnsweredMessages' => $this->getNoAnsweredMessages()
        ]);
    }

    public function newPartners()
    {
        $partnerModel = new Partners();
        $count = count($partnerModel->getNewPartner());
        return $count > 0 ? $count : '';
    }

    public function newGirls()
    {
        $girlModel = new Girls();
        $count = count($girlModel->getNewGirl());
        return $count > 0 ? $count : '';
    }

    public function newMessagesFromPartners()
    {
        $messageModel = new Messages();
        $count = $messageModel->getCountMessageToAdmin(2);
        return $count > 0 ? $count : '';
    }

    public function newMessagesFromGirls()
    {
        $messageModel = new Messages();
        $count = $messageModel->getCountMessageToAdmin(3);
        return $count > 0 ? $count : '';
    }

    public function newMessagesTopDates()
    {
        $messageModel = new ChatTopDates();
        $count = $messageModel->getCountMessage();
        return $count > 0 ? $count : '';
    }

    public function newContactsMessage()
    {
        $contactsModel = new Contacts();
        $count = $contactsModel->getCountMessage();
        return $count > 0 ? $count : '';

    }

    public function newAnswerMe()
    {
        $replyModel = new Reply();
        $chatModel = new ChatTopDates();
        $reply = $replyModel->getAnswerMe();

        foreach ($reply as $rep) {
            $girl_id = $rep->girl_id;
            $men_id = $rep->men_id;
            $chat = $chatModel->find()->where(['girl_id' => $girl_id, 'men_id' => $men_id])->orderBy('datetime DESC')->one();
            $is_men_letter = $chat->is_men_letter;
            if ($is_men_letter == 1) {
                $answer_me[] = $rep;
            }
        }
        $count = count($answer_me);
        return $count > 0 ? $count : '';
    }

    public function newRefuse()
    {
        $replyModel = new Reply();
        $chatModel = new ChatTopDates();
        $reply = $replyModel->getRefuse();
        foreach ($reply as $rep) {
            $girl_id = $rep->girl_id;
            $men_id = $rep->men_id;
            $chat = $chatModel->find()->where(['girl_id' => $girl_id, 'men_id' => $men_id])->orderBy('datetime DESC')->one();
            $is_men_letter = $chat->is_men_letter;
            if ($is_men_letter == 1) {
                $answer_me[] = $rep;
            }
        }
        $count = count($answer_me);
        return $count > 0 ? $count : '';
    }


    public function getNoAnsweredMessages()
    {
        $chatModel = new ChatTopDates();
        $count = $chatModel->find()->where(['is_answer' => 0, 'is_men_letter' => 1])->count();


        return $count > 0 ? $count : '';
    }




























}