<?php
namespace app\components;

use Yii;
use yii\base\Component;
use app\models\Balances;

class DLL extends Component
{
    public $content;

    public function init()
    {
        parent::init();
    }

    public function display($content = null)
    {
        if ($content != null) {
            $this->content = $content;
        }
        echo $this->content;
    }

    public function inMultiarray($elem, $arrays)
    {
        foreach ($arrays as $array) {
            foreach ($array as $item) {
                if ($item == $elem) {
                    return true;
                }
            }
        }
        return false;
    }

    public function bubbleSort($array)
    {
        $len = count($array);
        for ($j = 0; $j < $len - 1; $j++) {
            for ($i = 0; $i < $len - $j - 1; $i++) {
                if ($array[$i]['registerdate'] > $array[$i + 1]['registerdate']) {
                    $tmp = $array[$i];
                    $array[$i] = $array[$i + 1];
                    $array[$i + 1] = $tmp;
                }
            }
        }
        return $array;
    }

    public function sendEmail($text, $subject, $to = 'agencykievodessa@gmail.com ', $from = 'admin@meliapartner.com')
    {
        $res = Yii::$app->mail
            ->compose('auth', ['text' => $text])
            ->setFrom([$from => 'Meliapartner'])
            ->setTo($to)
            ->setSubject($subject)
            ->send();

        return $res;
    }

    public function img_resize($filename, $needHeight, $newFileName) {
        list($width, $height) = getimagesize($filename);
        $ratio = $width / $height;
        $newWidth = $needHeight * $ratio;
        $newHeight = $needHeight;

        $thumb = imagecreatetruecolor($newWidth, $newHeight);
        $source = imagecreatefromjpeg($filename);

        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        imagejpeg($thumb, $newFileName);
        return true;
    }

    function RotateJpg($filename, $angle = 0) {
        $original   =   imagecreatefromjpeg($filename);
        $rotated    =   imagerotate($original, $angle, 0);

        imagejpeg($rotated, $filename);
        imagedestroy($rotated);
    }

    public function logger($text)
    {
        $fp = fopen('logger.txt', 'a+');
        fwrite($fp, $text . PHP_EOL);
        fclose($fp);
    }


    public function totalBalance()
    {
        $userId = Yii::$app->user->id;
        $balancesModel = new Balances();
        return $balancesModel->getPartnerBalance($userId);
    }

}