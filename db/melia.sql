-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Хост: localhost:8889
-- Время создания: Авг 29 2015 г., 08:39
-- Версия сервера: 5.5.42
-- Версия PHP: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `melia`
--

-- --------------------------------------------------------

--
-- Структура таблицы `balances`
--

CREATE TABLE `balances` (
	`id` int(11) NOT NULL,
	`userid` int(11) NOT NULL,
	`usertypeid` int(11) NOT NULL,
	`transaction` int(11) NOT NULL,
	`datetime` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `characters`
--

CREATE TABLE `characters` (
	`id` int(11) NOT NULL,
	`title` varchar(120) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `chats`
--

CREATE TABLE `chats` (
	`id` int(11) NOT NULL,
	`girlid` int(11) NOT NULL,
	`topdatesgirlid` int(11) NOT NULL,
	`manid` int(11) NOT NULL,
	`datetime` int(15) NOT NULL,
	`text` text NOT NULL,
	`statusid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
	`id` int(11) NOT NULL,
	`countryid` int(11) NOT NULL,
	`title` varchar(120) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `countries`
--

CREATE TABLE `countries` (
	`id` int(11) NOT NULL,
	`title` varchar(120) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
	`id` int(11) NOT NULL,
	`path` varchar(120) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `giftpages`
--

CREATE TABLE `giftpages` (
	`id` int(11) NOT NULL,
	`title` varchar(120) NOT NULL,
	`text` text NOT NULL,
	`datetime` int(15) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `girls`
--

CREATE TABLE `girls` (
	`id` int(11) NOT NULL,
	`topdatesid` int(11) NOT NULL,
	`name` varchar(120) NOT NULL,
	`lastname` varchar(120) NOT NULL,
	`birthdate` int(15) NOT NULL,
	`passportnumber` int(15) NOT NULL,
	`passportimage` int(15) NOT NULL,
	`height` int(3) NOT NULL,
	`weight` int(3) NOT NULL,
	`eyescolor` varchar(15) NOT NULL,
	`alcohol` tinyint(1) NOT NULL,
	`materialstatusid` int(11) NOT NULL,
	`smoking` tinyint(1) NOT NULL,
	`children` tinyint(1) NOT NULL,
	`identificationvideo` int(11) NOT NULL,
	`occupation` varchar(255) NOT NULL,
	`religionid` int(11) NOT NULL,
	`countryid` int(11) NOT NULL,
	`cityid` int(11) NOT NULL,
	`livingaddress` varchar(255) NOT NULL,
	`postaddress` varchar(255) NOT NULL,
	`phone` varchar(20) NOT NULL,
	`email` varchar(35) NOT NULL,
	`characterid` int(11) NOT NULL,
	`interests` text NOT NULL,
	`mansdescr` text NOT NULL,
	`firstletter` text NOT NULL,
	`fblink` varchar(120) NOT NULL,
	`vklink` varchar(120) NOT NULL,
	`partnerid` int(11) NOT NULL,
	`statusid` int(11) NOT NULL,
	`is_active` tinyint(1) NOT NULL,
	`is_aproved` tinyint(1) NOT NULL,
	`is_accepted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `girlslanguages`
--

CREATE TABLE `girlslanguages` (
	`id` int(11) NOT NULL,
	`girlid` int(11) NOT NULL,
	`langid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `languages`
--

CREATE TABLE `languages` (
	`id` int(11) NOT NULL,
	`title` varchar(120) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `logs`
--

CREATE TABLE `logs` (
	`id` int(11) NOT NULL,
	`userid` int(11) NOT NULL,
	`usertypeid` int(11) NOT NULL,
	`acceptedtime` int(15) NOT NULL,
	`acceptedip` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `mainbgimage`
--

CREATE TABLE `mainbgimage` (
	`id` int(11) NOT NULL,
	`text` varchar(255) NOT NULL,
	`fileid` int(11) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `materialstatuses`
--

CREATE TABLE `materialstatuses` (
	`id` int(11) NOT NULL,
	`title` varchar(120) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
	`id` int(11) NOT NULL,
	`fromid` int(11) NOT NULL,
	`fromtypeid` int(11) NOT NULL,
	`toid` int(11) NOT NULL,
	`usertypeid` int(11) NOT NULL,
	`text` text NOT NULL,
	`datetime` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE `notifications` (
	`id` int(11) NOT NULL,
	`girlid` int(11) NOT NULL,
	`email` tinyint(1) NOT NULL,
	`phone` tinyint(1) NOT NULL,
	`vk` tinyint(1) NOT NULL,
	`fb` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `partners`
--

CREATE TABLE `partners` (
	`id` int(11) NOT NULL,
	`name` varchar(120) NOT NULL,
	`lastname` varchar(120) NOT NULL,
	`countryid` int(11) NOT NULL,
	`cityid` int(11) NOT NULL,
	`email` varchar(35) NOT NULL,
	`phone` int(20) NOT NULL,
	`avatar` int(11) NOT NULL,
	`passportnumber` int(15) NOT NULL,
	`withdrawal` varchar(255) NOT NULL,
	`getmoneytype` tinyint(1) NOT NULL,
	`is_active` tinyint(1) NOT NULL,
	`is_aproved` tinyint(1) NOT NULL,
	`is_accepted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `photoes`
--

CREATE TABLE `photoes` (
	`id` int(11) NOT NULL,
	`fileid` int(11) NOT NULL,
	`is_active` tinyint(1) NOT NULL,
	`girlid` int(11) NOT NULL,
	`is_cover` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `religions`
--

CREATE TABLE `religions` (
	`id` int(11) NOT NULL,
	`title` varchar(120) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `staticpages`
--

CREATE TABLE `staticpages` (
	`id` int(11) NOT NULL,
	`title` varchar(120) NOT NULL,
	`is_active` tinyint(1) NOT NULL,
	`text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `statuses`
--

CREATE TABLE `statuses` (
	`id` int(11) NOT NULL,
	`title` varchar(120) NOT NULL DEFAULT '11',
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `successstories`
--

CREATE TABLE `successstories` (
	`id` int(11) NOT NULL,
	`title` varchar(120) NOT NULL,
	`text` text NOT NULL,
	`fileid` int(11) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
	`id` int(11) NOT NULL,
	`username` varchar(120) NOT NULL,
	`password` varchar(120) NOT NULL,
	`email` varchar(120) NOT NULL,
	`statusid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `usertypes`
--

CREATE TABLE `usertypes` (
	`id` int(11) NOT NULL,
	`title` varchar(120) NOT NULL,
	`is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `balances`
--
ALTER TABLE `balances`
ADD PRIMARY KEY (`id`),
ADD KEY `balances_fk0` (`usertypeid`);

--
-- Индексы таблицы `characters`
--
ALTER TABLE `characters`
ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `chats`
--
ALTER TABLE `chats`
ADD PRIMARY KEY (`id`),
ADD KEY `chats_fk0` (`girlid`),
ADD KEY `chats_fk1` (`statusid`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
ADD PRIMARY KEY (`id`),
ADD KEY `cities_fk0` (`countryid`);

--
-- Индексы таблицы `countries`
--
ALTER TABLE `countries`
ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `giftpages`
--
ALTER TABLE `giftpages`
ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `girls`
--
ALTER TABLE `girls`
ADD PRIMARY KEY (`id`),
ADD KEY `girls_fk0` (`passportimage`),
ADD KEY `girls_fk1` (`materialstatusid`),
ADD KEY `girls_fk2` (`identificationvideo`),
ADD KEY `girls_fk3` (`religionid`),
ADD KEY `girls_fk4` (`countryid`),
ADD KEY `girls_fk5` (`cityid`),
ADD KEY `girls_fk6` (`characterid`),
ADD KEY `girls_fk7` (`partnerid`),
ADD KEY `girls_fk8` (`statusid`);

--
-- Индексы таблицы `girlslanguages`
--
ALTER TABLE `girlslanguages`
ADD PRIMARY KEY (`id`),
ADD KEY `girlslanguages_fk0` (`girlid`),
ADD KEY `girlslanguages_fk1` (`langid`);

--
-- Индексы таблицы `languages`
--
ALTER TABLE `languages`
ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `logs`
--
ALTER TABLE `logs`
ADD PRIMARY KEY (`id`),
ADD KEY `logs_fk0` (`usertypeid`);

--
-- Индексы таблицы `mainbgimage`
--
ALTER TABLE `mainbgimage`
ADD PRIMARY KEY (`id`),
ADD KEY `mainbgimage_fk0` (`fileid`);

--
-- Индексы таблицы `materialstatuses`
--
ALTER TABLE `materialstatuses`
ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
ADD PRIMARY KEY (`id`),
ADD KEY `messages_fk0` (`fromtypeid`),
ADD KEY `messages_fk1` (`usertypeid`);

--
-- Индексы таблицы `notifications`
--
ALTER TABLE `notifications`
ADD PRIMARY KEY (`id`),
ADD KEY `notifications_fk0` (`girlid`);

--
-- Индексы таблицы `partners`
--
ALTER TABLE `partners`
ADD PRIMARY KEY (`id`),
ADD KEY `partners_fk0` (`countryid`),
ADD KEY `partners_fk1` (`cityid`),
ADD KEY `partners_fk2` (`avatar`);

--
-- Индексы таблицы `photoes`
--
ALTER TABLE `photoes`
ADD PRIMARY KEY (`id`),
ADD KEY `photoes_fk0` (`fileid`),
ADD KEY `photoes_fk1` (`girlid`);

--
-- Индексы таблицы `religions`
--
ALTER TABLE `religions`
ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `staticpages`
--
ALTER TABLE `staticpages`
ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `statuses`
--
ALTER TABLE `statuses`
ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `successstories`
--
ALTER TABLE `successstories`
ADD PRIMARY KEY (`id`),
ADD KEY `successstories_fk0` (`fileid`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
ADD PRIMARY KEY (`id`),
ADD KEY `users_fk0` (`statusid`);

--
-- Индексы таблицы `usertypes`
--
ALTER TABLE `usertypes`
ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `balances`
--
ALTER TABLE `balances`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `characters`
--
ALTER TABLE `characters`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `chats`
--
ALTER TABLE `chats`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `countries`
--
ALTER TABLE `countries`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `giftpages`
--
ALTER TABLE `giftpages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `girls`
--
ALTER TABLE `girls`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `girlslanguages`
--
ALTER TABLE `girlslanguages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `languages`
--
ALTER TABLE `languages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `logs`
--
ALTER TABLE `logs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mainbgimage`
--
ALTER TABLE `mainbgimage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `materialstatuses`
--
ALTER TABLE `materialstatuses`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `notifications`
--
ALTER TABLE `notifications`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `partners`
--
ALTER TABLE `partners`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `photoes`
--
ALTER TABLE `photoes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `religions`
--
ALTER TABLE `religions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `staticpages`
--
ALTER TABLE `staticpages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `statuses`
--
ALTER TABLE `statuses`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `successstories`
--
ALTER TABLE `successstories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `usertypes`
--
ALTER TABLE `usertypes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `balances`
--
ALTER TABLE `balances`
ADD CONSTRAINT `balances_fk0` FOREIGN KEY (`usertypeid`) REFERENCES `usertypes` (`id`);

--
-- Ограничения внешнего ключа таблицы `chats`
--
ALTER TABLE `chats`
ADD CONSTRAINT `chats_fk0` FOREIGN KEY (`girlid`) REFERENCES `girls` (`id`),
ADD CONSTRAINT `chats_fk1` FOREIGN KEY (`statusid`) REFERENCES `statuses` (`id`);

--
-- Ограничения внешнего ключа таблицы `cities`
--
ALTER TABLE `cities`
ADD CONSTRAINT `cities_fk0` FOREIGN KEY (`countryid`) REFERENCES `countries` (`id`);

--
-- Ограничения внешнего ключа таблицы `girls`
--
ALTER TABLE `girls`
ADD CONSTRAINT `girls_fk0` FOREIGN KEY (`passportimage`) REFERENCES `files` (`id`),
ADD CONSTRAINT `girls_fk1` FOREIGN KEY (`materialstatusid`) REFERENCES `materialstatuses` (`id`),
ADD CONSTRAINT `girls_fk2` FOREIGN KEY (`identificationvideo`) REFERENCES `files` (`id`),
ADD CONSTRAINT `girls_fk3` FOREIGN KEY (`religionid`) REFERENCES `religions` (`id`),
ADD CONSTRAINT `girls_fk4` FOREIGN KEY (`countryid`) REFERENCES `countries` (`id`),
ADD CONSTRAINT `girls_fk5` FOREIGN KEY (`cityid`) REFERENCES `cities` (`id`),
ADD CONSTRAINT `girls_fk6` FOREIGN KEY (`characterid`) REFERENCES `characters` (`id`),
ADD CONSTRAINT `girls_fk7` FOREIGN KEY (`partnerid`) REFERENCES `partners` (`id`),
ADD CONSTRAINT `girls_fk8` FOREIGN KEY (`statusid`) REFERENCES `statuses` (`id`);

--
-- Ограничения внешнего ключа таблицы `girlslanguages`
--
ALTER TABLE `girlslanguages`
ADD CONSTRAINT `girlslanguages_fk0` FOREIGN KEY (`girlid`) REFERENCES `girls` (`id`),
ADD CONSTRAINT `girlslanguages_fk1` FOREIGN KEY (`langid`) REFERENCES `languages` (`id`);

--
-- Ограничения внешнего ключа таблицы `logs`
--
ALTER TABLE `logs`
ADD CONSTRAINT `logs_fk0` FOREIGN KEY (`usertypeid`) REFERENCES `usertypes` (`id`);

--
-- Ограничения внешнего ключа таблицы `mainbgimage`
--
ALTER TABLE `mainbgimage`
ADD CONSTRAINT `mainbgimage_fk0` FOREIGN KEY (`fileid`) REFERENCES `files` (`id`);

--
-- Ограничения внешнего ключа таблицы `messages`
--
ALTER TABLE `messages`
ADD CONSTRAINT `messages_fk0` FOREIGN KEY (`fromtypeid`) REFERENCES `usertypes` (`id`),
ADD CONSTRAINT `messages_fk1` FOREIGN KEY (`usertypeid`) REFERENCES `usertypes` (`id`);

--
-- Ограничения внешнего ключа таблицы `notifications`
--
ALTER TABLE `notifications`
ADD CONSTRAINT `notifications_fk0` FOREIGN KEY (`girlid`) REFERENCES `girls` (`id`);

--
-- Ограничения внешнего ключа таблицы `partners`
--
ALTER TABLE `partners`
ADD CONSTRAINT `partners_fk0` FOREIGN KEY (`countryid`) REFERENCES `countries` (`id`),
ADD CONSTRAINT `partners_fk1` FOREIGN KEY (`cityid`) REFERENCES `cities` (`id`),
ADD CONSTRAINT `partners_fk2` FOREIGN KEY (`avatar`) REFERENCES `files` (`id`);

--
-- Ограничения внешнего ключа таблицы `photoes`
--
ALTER TABLE `photoes`
ADD CONSTRAINT `photoes_fk0` FOREIGN KEY (`fileid`) REFERENCES `files` (`id`),
ADD CONSTRAINT `photoes_fk1` FOREIGN KEY (`girlid`) REFERENCES `girls` (`id`);

--
-- Ограничения внешнего ключа таблицы `successstories`
--
ALTER TABLE `successstories`
ADD CONSTRAINT `successstories_fk0` FOREIGN KEY (`fileid`) REFERENCES `files` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_fk0` FOREIGN KEY (`statusid`) REFERENCES `statuses` (`id`);
