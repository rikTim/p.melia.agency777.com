<?php

namespace app\commands;

use Yii;
use app\models\Relations;

class CleanController extends \yii\console\Controller
{
    public function actionRun()
    {

        //очищаем таблицу Reletaion от записей у которых привышен лимит времени
        $time = time() - Yii::$app->params['TimeLimitRelations'];

        $delete = new Relations();
        $delete->deleteRecords($time);
    }

}