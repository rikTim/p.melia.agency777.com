<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/23/15
 * Time: 4:06 PM
 */

namespace app\commands;


use app\components\Curl;
use app\models\Relations;
use Yii;

class GetmessageController extends \yii\console\Controller
{
    public function actionRun($worker_id)
    {
        //берем необходимое количество из Relation
        // собираем массив сообщений
        // и записываем в Chattopdates

        $relations = Relations::find()->count();
        $worker_count = Yii::$app->params['WorkerCount'];
        $count = ceil($relations / $worker_count);
        $process = Relations::find()->limit($count)->offset($worker_id * $count)->all();

        Curl::login();
        foreach ($process as $relation) {
             Curl::getMessage($relation->id_girl, $relation->id_men);
        }

    }
}