<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/9/15
 * Time: 1:20 PM
 */

namespace app\commands;

use Yii;
use app\models\ChatTopDates;
use app\models\Girls;
use app\models\Notifications;

class EmailController extends \yii\console\Controller
{
    public function actionRun()
    {
        $notificatioModel = new Notifications();
        $girlModel = new Girls();
        $chatModel = new ChatTopDates();

       $chat_notifications = $chatModel ->getNotificationNull();
        foreach($chat_notifications as $chat_notification)
        {
            $topdatesid = $chat_notification->girl_id;
            $text = $chat_notification->text;
            $girl = $girlModel->getGirlTopdates($topdatesid);
            $girlid = $girl->id;
            $vkLink = $girl->vklink;
            $email = $girl->email;
            $notification = $notificatioModel->getGirlEmail($girlid);
            if($notificatioModel->email == 0) {
                $sendMess = Yii::$app->DLL->sendEmail($text, 'Letter', $email);
            }
            if($notification->vk == 0)
            {
               $needle = '.com/';
                $pos = strpos($vkLink,$needle);
                $pos = $pos+5;
                $vklink = substr($vkLink,$pos);

                send($text,$vklink);
            }
        }

    }
    public function send($message, $domain)
    {
        $url = 'https://api.vk.com/method/messages.send';

        $params = array(
            'domain' => $domain,
            'message' => $message,
            'access_token' => 'd1430807a0fd50a071ec9b2ef88df0e8b2a9d862c56cbb3dadc60d2ad225731ddf5b518ef30e59bc416ec',  // access_token можно вбить хардкодом, если работа будет идти из под одного юзера
            'v' => '5.37',
        );

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
    }
}

