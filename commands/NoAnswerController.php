<?php

namespace app\commands;

use Yii;
use app\models\ChatTopDates;

class NoAnswerController extends \yii\console\Controller
{
    public function actionRun()
    {

        $chatModel = new ChatTopDates();
        $messages = $chatModel->find()->where(['is_answer' => 0, 'is_men_letter' => 1])->groupBy('girl_id')->all();

        foreach ($messages as $message) {
            if (!empty($message->tdGirl)) {
                $text = 'У девушки ' . $message->tdGirl->name . ' ' . $message->tdGirl->lastname . ' id ' . $message->girl_id . ' есть не отвеченые сообщения';
            } else {
                $text = 'У девушки id ' . $message->girl_id . ' есть не отвеченые сообщения';
            }

            // @todo летит в спам
            mail('mixalleach@gmail.com', 'Не прочитанные сообщения', $text);

            sleep(1);
        }
    }

}
