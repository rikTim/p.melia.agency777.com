<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/23/15
 * Time: 4:19 PM
 */

namespace app\commands;

use Yii;
use app\components\BackgroundProcess;

class StartController extends \yii\console\Controller
{
    public function actionRun()
    {
        //запускаем необходимое количество воркеров

        for ($i = 0; $i < Yii::$app->params['WorkerCount']; $i++) {
            BackgroundProcess::launchBackgroundProcessStart('php ' . realpath(Yii::$app->basePath . '/') . '/yii getmessage/run ' . $i);
        }
    }
}