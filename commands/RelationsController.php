<?php
namespace app\commands;

use Yii;
use app\components\Curl;
use app\models\Reply;
use app\models\Relations;


class RelationsController extends \yii\console\Controller
{
    public function actionRun()
    {
        Curl::login();
        $girls = Curl::getIdGirls();

        if (!empty($girls)) {
            foreach ($girls as $girl) {
                $mens = Curl::getIdMen($girl);
                if (!empty($mens)) {
                    foreach ($mens as $men) {
                        $uniq = $girl . $men;
                        $relations = new Relations();
                        $reply = new Reply();
                        $old = $relations->find()->where(['id_girl' => $girl, 'id_men' => $men])->one();
                        if (!empty($old)) {
                            $old->datetime = time();
                            $old->update();
                        }
                        $relations->insertRecords($girl, $men, $uniq);
                        $reply->insertRecords($girl, $men, $uniq);
                    }
                }
            }
        } else {
            return false;
        }
    }
}
