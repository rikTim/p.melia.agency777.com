<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_121959_create_tbl_logs extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('logs', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%logs}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'userid' => 'INT(11) NOT NULL',
                    'usertypeid' => 'INT(11) NOT NULL',
                    'acceptedtime' => 'INT(15) NOT NULL',
                    'acceptedip' => 'INT(15) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_usertypeid_38_00','logs','usertypeid',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_usertypes_38_00','{{%logs}}', 'usertypeid', '{{%usertypes}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `logs`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
