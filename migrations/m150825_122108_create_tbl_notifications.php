<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_122108_create_tbl_notifications extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('notifications', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%notifications}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'girlid' => 'INT(11) NOT NULL',
                    'email' => 'TINYINT(1) NOT NULL',
                    'phone' => 'TINYINT(1) NOT NULL',
                    'vk' => 'TINYINT(1) NOT NULL',
                    'fb' => 'TINYINT(1) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_girlid_68_00','notifications','girlid',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_girls_68_00','{{%notifications}}', 'girlid', '{{%girls}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `notifications`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
