<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_122126_create_tbl_photoes extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('photoes', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%photoes}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'fileid' => 'INT(11) NOT NULL',
                    'is_active' => 'TINYINT(1) NOT NULL',
                    'girlid' => 'INT(11) NOT NULL',
                    'is_cover' => 'TINYINT(1) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_fileid_27_00','photoes','fileid',0);
        $this->createIndex('idx_girlid_27_01','photoes','girlid',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_files_27_00','{{%photoes}}', 'fileid', '{{%files}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_girls_27_01','{{%photoes}}', 'girlid', '{{%girls}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `photoes`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
