<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_121933_create_tbl_girls extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('girls', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%girls}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'topdatesid' => 'INT(11) NOT NULL',
                    'name' => 'VARCHAR(120) NOT NULL',
                    'lastname' => 'VARCHAR(120) NOT NULL',
                    'birthdate' => 'INT(15) NOT NULL',
                    'passportnumber' => 'INT(15) NOT NULL',
                    'passportimage' => 'INT(15) NOT NULL',
                    'height' => 'INT(3) NOT NULL',
                    'weight' => 'INT(3) NOT NULL',
                    'eyescolor' => 'VARCHAR(15) NOT NULL',
                    'alcohol' => 'TINYINT(1) NOT NULL',
                    'materialstatusid' => 'INT(11) NOT NULL',
                    'smoking' => 'TINYINT(1) NOT NULL',
                    'children' => 'TINYINT(1) NOT NULL',
                    'identificationvideo' => 'INT(11) NOT NULL',
                    'occupation' => 'VARCHAR(255) NOT NULL',
                    'religionid' => 'INT(11) NOT NULL',
                    'countryid' => 'INT(11) NOT NULL',
                    'cityid' => 'INT(11) NOT NULL',
                    'livingaddress' => 'VARCHAR(255) NOT NULL',
                    'postaddress' => 'VARCHAR(255) NOT NULL',
                    'phone' => 'VARCHAR(20) NOT NULL',
                    'email' => 'VARCHAR(35) NOT NULL',
                    'characterid' => 'INT(11) NOT NULL',
                    'interests' => 'TEXT NOT NULL',
                    'mansdescr' => 'TEXT NOT NULL',
                    'firstletter' => 'TEXT NOT NULL',
                    'fblink' => 'VARCHAR(120) NOT NULL',
                    'vklink' => 'VARCHAR(120) NOT NULL',
                    'partnerid' => 'INT(11) NOT NULL',
                    'statusid' => 'INT(11) NOT NULL',
                    'is_active' => 'TINYINT(1) NOT NULL',
                    'is_aproved' => 'TINYINT(1) NOT NULL',
                    'is_accepted' => 'TINYINT(4) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_passportimage_36_00','girls','passportimage',0);
        $this->createIndex('idx_materialstatusid_36_01','girls','materialstatusid',0);
        $this->createIndex('idx_identificationvideo_36_02','girls','identificationvideo',0);
        $this->createIndex('idx_religionid_36_03','girls','religionid',0);
        $this->createIndex('idx_countryid_36_04','girls','countryid',0);
        $this->createIndex('idx_cityid_36_05','girls','cityid',0);
        $this->createIndex('idx_characterid_36_06','girls','characterid',0);
        $this->createIndex('idx_partnerid_36_07','girls','partnerid',0);
        $this->createIndex('idx_statusid_36_08','girls','statusid',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_files_36_00','{{%girls}}', 'passportimage', '{{%files}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_materialstatuses_36_01','{{%girls}}', 'materialstatusid', '{{%materialstatuses}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_files_36_02','{{%girls}}', 'identificationvideo', '{{%files}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_religions_36_03','{{%girls}}', 'religionid', '{{%religions}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_countries_36_04','{{%girls}}', 'countryid', '{{%countries}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_cities_36_05','{{%girls}}', 'cityid', '{{%cities}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_characters_36_06','{{%girls}}', 'characterid', '{{%characters}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_partners_36_07','{{%girls}}', 'partnerid', '{{%partners}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_statuses_36_08','{{%girls}}', 'statusid', '{{%statuses}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `girls`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
