<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_121858_create_tbl_cities extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('cities', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%cities}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'countryid' => 'INT(11) NOT NULL',
                    'title' => 'VARCHAR(120) NOT NULL',
                    'is_active' => 'TINYINT(1) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_countryid_25_00','cities','countryid',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_countries_25_00','{{%cities}}', 'countryid', '{{%countries}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `cities`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
