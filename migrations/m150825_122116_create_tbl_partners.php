<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_122116_create_tbl_partners extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('partners', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%partners}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'name' => 'VARCHAR(120) NOT NULL',
                    'lastname' => 'VARCHAR(120) NOT NULL',
                    'countryid' => 'INT(11) NOT NULL',
                    'cityid' => 'INT(11) NOT NULL',
                    'email' => 'VARCHAR(35) NOT NULL',
                    'phone' => 'INT(20) NOT NULL',
                    'avatar' => 'INT(11) NOT NULL',
                    'passportnumber' => 'INT(15) NOT NULL',
                    'withdrawal' => 'VARCHAR(255) NOT NULL',
                    'getmoneytype' => 'TINYINT(1) NOT NULL',
                    'is_active' => 'TINYINT(1) NOT NULL',
                    'is_aproved' => 'TINYINT(1) NOT NULL',
                    'is_accepted' => 'TINYINT(1) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_countryid_83_00','partners','countryid',0);
        $this->createIndex('idx_cityid_83_01','partners','cityid',0);
        $this->createIndex('idx_avatar_83_02','partners','avatar',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_countries_83_00','{{%partners}}', 'countryid', '{{%countries}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_cities_83_01','{{%partners}}', 'cityid', '{{%cities}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_files_83_02','{{%partners}}', 'avatar', '{{%files}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `partners`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
