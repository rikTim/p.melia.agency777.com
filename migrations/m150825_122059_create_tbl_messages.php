<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_122059_create_tbl_messages extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('messages', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%messages}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'fromid' => 'INT(11) NOT NULL',
                    'fromtypeid' => 'INT(11) NOT NULL',
                    'toid' => 'INT(11) NOT NULL',
                    'usertypeid' => 'INT(11) NOT NULL',
                    'text' => 'TEXT NOT NULL',
                    'datetime' => 'INT(15) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_fromtypeid_73_00','messages','fromtypeid',0);
        $this->createIndex('idx_usertypeid_73_01','messages','usertypeid',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_usertypes_73_00','{{%messages}}', 'fromtypeid', '{{%usertypes}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_usertypes_73_01','{{%messages}}', 'usertypeid', '{{%usertypes}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `messages`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
