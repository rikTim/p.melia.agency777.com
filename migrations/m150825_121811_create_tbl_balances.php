<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_121811_create_tbl_balances extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('balances', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%balances}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'userid' => 'INT(11) NOT NULL',
                    'usertypeid' => 'INT(11) NOT NULL',
                    'transaction' => 'INT(11) NOT NULL',
                    'datetime' => 'INT(15) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_usertypeid_59_00','balances','usertypeid',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_usertypes_58_00','{{%balances}}', 'usertypeid', '{{%usertypes}}', 'id', 'CASCADE', 'NO ACTION' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `balances`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
