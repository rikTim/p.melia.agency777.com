<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_122220_create_tbl_successstories extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('successstories', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%successstories}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'title' => 'VARCHAR(120) NOT NULL',
                    'text' => 'TEXT NOT NULL',
                    'fileid' => 'INT(11) NOT NULL',
                    'is_active' => 'TINYINT(1) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_fileid_45_00','successstories','fileid',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_files_45_00','{{%successstories}}', 'fileid', '{{%files}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `successstories`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
