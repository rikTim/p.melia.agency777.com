<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_121852_create_tbl_chats extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('chats', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%chats}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'girlid' => 'INT(11) NOT NULL',
                    'topdatesgirlid' => 'INT(11) NOT NULL',
                    'manid' => 'INT(11) NOT NULL',
                    'datetime' => 'INT(15) NOT NULL',
                    'text' => 'TEXT NOT NULL',
                    'statusid' => 'INT(11) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_girlid_3_00','chats','girlid',0);
        $this->createIndex('idx_statusid_3_01','chats','statusid',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_girls_3_00','{{%chats}}', 'girlid', '{{%girls}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_statuses_3_01','{{%chats}}', 'statusid', '{{%statuses}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `chats`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
