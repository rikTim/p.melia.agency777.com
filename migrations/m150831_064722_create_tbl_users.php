<?php

use yii\db\Schema;
use yii\db\Migration;

class m150831_064722_create_tbl_users extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('users', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%users}}', [
                    'id' => 'INT(11) NOT NULL',
                    'username' => 'VARCHAR(120) NOT NULL',
                    'password' => 'VARCHAR(120) NOT NULL',
                    'email' => 'VARCHAR(120) NOT NULL',
                    'statusid' => 'INT(11) NOT NULL',
                    'auth_key' => 'VARCHAR(120) NOT NULL',
                    'email_confirm_token' => 'VARCHAR(120) NOT NULL',
                    'password_reset_token' => 'VARCHAR(120) NOT NULL',
                ], $tableOptions_mysql);
            }
        }

    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `users`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
