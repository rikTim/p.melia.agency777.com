<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_121943_create_tbl_girlslanguages extends Migration
{
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('girlslanguages', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%girlslanguages}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'girlid' => 'INT(11) NOT NULL',
                    'langid' => 'INT(11) NOT NULL',
                ], $tableOptions_mysql);
            }
        }


        $this->createIndex('idx_girlid_45_00','girlslanguages','girlid',0);
        $this->createIndex('idx_langid_45_01','girlslanguages','langid',0);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_girls_45_00','{{%girlslanguages}}', 'girlid', '{{%girls}}', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('fk_languages_45_01','{{%girlslanguages}}', 'langid', '{{%languages}}', 'id', 'CASCADE', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');
    }

    public function down()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `girlslanguages`');
        $this->execute('SET foreign_key_checks = 1;');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
