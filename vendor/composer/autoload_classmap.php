<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'LightOpenID' => $vendorDir . '/nodge/lightopenid/openid.php',
    'LightOpenIDProvider' => $vendorDir . '/nodge/lightopenid/provider/provider.php',
    'PHPMailer' => $vendorDir . '/phpmailer/phpmailer/class.phpmailer.php',
    'POP3' => $vendorDir . '/phpmailer/phpmailer/class.pop3.php',
    'SMTP' => $vendorDir . '/phpmailer/phpmailer/class.smtp.php',
    'phpmailerException' => $vendorDir . '/phpmailer/phpmailer/class.phpmailer.php',
);
