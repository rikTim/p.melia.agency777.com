<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'c006/yii2-migration-utility' => 
  array (
    'name' => 'c006/yii2-migration-utility',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@c006/utility/migration' => $vendorDir . '/c006/yii2-migration-utility',
    ),
  ),
  'moonlandsoft/yii2-tinymce' => 
  array (
    'name' => 'moonlandsoft/yii2-tinymce',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@moonland/tinymce' => $vendorDir . '/moonlandsoft/yii2-tinymce',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'rodzadra/geolocation' => 
  array (
    'name' => 'rodzadra/geolocation',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@rodzadra/geolocation' => $vendorDir . '/rodzadra/geolocation',
    ),
  ),
  'nodge/yii2-eauth' => 
  array (
    'name' => 'nodge/yii2-eauth',
    'version' => '2.3.0.0',
    'alias' => 
    array (
      '@nodge/eauth' => $vendorDir . '/nodge/yii2-eauth/src',
    ),
    'bootstrap' => 'nodge\\eauth\\Bootstrap',
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'serhatozles/yii2-simplehtmldom' => 
  array (
    'name' => 'serhatozles/yii2-simplehtmldom',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@serhatozles/simplehtmldom' => $vendorDir . '/serhatozles/yii2-simplehtmldom',
    ),
  ),
  'zyx/zyx-phpmailer' => 
  array (
    'name' => 'zyx/zyx-phpmailer',
    'version' => '0.9.2.0',
    'alias' => 
    array (
      '@zyx/phpmailer' => $vendorDir . '/zyx/zyx-phpmailer',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
);
